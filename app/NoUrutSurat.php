<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NoUrutSurat extends Model
{
    protected $table = 'no_urut_surat';

    protected $fillable = [
        'nomor'
    ];
}
