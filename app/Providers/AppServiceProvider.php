<?php

namespace App\Providers;

use App\Pegawai;
use Illuminate\Support\ServiceProvider;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\URL;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Carbon::setLocale('id');
        $dt = Carbon::now();
        // dd($dt->isoFormat('D MMMM Y'));

        Gate::define('isAdmin', function($user) {
        return $user->role == 'Admin';
        });

        Gate::define('onlyUser', function($user, $usr) {
            return Auth::user()->id ==  $usr;
        });

        Gate::define('isSame', function($user, $pegawai) {
            $check = $user->pegawai->nip ==  $pegawai || $user->role == 'Admin';
            return $check; 
        });
    }
}
