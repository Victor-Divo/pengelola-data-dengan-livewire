<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jabatan extends Model
{
    protected $table='jabatan';

    protected $fillable=[
        'id', 
        'nama', 'deskripsi'
    ];

    public function pegawai()
    {
        return $this->hasMany('App\pegawai', 'jabatan_id', 'id');
    }
}
