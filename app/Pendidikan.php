<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pendidikan extends Model
{
    protected $table='pendidikan';

    protected $fillable=[
        'id', 
        'nama'
    ];

    public function pegawai()
    {
        return $this->hasMany('App\pegawai', 'pendidikan_id', 'id');
    }
}
