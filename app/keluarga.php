<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Keluarga extends Model
{
    use SoftDeletes;
    
    protected $table='keluarga';

    protected $fillable=[
        'pegawai_id',
        'nama',
        'nik',
        'tempat_lahir', 
        'tanggal_lahir',
        'jenis_kelamin',
        'alamat',
        'agama',
        'pekerjaan_id',
        'hubungan_id', 
    ];

    public function getTanggalLahirAttribute($tanggal_lahir)
    {
        $tanggal_lahir = Carbon::parse($tanggal_lahir)->format('d/m/Y');
        return $tanggal_lahir;
    }

    public function setTanggalLahirAttribute($tanggal_lahir)
    {
        $tanggal_lahir = Carbon::createFromFormat('d/m/Y', $tanggal_lahir)->format('Y-m-d');
        $this->attributes['tanggal_lahir'] = $tanggal_lahir;
    }

    public function pegawai()
    {
        return $this->belongsTo('App\pegawai', 'pegawai_id');
    }

    public function pekerjaan()
    {
        return $this->belongsTo('App\Pekerjaan', 'pekerjaan_id');
    }

    public function hubungan()
    {
        return $this->belongsTo('App\Hubungan', 'hubungan_id');
    }

}
