<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusPerkawinan extends Model
{
    protected $table='status_perkawinan';

    protected $fillable=[
        'id', 
        'nama'
    ];

    public function pegawai()
    {
        return $this->hasMany('App\pegawai', 'status_perkawinan_id', 'id');
    }
}
