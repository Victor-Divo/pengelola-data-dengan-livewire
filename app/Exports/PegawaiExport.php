<?php

namespace App\Exports;

use App\Pegawai;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class PegawaiExport extends \PhpOffice\PhpSpreadsheet\Cell\StringValueBinder implements FromView, ShouldAutoSize, WithStyles, WithCustomValueBinder
{
    public function styles(Worksheet $sheet)
    {
        // $sheet->getStyle('A')->getFont()->setBold(true);
    }

    public function view(): View
    {
        return view('exports.pegawai', [
            'pegawai' => Pegawai::all()
        ]);
    }
}
