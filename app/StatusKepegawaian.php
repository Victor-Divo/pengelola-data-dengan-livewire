<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StatusKepegawaian extends Model
{
    protected $table='status_kepegawaian';

    protected $fillable = [
        'id', 'jenis', 'deskripsi', 
    ];

    public function pegawai()
    {
        return $this->belongsToMany('App\Pegawai');
    }
    
    public function templateSurat()
    {
        return $this->hasOne('App\TemplateSurat', 'status_kepegawaian_id', 'id');
    }
}
