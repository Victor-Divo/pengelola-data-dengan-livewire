<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TemplateSurat extends Model
{
    protected $table = 'template_surat';

    protected $fillable = [
        'status_kepegawaian_id',
        'header',
        'main',
        'footer'
    ];

    public function statusKepegawaian()
    {
        return $this->belongsTo('App\StatusKepegawaian', 'status_kepegawaian_id');
    }
}
