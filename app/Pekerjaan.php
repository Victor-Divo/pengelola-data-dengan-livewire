<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pekerjaan extends Model
{
    protected $table='pekerjaan';

    protected $fillable=[
        'id', 
        'nama'
    ];

    public function pegawai()
    {
        return $this->hasMany('App\keluarga', 'pekerjaan_id', 'id');
    }
}
