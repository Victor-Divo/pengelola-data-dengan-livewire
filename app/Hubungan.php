<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hubungan extends Model
{
    protected $table='hubungan';

    protected $fillable=[
        'id', 
        'nama'
    ];

    public function pegawai()
    {
        return $this->hasMany('App\keluarga', 'hubungan_id', 'id');
    }
}
