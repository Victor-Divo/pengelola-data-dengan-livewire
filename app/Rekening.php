<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rekening extends Model
{
    protected $table='rekening';

    protected $fillable = [
        'id', 'nama', 
    ];

    public function pegawai()
    {
        return $this->belongsToMany('App\pegawai');
    }
}
