<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departemen extends Model
{
    protected $table='departemen';

    protected $fillable=[
        'id', 
        'nama', 'deskripsi'
    ];

    public function pegawai()
    {
        return $this->hasMany('App\pegawai', 'departemen_id', 'id');
    }
}
