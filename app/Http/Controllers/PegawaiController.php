<?php

namespace App\Http\Controllers;

use App\Exports\PegawaiExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use App\{Pegawai, Jabatan, StatusKepegawaian, TemplateSurat};
use App\Imports\PegawaiImport;
use PDF;
use DataTables;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class PegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view ('pegawai');
    }

    public function getDataPegawai()
    {
        $pegawai = Pegawai::with('jabatan');
        // $DataTables = ;
        return DataTables::eloquent($pegawai)
                    ->editColumn('jabatan_id', function(Pegawai $pegawai) {
                        return $pegawai->jabatan->nama;
                    })
                    ->addColumn('aksi', function($p) {
                        $link = route('pegawai.show', ['pegawai' => $p]);
                        return '<a href="'. $link .'" class="btn btn-info btn-sm text-white" data-turbolinks-action="replace" >Detail</a>';
                    })
                    ->filter(function ($query) {
                        if (request()->has('status')) {
                            $query->where('status', 'like', "%" . request('status') . "%");
                        }
                    }, true)
                    ->rawColumns(['aksi'])
                    ->addIndexColumn()
                    ->toJson();
    }

    public function print($pegawai)
    {
        $pegawai = Pegawai::where('nip', $pegawai)->firstOrFail();
        $pdf = PDF::loadView('livewire.pegawai.pegawai-print', ['pegawai' => $pegawai]);
        return $pdf->stream('CV '.$pegawai->nama.'-'.$pegawai->nip.'.pdf');
    }

    public function printLetter($pegawai, $penomoran, $jenis)
    {
        $pegawai = Pegawai::where('nip', $pegawai)->firstOrFail();
        $jenis = StatusKepegawaian::findOrFail($jenis)->id;
        $collection = collect($pegawai->statusKepegawaian)->map(function($statpeg) {
            return $statpeg;
        });
        $stat_kep = $collection->where('id', $jenis)->first();
        $rome_month = [
            1 => 'I',
            2 => 'II',
            3 => 'III',
            4 => 'IV',
            5 => 'V',
            6 => 'VI',
            7 => 'VII',
            8 => 'VIII',
            9 => 'IX',
            10 => 'X',
            11 => 'XI',
            12 => 'XII',
        ];
        $month = $rome_month[date('n')];
        $template = TemplateSurat::where('status_kepegawaian_id', $jenis)->first();
        $header = $template->header;
        $main = json_decode($template->main);
        $footer = $template->footer;

        $pdf = PDF::loadView('surat-ket-bekerja', [
            'pegawai' => $pegawai,
            'stat_kep' => $stat_kep,
            'jenis' => $jenis,
            'header' => $header,
            'footer' => $footer,
            'main' => $main,
            'month' => $month,
            'penomoran' => $penomoran,
            ]);
        return $pdf->stream('Surat Keterangan Kerja.pdf');
    }

    public function export()
    {
        return Excel::download(new PegawaiExport, 'pegawai.xlsx');
    }

    public function import($file)
    {
        Excel::import(new PegawaiImport, $file);
        return redirect()->route('pegawai')->with('pesan', 'All good!');
    }

    public function download($pegawai, $folder, $file)
    {
        $pegawai = Pegawai::where('nip', $pegawai)->firstOrFail();
        $filename = $folder.'-'.$pegawai->nama.'.'.\File::extension($file);
        return response()->download(Storage::path($folder.'/'.$file), $filename);
    }

    public function preview($pegawai, $folder, $file)
    {
        $path = Storage::path($folder.'/'.$file);
        return response()->file($path);;
    }
}
