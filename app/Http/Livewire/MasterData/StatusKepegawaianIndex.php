<?php

namespace App\Http\Livewire\MasterData;

use App\StatusKepegawaian;
use Illuminate\Validation\Rule;
use Livewire\Component;
use Livewire\WithPagination;

class StatusKepegawaianIndex extends Component
{
    use WithPagination;
 
    public $statusUpdate = false;
    public $conf;
    public $paginate = 5;
    public $search;
    
    public $jenis;
    public $deskripsi;
    public $statKepId;

    //Initialize variable
    public function mount()
    {
    }

    //Where to return the view
    public function render()
    {
        return view('livewire.master-data.status-kepegawaian-index', [
            'daftarStatKep' => $this->search === null ?
            StatusKepegawaian::latest()->paginate($this->paginate) :
            StatusKepegawaian::latest()->where('jenis', env('DB_CONNECTION') == 'pgsql' ? 'ilike' : 'like', '%' . $this->search . '%')
            ->orWhere('deskripsi', env('DB_CONNECTION') == 'pgsql' ? 'ilike' : 'like', '%' . $this->search . '%')
            ->paginate($this->paginate)
        ]);
    }

    //Realtime validation
    public function updated($field)
    {
        $this->validateOnly($field, [
            'jenis' => ['required', Rule::unique('status_kepegawaian')->ignore($this->statKepId)],
            'deskripsi' => 'required',
        ]);
    }

    //Function to check it's update or create
    public function submit()
    {
        $validatedData = $this->validate([
            'jenis' => ['required', Rule::unique('status_kepegawaian')->ignore($this->statKepId)],
            'deskripsi' => 'required',
        ]);

        if ($this->statusUpdate && $this->statKepId) {
            $this->update($validatedData);
            session()->flash('pesan', 'Status kepegawaian '.$this->jenis.' telah tersimpan!');
        } else {
            $this->store($validatedData);
            session()->flash('pesan', 'Status kepegawaian '.$this->jenis.' telah ditambahkan!');
        };
        redirect()->route('status.kepegawaian');
    }

    //Create statKep
    public function store($validatedData)
    {
        // $validatedData = array_merge($validatedData, ['password' =>Hash::make('password')]);
        $statKep = StatusKepegawaian::create($validatedData);
    }

    //Show statKep data to form
    public function showStatKep($id)
    {
        $this->statusUpdate = true;
        $statKep = StatusKepegawaian::find($id);
        $this->jenis = $statKep['jenis'];
        $this->deskripsi = $statKep['deskripsi'];
        $this->statKepId = $statKep['id'];
    }

    //Update statKep
    public function update($validatedData)
    {
        $statKep = StatusKepegawaian::find($this->statKepId);
        $statKep->update($validatedData);
    }

    //Delete statKep
    public function destroy($id, $conf)
    {
        if($id && $conf){
            $data = StatusKepegawaian::find($id);
            $data->delete();
            session()->flash('pesan', 'Status kepegawaian berhasil dihapus!');
        }
        redirect()->route('status.kepegawaian');
    }
}
