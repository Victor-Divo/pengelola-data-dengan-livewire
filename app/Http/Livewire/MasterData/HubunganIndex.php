<?php

namespace App\Http\Livewire\MasterData;

use App\Hubungan;
use Illuminate\Validation\Rule;
use Livewire\Component;
use Livewire\WithPagination;

class HubunganIndex extends Component
{
    use WithPagination;
 
    public $statusUpdate = false;
    public $conf;
    public $paginate = 5;
    public $search;
    
    public $nama;
    public $hubunganId;

    //Initialize variable
    public function mount()
    {
    }

    //Where to return the view
    public function render()
    {
        return view('livewire.master-data.hubungan-index', [
            'daftar_hubungan' => $this->search === null ?
            Hubungan::latest()->paginate($this->paginate) :
            Hubungan::latest()->where('nama', env('DB_CONNECTION') == 'pgsql' ? 'ilike' : 'like', '%' . $this->search . '%')
            ->paginate($this->paginate)
        ]);
    }

    //Realtime validation
    public function updated($field)
    {
        $this->validateOnly($field, [
            'nama' => ['required', Rule::unique('hubungan')->ignore($this->hubunganId)],
        ]);
    }

    //Function to check it's update or create
    public function submit()
    {
        $validatedData = $this->validate([
            'nama' => ['required', Rule::unique('hubungan')->ignore($this->hubunganId)],
        ]);

        if ($this->statusUpdate && $this->hubunganId) {
            $this->update($validatedData);
            session()->flash('pesan', 'Hubungan '.$this->nama.' telah tersimpan!');
        } else {
            $this->store($validatedData);
            session()->flash('pesan', 'Hubungan '.$this->nama.' telah ditambahkan!');
        };
        redirect()->route('hubungan');
    }

    //Create hubungan
    public function store($validatedData)
    {
        // $validatedData = array_merge($validatedData, ['password' =>Hash::make('password')]);
        $hubungan = Hubungan::create($validatedData);
    }

    //Show hubungan data to form
    public function showHubungan($id)
    {
        $this->statusUpdate = true;
        $hubungan = Hubungan::find($id);
        $this->nama = $hubungan['nama'];
        $this->hubunganId = $hubungan['id'];
    }

    //Update hubungan
    public function update($validatedData)
    {
        $hubungan = Hubungan::find($this->hubunganId);
        $hubungan->update($validatedData);
    }

    //Delete hubungan
    public function destroy($id, $conf)
    {
        if($id && $conf){
            $data = Hubungan::find($id);
            $data->delete();
            session()->flash('pesan', 'Hubungan berhasil dihapus!');
        }
        redirect()->route('hubungan');
    }
}
