<?php

namespace App\Http\Livewire\MasterData;

use App\Jabatan;
use Illuminate\Validation\Rule;
use Livewire\Component;
use Livewire\WithPagination;

class JabatanIndex extends Component
{
    use WithPagination;
 
    public $statusUpdate = false;
    public $conf;
    public $paginate = 5;
    public $search;
    
    public $nama;
    public $deskripsi;
    public $jabatanId;

    //Initialize variable
    public function mount()
    {
    }

    //Where to return the view
    public function render()
    {
        return view('livewire.master-data.jabatan-index', [
            'daftar_jabatan' => $this->search === null ?
            Jabatan::latest()->paginate($this->paginate) :
            Jabatan::latest()->where('nama', env('DB_CONNECTION') == 'pgsql' ? 'ilike' : 'like', '%' . $this->search . '%')
            ->orWhere('deskripsi', env('DB_CONNECTION') == 'pgsql' ? 'ilike' : 'like', '%' . $this->search . '%')
            ->paginate($this->paginate)
        ]);
    }

    //Realtime validation
    public function updated($field)
    {
        $this->validateOnly($field, [
            'nama' => ['required', Rule::unique('jabatan')->ignore($this->jabatanId)],
            'deskripsi' => 'required',
        ]);
    }

    //Function to check it's update or create
    public function submit()
    {
        $validatedData = $this->validate([
            'nama' => ['required', Rule::unique('jabatan')->ignore($this->jabatanId)],
            'deskripsi' => 'required',
        ]);

        if ($this->statusUpdate && $this->jabatanId) {
            $this->update($validatedData);
            session()->flash('pesan', 'Jabatan '.$this->nama.' telah tersimpan!');
        } else {
            $this->store($validatedData);
            session()->flash('pesan', 'Jabatan '.$this->nama.' telah ditambahkan!');
        };
        redirect()->route('jabatan');
    }

    //Create jabatan
    public function store($validatedData)
    {
        // $validatedData = array_merge($validatedData, ['password' =>Hash::make('password')]);
        $jabatan = jabatan::create($validatedData);
    }

    //Show jabatan data to form
    public function showJabatan($id)
    {
        $this->statusUpdate = true;
        $jabatan = jabatan::find($id);
        $this->nama = $jabatan['nama'];
        $this->deskripsi = $jabatan['deskripsi'];
        $this->jabatanId = $jabatan['id'];
    }

    //Update jabatan
    public function update($validatedData)
    {
        $jabatan = jabatan::find($this->jabatanId);
        $jabatan->update($validatedData);
    }

    //Delete jabatan
    public function destroy($id, $conf)
    {
        if($id && $conf){
            $data = jabatan::find($id);
            $data->delete();
            session()->flash('pesan', 'Jabatan berhasil dihapus!');
        }
        redirect()->route('jabatan');
    }
}
