<?php

namespace App\Http\Livewire\MasterData;

use App\Departemen;
use Illuminate\Validation\Rule;
use Livewire\Component;
use Livewire\WithPagination;

class DepartemenIndex extends Component
{
    use WithPagination;
 
    public $statusUpdate = false;
    public $conf;
    public $paginate = 5;
    public $search;
    
    public $nama;
    public $deskripsi;
    public $departemenId;

    // Initialize variable
    public function mount()
    {
    }

    // Where to return the view
    public function render()
    {
        return view('livewire.master-data.departemen-index', [
            'daftar_departemen' => $this->search === null ?
            Departemen::latest()->paginate($this->paginate) :
            Departemen::latest()->where('nama', env('DB_CONNECTION') == 'pgsql' ? 'ilike' : 'like', '%' . $this->search . '%')
            ->orWhere('deskripsi', env('DB_CONNECTION') == 'pgsql' ? 'ilike' : 'like', '%' . $this->search . '%')
            ->paginate($this->paginate)
        ]);
    }

    // Realtime validation
    public function updated($field)
    {
        $this->validateOnly($field, [
            'nama' => ['required', Rule::unique('departemen')->ignore($this->departemenId)],
            'deskripsi' => 'required',
        ]);
    }

    // Function to check it's update or create
    public function submit()
    {
        $validatedData = $this->validate([
            'nama' => ['required', Rule::unique('departemen')->ignore($this->departemenId)],
            'deskripsi' => 'required',
        ]);

        if ($this->statusUpdate && $this->departemenId) {
            $this->update($validatedData);
            session()->flash('pesan', 'Departemen '.$this->nama.' telah tersimpan!');
        } else {
            $this->store($validatedData);
            session()->flash('pesan', 'Departemen '.$this->nama.' telah ditambahkan!');
        };
        redirect()->route('departemen');
    }

    // Create departemen
    public function store($validatedData)
    {
        // $validatedData = array_merge($validatedData, ['password' =>Hash::make('password')]);
        $departemen = Departemen::create($validatedData);
    }

    // Show departemen data to form
    public function showDepartemen($id)
    {
        $this->statusUpdate = true;
        $departemen = Departemen::find($id);
        $this->nama = $departemen['nama'];
        $this->deskripsi = $departemen['deskripsi'];
        $this->departemenId = $departemen['id'];
    }

    // Update departemen
    public function update($validatedData)
    {
        $departemen = Departemen::find($this->departemenId);
        $departemen->update($validatedData);
    }

    // Delete departemen
    public function destroy($id, $conf)
    {
        if($id && $conf){
            $data = Departemen::find($id);
            $data->delete();
            session()->flash('pesan', 'Departemen berhasil dihapus!');
        }
        redirect()->route('departemen');
    }
}
