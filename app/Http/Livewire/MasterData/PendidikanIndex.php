<?php

namespace App\Http\Livewire\MasterData;

use App\Pendidikan;
use Illuminate\Validation\Rule;
use Livewire\Component;
use Livewire\WithPagination;

class PendidikanIndex extends Component
{
    use WithPagination;
 
    public $statusUpdate = false;
    public $conf;
    public $paginate = 5;
    public $search;
    
    public $nama;
    public $pendidikanId;

    //Initialize variable
    public function mount()
    {
    }

    //Where to return the view
    public function render()
    {
        return view('livewire.master-data.pendidikan-index', [
            'daftar_pendidikan' => $this->search === null ?
            Pendidikan::latest()->paginate($this->paginate) :
            Pendidikan::latest()->where('nama', env('DB_CONNECTION') == 'pgsql' ? 'ilike' : 'like', '%' . $this->search . '%')
            ->paginate($this->paginate)
        ]);
    }

    //Realtime validation
    public function updated($field)
    {
        $this->validateOnly($field, [
            'nama' => ['required', Rule::unique('pendidikan')->ignore($this->pendidikanId)],
        ]);
    }

    //Function to check it's update or create
    public function submit()
    {
        $validatedData = $this->validate([
            'nama' => ['required', Rule::unique('pendidikan')->ignore($this->pendidikanId)],
        ]);

        if ($this->statusUpdate && $this->pendidikanId) {
            $this->update($validatedData);
            session()->flash('pesan', 'Pendidikan '.$this->nama.' telah tersimpan!');
        } else {
            $this->store($validatedData);
            session()->flash('pesan', 'Pendidikan '.$this->nama.' telah ditambahkan!');
        };
        redirect()->route('pendidikan');
    }

    //Create pendidikan
    public function store($validatedData)
    {
        // $validatedData = array_merge($validatedData, ['password' =>Hash::make('password')]);
        $pendidikan = Pendidikan::create($validatedData);
    }

    //Show pendidikan data to form
    public function showPendidikan($id)
    {
        $this->statusUpdate = true;
        $pendidikan = Pendidikan::find($id);
        $this->nama = $pendidikan['nama'];
        $this->pendidikanId = $pendidikan['id'];
    }

    //Update pendidikan
    public function update($validatedData)
    {
        $pendidikan = Pendidikan::find($this->pendidikanId);
        $pendidikan->update($validatedData);
    }

    //Delete pendidikan
    public function destroy($id, $conf)
    {
        if($id && $conf){
            $data = Pendidikan::find($id);
            $data->delete();
            session()->flash('pesan', 'Pendidikan berhasil dihapus!');
        }
        redirect()->route('pendidikan');
    }
}
