<?php

namespace App\Http\Livewire\MasterData;

use App\StatusPerkawinan;
use Illuminate\Validation\Rule;
use Livewire\Component;
use Livewire\WithPagination;

class StatusPerkawinanIndex extends Component
{
    use WithPagination;
 
    public $statusUpdate = false;
    public $conf;
    public $paginate = 5;
    public $search;
    
    public $nama;
    public $status_perkawinanId;

    //Initialize variable
    public function mount()
    {
    }

    //Where to return the view
    public function render()
    {
        return view('livewire.master-data.status-perkawinan-index', [
            'daftar_status_perkawinan' => $this->search === null ?
            StatusPerkawinan::latest()->paginate($this->paginate) :
            StatusPerkawinan::latest()->where('nama', env('DB_CONNECTION') == 'pgsql' ? 'ilike' : 'like', '%' . $this->search . '%')
            ->paginate($this->paginate)
        ]);
    }

    //Realtime validation
    public function updated($field)
    {
        $this->validateOnly($field, [
            'nama' => ['required', Rule::unique('status_perkawinan')->ignore($this->status_perkawinanId)],
        ]);
    }

    //Function to check it's update or create
    public function submit()
    {
        $validatedData = $this->validate([
            'nama' => ['required', Rule::unique('status_perkawinan')->ignore($this->status_perkawinanId)],
        ]);

        if ($this->statusUpdate && $this->status_perkawinanId) {
            $this->update($validatedData);
            session()->flash('pesan', 'Status perkawinan '.$this->nama.' telah tersimpan!');
        } else {
            $this->store($validatedData);
            session()->flash('pesan', 'Status perkawinan '.$this->nama.' telah ditambahkan!');
        };
        redirect()->route('status.perkawinan');
    }

    //Create status_perkawinan
    public function store($validatedData)
    {
        // $validatedData = array_merge($validatedData, ['password' =>Hash::make('password')]);
        $status_perkawinan = StatusPerkawinan::create($validatedData);
    }

    //Show status_perkawinan data to form
    public function showStatusPerkawinan($id)
    {
        $this->statusUpdate = true;
        $status_perkawinan = StatusPerkawinan::find($id);
        $this->nama = $status_perkawinan['nama'];
        $this->status_perkawinanId = $status_perkawinan['id'];
    }

    //Update status_perkawinan
    public function update($validatedData)
    {
        $status_perkawinan = StatusPerkawinan::find($this->status_perkawinanId);
        $status_perkawinan->update($validatedData);
    }

    //Delete status_perkawinan
    public function destroy($id, $conf)
    {
        if($id && $conf){
            $data = StatusPerkawinan::find($id);
            $data->delete();
            session()->flash('pesan', 'Status perkawinan berhasil dihapus!');
        }
        redirect()->route('status.perkawinan');
    }
}
