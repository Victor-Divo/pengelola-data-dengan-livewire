<?php

namespace App\Http\Livewire\MasterData;

use App\Rekening;
use Illuminate\Validation\Rule;
use Livewire\Component;
use Livewire\WithPagination;

class RekeningIndex extends Component
{
    use WithPagination;
 
    public $statusUpdate = false;
    public $conf;
    public $paginate = 5;
    public $search;
    
    public $nama;
    public $rekeningId;

    //Initialize variable
    public function mount()
    {
    }

    //Where to return the view
    public function render()
    {
        return view('livewire.master-data.rekening-index', [
            'daftar_rekening' => $this->search === null ?
            Rekening::latest()->paginate($this->paginate) :
            Rekening::latest()->where('nama', env('DB_CONNECTION') == 'pgsql' ? 'ilike' : 'like', '%' . $this->search . '%')
            ->paginate($this->paginate)
        ]);
    }

    //Realtime validation
    public function updated($field)
    {
        $this->validateOnly($field, [
            'nama' => ['required', Rule::unique('rekening')->ignore($this->rekeningId)],
        ]);
    }

    //Function to check it's update or create
    public function submit()
    {
        $validatedData = $this->validate([
            'nama' => ['required', Rule::unique('rekening')->ignore($this->rekeningId)],
        ]);

        if ($this->statusUpdate && $this->rekeningId) {
            $this->update($validatedData);
            session()->flash('pesan', 'Bank '.$this->nama.' telah tersimpan!');
        } else {
            $this->store($validatedData);
            session()->flash('pesan', 'Bank '.$this->nama.' telah ditambahkan!');
        };
        redirect()->route('bank');
    }

    //Create rekening
    public function store($validatedData)
    {
        // $validatedData = array_merge($validatedData, ['password' =>Hash::make('password')]);
        $rekening = Rekening::create($validatedData);
    }

    //Show rekening data to form
    public function showRekening($id)
    {
        $this->statusUpdate = true;
        $rekening = Rekening::find($id);
        $this->nama = $rekening['nama'];
        $this->rekeningId = $rekening['id'];
    }

    //Update rekening
    public function update($validatedData)
    {
        $rekening = Rekening::find($this->rekeningId);
        $rekening->update($validatedData);
    }

    //Delete rekening
    public function destroy($id, $conf)
    {
        if($id && $conf){
            $data = Rekening::find($id);
            $data->delete();
            session()->flash('pesan', 'Bank berhasil dihapus!');
        }
        redirect()->route('bank');
    }
}
