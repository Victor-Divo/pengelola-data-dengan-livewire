<?php

namespace App\Http\Livewire\MasterData;

use App\Pekerjaan;
use Illuminate\Validation\Rule;
use Livewire\Component;
use Livewire\WithPagination;

class PekerjaanIndex extends Component
{
    use WithPagination;
 
    public $statusUpdate = false;
    public $conf;
    public $paginate = 5;
    public $search;
    
    public $nama;
    public $pekerjaanId;

    //Initialize variable
    public function mount()
    {
    }

    //Where to return the view
    public function render()
    {
        return view('livewire.master-data.pekerjaan-index', [
            'daftar_pekerjaan' => $this->search === null ?
            Pekerjaan::latest()->paginate($this->paginate) :
            Pekerjaan::latest()->where('nama', env('DB_CONNECTION') == 'pgsql' ? 'ilike' : 'like', '%' . $this->search . '%')
            ->paginate($this->paginate)
        ]);
    }

    //Realtime validation
    public function updated($field)
    {
        $this->validateOnly($field, [
            'nama' => ['required', Rule::unique('pekerjaan')->ignore($this->pekerjaanId)],
        ]);
    }

    //Function to check it's update or create
    public function submit()
    {
        $validatedData = $this->validate([
            'nama' => ['required', Rule::unique('pekerjaan')->ignore($this->pekerjaanId)],
        ]);

        if ($this->statusUpdate && $this->pekerjaanId) {
            $this->update($validatedData);
            session()->flash('pesan', 'Pekerjaan '.$this->nama.' telah tersimpan!');
        } else {
            $this->store($validatedData);
            session()->flash('pesan', 'Pekerjaan '.$this->nama.' telah ditambahkan!');
        };
        redirect()->route('pekerjaan');
    }

    //Create pekerjaan
    public function store($validatedData)
    {
        // $validatedData = array_merge($validatedData, ['password' =>Hash::make('password')]);
        $pekerjaan = Pekerjaan::create($validatedData);
    }

    //Show pekerjaan data to form
    public function showPekerjaan($id)
    {
        $this->statusUpdate = true;
        $pekerjaan = Pekerjaan::find($id);
        $this->nama = $pekerjaan['nama'];
        $this->pekerjaanId = $pekerjaan['id'];
    }

    //Update pekerjaan
    public function update($validatedData)
    {
        $pekerjaan = Pekerjaan::find($this->pekerjaanId);
        $pekerjaan->update($validatedData);
    }

    //Delete pekerjaan
    public function destroy($id, $conf)
    {
        if($id && $conf){
            $data = Pekerjaan::find($id);
            $data->delete();
            session()->flash('pesan', 'Pekerjaan berhasil dihapus!');
        }
        redirect()->route('pekerjaan');
    }
}
