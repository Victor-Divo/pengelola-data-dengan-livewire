<?php

namespace App\Http\Livewire\TemplateSurat;

use App\StatusKepegawaian;
use App\TemplateSurat;
use Livewire\Component;

class TemplateSuratIndex extends Component
{
    public $jenisSurat;

    public function createTemplate()
    {
        $this->validate([
            'jenisSurat' => 'required'
        ]);

        $header = strval (
            '<img class="note-float-left" style="width: 87px; float: left;" src="'.env('APP_URL').'/img/image4.png'.'" height="71" />
            <img class="note-float-right" style="width: 208px; height: 72px; float: right;" src="'.env('APP_URL').'/img/image5.png'.'" />
            <p style="font-family: '.'IBM Plex Sans'.', sans-serif; font-size: 12pt; line-height: 1.2;"><span style="color: #4f4f4f;"><strong>
            <span style="font-size: 9pt;"><br /><br />&nbsp; Jalan Lumbungsari no 3 Telp +6281215990612<br /></span></strong>
            </span><span style="font-size: 9pt;"><span style="color: #4f4f4f;"><strong>&nbsp; Email: hello@azuralabs.id Website: </strong>
            </span><span style="color: #4f4f4f; font-family: '.'IBM Plex Sans'.', sans-serif;"><strong>https://www.azuralabs.id/</strong></span>
            </span></p>'
        );
        $main = [
            'judul' => 'SURAT KETERANGAN KERJA',
            'jenis_surat' => '07',
            'inisial' => 'AL',
            'pembuka' => 'Yang bertanda tangan dibawah ini :',
            'ttd_nama' => 'Abdurrosyid Broto Handoyo',
            'ttd_jabatan' => 'CEO Azura Labs',
            'keterangan' => 'Menerangkan dengan sesungguhnya bahwa :',
            'pernyataan_1' => 'Benar telah bekerja pada perusahaan yang kami pimpin sejak Tanggal',
            'pernyataan_2' => 'sampai dengan Tanggal',
            'pernyataan_3' => 'Dengan Jabatan terakhir sebagai',
            'isi_1' => 'Selama menjadi Pegawai kami, Saudara',
            'isi_2' => 'telah menunjukkan dedikasi dan loyalitas tinggi terhadap perusahaan dan tidak pernah melakukan hal-hal yang merugikan perusahaan.',
            'harapan' => 'Kami berterimakasih dan berharap semoga yang bersangkutan dapat lebih sukses dimasa yang akan datang.',
            'penutup' => 'Demikian Surat Keterangan ini dibuat agar dapat digunakan sebagaimana mestinya.',
            'lokasi' => 'Semarang',
            'tdtgn_jabatan' => 'CEO Azura Labs',
            'tdtgn_nama' => 'Abdurrosyid Broto Handoyo',
        ];
        $footer = strval (
            '<p style="text-align: right;"><span style="color: #4f4f4f; font-family: '.'IBM Plex Sans'.', sans-serif;">Azura Labs&nbsp; </span>
            <img style="vertical-align: text-bottom;" src="'.env('APP_URL').'/img/image3.png'.'" alt="" width="89" height="41" /></p>'
        );

        $template = TemplateSurat::create([
            'status_kepegawaian_id' => $this->jenisSurat,
            'header' => $header,
            'main' => json_encode($main),
            'footer' => $footer,
        ]);

        redirect()->route('template.surat.show', $template);
    }

    public function render()
    {
        $daftar_template = TemplateSurat::all();
        $statpegsNotRelated = StatusKepegawaian::doesntHave('templateSurat')->get();

        return view('livewire.template-surat.template-surat-index', [
            'daftar_template' => $daftar_template,
            'statpegsNotRelated' => $statpegsNotRelated,
        ]);
    }
}
