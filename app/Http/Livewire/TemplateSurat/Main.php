<?php

namespace App\Http\Livewire\TemplateSurat;

use Livewire\Component;
use App\TemplateSurat;

class Main extends Component
{
    public $main;
    public $templateSurat;

    public function mount(TemplateSurat $templateSurat)
    {
        $this->templateSurat = $templateSurat;
        $this->showMain(json_decode($templateSurat->main));
        // dd($this->main);
    }

    public function render()
    {
        return view('livewire.template-surat.main');
    }

    public function showMain($main)
    {
        foreach ($main as $key => $value) {
            $this->main[$key] = $value;;
        }
    }

    public function saveContent()
    {
        // dd($this->main);
        $this->validate([
            'main.*' => 'required'
        ]);

        $this->templateSurat->update([
            'main' => $this->main,
        ]);

        session()->flash('pesan', 'Template Surat Bagian Content Berhasil Disimpan');
        redirect()->route('template.surat.show', $this->templateSurat);
    }
}
