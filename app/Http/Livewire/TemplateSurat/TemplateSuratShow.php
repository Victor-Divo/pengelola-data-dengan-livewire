<?php

namespace App\Http\Livewire\TemplateSurat;

use App\TemplateSurat;
use Livewire\Component;

class TemplateSuratShow extends Component
{
    public $templateSurat;
    public $header;
    public $footer;
    public $body;

    public function mount(TemplateSurat $templateSurat)
    {
        $this->templateSurat = $templateSurat;
        $this->header = $templateSurat->header;
        $this->footer = $templateSurat->footer;
        // dd(json_decode($this->templateSurat->main)->judul);
    }

    public function saveTemplate()
    {
        $validatedData = $this->validate([
            'header' => 'required',
            'footer' => 'required',
        ]);

        $template = TemplateSurat::findOrFail($this->templateSurat->id);
        $template->update($validatedData);
        session()->flash('pesan', 'Template Surat Bagian Header & Footer Berhasil Disimpan');
        redirect()->route('template.surat.show', $template);
    }

    public function render()
    {
        return view('livewire.template-surat.template-surat-show');
    }
}
