<?php

namespace App\Http\Livewire\Keluarga;

use App\Hubungan;
use Livewire\Component;
use Livewire\WithFileUploads;
use App\keluarga;
use App\Pegawai;
use App\Pekerjaan;
use Illuminate\Validation\Rule;

class KeluargaUpdate extends Component
{
    
    use WithFileUploads;
    public $pegawai;
    public $keluarga;
    public $nik;
    public $nama;
    public $tempat_lahir;
    public $tanggal_lahir;
    public $jenis_kelamin;
    public $alamat;
    public $agama;
    public $pekerjaan_id;
    public $hubungan_id;

    protected $listeners = [
    ];

    public function mount(Pegawai $pegawai, Keluarga $keluarga)
    {
        $this->pegawai = $pegawai;
        $this->keluarga = $keluarga;
        $this->showKeluarga($keluarga);
        // dd($this->pekerjaan_id);
    }

    public function render()
    {
        $daftar_hubungan = Hubungan::all();
        $daftar_pekerjaan = Pekerjaan::all();
        return view('livewire.keluarga.keluarga-update', compact('daftar_hubungan', 'daftar_pekerjaan'));
    }

    public function showKeluarga($keluarga)
    {
        $this->nik = $keluarga['nik'];
        $this->nama = $keluarga['nama'];
        $this->tempat_lahir = $keluarga['tempat_lahir'];
        $this->tanggal_lahir = $keluarga['tanggal_lahir'];
        $this->jenis_kelamin = $keluarga['jenis_kelamin'];
        $this->alamat = $keluarga['alamat'];
        $this->agama = $keluarga['agama'];
        $this->pekerjaan_id = $keluarga['pekerjaan_id'];
        $this->hubungan_id = $keluarga['hubungan_id'];
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'nik'=> ['required', 'numeric', Rule::unique('pegawai')->ignore($this->keluarga)->whereNull('deleted_at'), Rule::unique('keluarga')->ignore($this->keluarga)->whereNull('deleted_at')],
            'nama' => 'required|min:3',
            'tempat_lahir'=> 'required',
            'tanggal_lahir'=> 'required',
            'jenis_kelamin'=> 'required',
            'alamat'=> 'required',
            'agama'=> 'required',
            'pekerjaan_id'=> 'required',
            'hubungan_id'=> 'required',
        ]);
    }

    public function update()
    {
        $validatedData = $this->validate([
            'nik'=> 'required',
            'nama' => 'required|min:3',
            'tempat_lahir'=> 'required',
            'tanggal_lahir'=> 'required',
            'jenis_kelamin'=> 'required',
            'alamat'=> 'required',
            'agama'=> 'required',
            'pekerjaan_id'=> 'required',
            'hubungan_id'=> 'required',
        ]);
        if ( $this->keluarga->id ) {
            $validatedData = array_merge(['pegawai_id' => $this->pegawai->id,], $validatedData);
            $keluarga = keluarga::find($this->keluarga->id);
            $keluarga->update($validatedData);

            session()->flash('pesan', 'Keluarga Berhasil Diupdate!');
            $url = route('keluarga.show', [$this->pegawai, $this->keluarga]);
            redirect()->to($url);
        }
    }
}