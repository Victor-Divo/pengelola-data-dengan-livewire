<?php

namespace App\Http\Livewire\Keluarga;

use Livewire\Component;
use App\keluarga;
use App\pegawai;

class KeluargaShow extends Component
{
    
    public $pegawai;
    public $keluarga;
    public $showKeluarga = false;
    public $createKeluarga = false; 
    public $updateKeluarga = false; 
    public $conf; 
    
    protected $listeners = [
    ];
    
    public function mount(Pegawai $pegawai, Keluarga $keluarga)
    {
        $this->pegawai = $pegawai;
        $this->keluarga = $keluarga;
        // dd($pegawai);
    }
    
    public function render()
    {
        $pegawai = $this->pegawai;
        $keluarga = $this->keluarga;
        // // dd($keluarga);
        return view('livewire.keluarga.keluarga-show', compact('keluarga', 'pegawai'));
    }
    
    public function destroy($conf)
    {
        if($conf){
            $this->keluarga->delete();
            
            session()->flash('keluarga', 'Data keluarga berhasil dihapus!');
            $url = route('pegawai.show', [$this->pegawai]).'#keluarga';
            redirect()->to($url);
        }
    }

    public function message()
    {
        session()->flash('pesan', 'Data keluarga telah ditambahkan');
    }
}
