<?php

namespace App\Http\Livewire\Keluarga;

use App\Hubungan;
use Livewire\Component;
use Livewire\WithFileUploads;
use App\keluarga;
use App\Pegawai;
use App\Pekerjaan;
use Illuminate\Validation\Rule;

class KeluargaCreate extends Component
{
    
    use WithFileUploads;

    public $pegawai;
    public $nik;
    public $nama;
    public $tempat_lahir;
    public $tanggal_lahir;
    public $jenis_kelamin;
    public $alamat;
    public $agama;
    public $pekerjaan_id;
    public $hubungan_id;

    public function mount(Pegawai $pegawai)
    {
        $this->pegawai = $pegawai;
    }

    public function render()
    {
        $daftar_hubungan = Hubungan::all();
        $daftar_pekerjaan = Pekerjaan::all();
        return view('livewire.keluarga.keluarga-create', compact('daftar_hubungan', 'daftar_pekerjaan'));
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'nik'=> ['required', 'numeric', Rule::unique('keluarga')->whereNull('deleted_at'), Rule::unique('pegawai')->whereNull('deleted_at')],
            'nama' => 'required|min:3',
            'tempat_lahir'=> 'required',
            'tanggal_lahir'=> 'required',
            'jenis_kelamin'=> 'required',
            'alamat'=> 'required',
            'agama'=> 'required',
            'pekerjaan_id'=> 'required',
            'hubungan_id'=> 'required',
        ]);
    }

    public function storeKeluarga()
    {
        $validatedData = array_merge(['pegawai_id' => $this->pegawai->id], $this->validate([
            'nik'=> ['required', 'numeric', Rule::unique('keluarga')->whereNull('deleted_at'), Rule::unique('pegawai')->whereNull('deleted_at')],
            'nama' => 'required|min:3',
            'tempat_lahir'=> 'required',
            'tanggal_lahir'=> 'required',
            'jenis_kelamin'=> 'required',
            'alamat'=> 'required',
            'agama'=> 'required',
            'pekerjaan_id'=> 'required',
            'hubungan_id'=> 'required',
        ]));
        $keluarga = keluarga::create($validatedData);
        
        session()->flash('keluarga', 'Keluarga Berhasil Ditambahkan!');
        $url = route('pegawai.show', [$this->pegawai]).'#keluarga';
        redirect()->to($url);
    }
}
