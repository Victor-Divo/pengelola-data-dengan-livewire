<?php

namespace App\Http\Livewire\User;

use App\Pegawai;
use Livewire\WithPagination;
use Livewire\Component;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class UserIndex extends Component
{
    use WithPagination;
 
    public $statusUpdate = false;
    public $conf;
    public $paginate = 5;
    public $search;

    public $pegawai_id;
    public $role;
    public $pegawai;
    public $userId;
    public $username;

    //Initialize variable
    public function mount()
    {
        $this->pegawai = Pegawai::with('user:pegawai_id')->get();
    }

    //Where to return the view
    public function render()
    {
        return view('livewire.user.user-index', [
            'users' => $this->search === null ?
            User::latest()->paginate($this->paginate) :
            User::latest()
            ->whereHas('pegawai', function (Builder $query) {
                $query->where('nama', 'LIKE', '%'.$this->search.'%');
            })
            ->orWhere('username', 'LIKE', '%' . $this->search . '%')
            ->paginate($this->paginate)
        ]);
    }

    //Realtime validation
    public function updated($field)
    {
        $this->validateOnly($field, [
            'pegawai_id' => 'required',
            'username' => ['required', Rule::unique('users')->ignore($this->userId)],
            'role' => 'required',
        ]);
    }

    //Function to check it's update or create
    public function submit()
    {
        $validatedData = $this->validate([
            'pegawai_id' => 'required',
            'username' => ['required', Rule::unique('users')->ignore($this->userId)],
            'role' => 'required',
        ]);

        if ($this->statusUpdate && $this->userId) {
            $this->update($validatedData);
            session()->flash('pesan',  Auth::user()->id == $this->userId && Auth::user()->role != $this->role ? 
            'tidak dapat mengubah role akun sendiri' 
            :'User telah teredit!');
        } else {
            $this->store($validatedData);
            session()->flash('pesan', 'User telah tersimpan!');
        };
        redirect()->route('user');
    }

    //Create user
    public function store($validatedData)
    {
        $validatedData = array_merge($validatedData, ['password' => Hash::make('password')]);
        $user = User::create($validatedData);
        $user->update(['email' => $user->pegawai->email, 'password' => Hash::make(Carbon::createFromFormat('d/m/Y', $user->pegawai->tanggal_lahir)->format('dmY'))]);
    }

    //Show user data to form
    public function showUser($id)
    {
        $this->statusUpdate = true;
        $user = User::find($id);
        $this->pegawai_id = $user['pegawai_id'];
        $this->username = $user['username'];
        $this->role = $user['role'];
        $this->userId = $user['id'];
        $this->emit('showed', $this->userId, $this->pegawai_id, $this->role, Auth::user()->id == $this->userId ? true : false);
    }

    //Update user
    public function update($validatedData)
    {
        $user = User::find($this->userId);
        // dd(Auth::user()->id == $user->id && Auth::user()->role != $this->role, Auth::user()->id == $user->id, Auth::user()->role != $this->role);
        $validatedData = array_merge($validatedData, ['email' => $user->pegawai->email]);
        if (!(Auth::user()->id == $user->id && Auth::user()->role != $this->role)) {
            $user->update($validatedData);
        }
    }

    //Delete user
    public function destroy($id, $conf)
    {
        if ($id && $conf && $id != Auth::user()->id){
            $data = User::find($id);
            $data->delete();
            session()->flash('pesan', 'User berhasil dihapus!');
        } else {
            session()->flash('pesan-error', 'User gagal dihapus!');
        }
        redirect()->route('user');
    }
}
