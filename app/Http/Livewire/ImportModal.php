<?php

namespace App\Http\Livewire;

use App\Imports\PegawaiImport;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Livewire\Component;
use Livewire\WithFileUploads;
use Maatwebsite\Excel\Facades\Excel;

class ImportModal extends Component
{
    use WithFileUploads;

    public $file;

    public function render()
    {
        return view('livewire.import-modal');
    }

    // public function updated($field)
    // {
    //     $this->validateOnly($field, [
    //         'file'=> 'required|mimes:xlsx,doc,docx,ppt,pptx,ods,odt,odp',
    //     ]);
    // }

    public function import() 
    {
        $validator = Validator::make(
            [
                'file'      => $this->file,
                'extension' => strtolower($this->file->getClientOriginalExtension()),
            ],
            [
                'file'          => 'required',
                'extension'      => 'required|in:doc,csv,xlsx,xls,docx,ppt,odt,ods,odp',
            ]
        );
        // dd(Storage::path('temp/importpegawai.xlsx'))
        $this->deleteFileExist('temp/importpegawai.xlsx');
        $excel = $this->file->storeAs('temp', 'importpegawai.'.$this->file->getClientOriginalExtension());
        $file_path = Storage::path($excel);
        Excel::import(new PegawaiImport, $file_path);
        // (new PegawaiImport)->import('temp/importpegawai.xlsx', 'public', \Maatwebsite\Excel\Excel::XLSX);
        $this->emit('closeModal');
        redirect()->route('pegawai');
    }

    private function deleteFileExist($data)
    {
        $dataCheck = Storage::exists($data);
        // dd($dataCheck);
        if ($dataCheck) {
            Storage::delete($data);
        }
    }
}
