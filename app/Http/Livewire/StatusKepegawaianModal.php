<?php

namespace App\Http\Livewire;

use App\StatusKepegawaian;
use Carbon\Carbon;
use Livewire\Component;

class StatusKepegawaianModal extends Component
{
    public $pegawai, $status_kepegawaian, $tanggal_mulai, $tanggal_berakhir;
    public $updateMode = false;
    public $inputsStat = [];
    public $i = 0;

    protected $listeners = [
        'showReq' => 'showData',
    ];

    public function mount($pegawai)
    {
        // dd($pegawai);
        // dd($pegawai->statusKepegawaian[0]->pivot->getOriginal());
        $this->pegawai = $pegawai;
        if ( isset($pegawai) ) {
            $this->showData();
            $this->updateMode = true;
        };
    }

    public function add($i)
    {
        $i = $i + 1;
        $this->i = $i;
        array_push($this->inputsStat ,$i);
        $this->emit('newStat', $i);
    }

    public function remove($i)
    {
        unset($this->inputsStat[$i]);
        unset($this->status_kepegawaian[$i + 1]);
        unset($this->tanggal_berakhir[$i + 1]);
        unset($this->tanggal_mulai[$i + 1]);
        // dd($this->status_kepegawaian, $this->tanggal_mulai, $this->tanggal_berakhir);
        $this->emit('rmStat', $i);
    }

    public function showData()
    {
        // dd('asda');
        if(count($this->pegawai->statusKepegawaian) > 0) {
            foreach ($this->pegawai->statusKepegawaian as $kunci => $statusKepegawaian) {
                if (!$kunci == 0) {
                    $this->i = $kunci;
                    array_push($this->inputsStat ,$kunci);
                }
                $this->status_kepegawaian[$kunci] = $statusKepegawaian->pivot->getOriginal()['status_kepegawaian_id'];
                $this->tanggal_mulai[$kunci] = $this->pegawai->mulai[$kunci];
                if (!empty($this->pegawai->akhir[$kunci])) {
                    $this->tanggal_berakhir[$kunci] = $this->pegawai->akhir[$kunci];
                }
            }
            // $this->emit('rmStat', $i);
        }
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'status_kepegawaian.0' => 'required',
            'tanggal_mulai.0' => 'required',
            'status_kepegawaian.*' => 'required',
            'tanggal_mulai.*' => 'required',
            'tanggal_berakhir.*' => 'nullable',
        ],
        [
            'status_kepegawaian.0.required' => 'status_kepegawaian field is required',
            'tanggal_mulai.0.required' => 'tanggal_mulai field is required',
            'status_kepegawaian.*.required' => 'status_kepegawaian field is required',
            'tanggal_mulai.*.required' => 'tanggal_mulai field is required',
            'tanggal_mulai.*.unique' => 'this nomor rekening has been already taken',
        ]);
    }

    public function storeStatKep()
    {
        // dd($this->status_kepegawaian, $this->tanggal_mulai, $this->tanggal_berakhir);
        if ($this->tanggal_mulai && $this->updateMode) {
            $this->pegawai->statusKepegawaian()->detach();
        }

        $this->validate([
                'status_kepegawaian.0' => 'required',
                'tanggal_mulai.0' => 'required',
                'status_kepegawaian.*' => 'required',
                'tanggal_mulai.*' => 'required',
            ],
            [
                'status_kepegawaian.0.required' => 'status_kepegawaian field is required',
                'tanggal_mulai.0.required' => 'tanggal_mulai field is required',
                'tanggal_mulai.0.unique' => 'this nomor rekening has been already taken',
                'status_kepegawaian.*.required' => 'status_kepegawaian field is required',
                'tanggal_mulai.*.required' => 'tanggal_mulai field is required',
                'tanggal_mulai.*.unique' => 'this nomor rekening has been already taken',
            ]
        );
        // dd($this->status_kepegawaian, $this->tanggal_mulai, $this->tanggal_berakhir);
        if ($this->tanggal_mulai && $this->updateMode) {
            $this->attachData();
            session()->flash('message', 'Data Status Kepegawaian berhasil disimpan');
        } elseif (isset($this->status_kepegawaian, $this->tanggal_mulai)) {
            $status_kepegawaian = $this->status_kepegawaian;
            $tanggal_mulai = $this->tanggal_mulai;
            $tanggal_berakhir = $this->tanggal_berakhir;
            $this->emit('sendStatData', $status_kepegawaian, $tanggal_mulai, $tanggal_berakhir);
        }
        
        if (isset($this->status_kepegawaian, $this->tanggal_mulai)) {
            $this->emit('closeStatModal');
        }
    }

    public function attachData()
    {
        foreach ($this->status_kepegawaian as $key => $value) {
            $this->pegawai->statusKepegawaian()->attach(
                $this->status_kepegawaian[$key], [
                    'tanggal_mulai' => Carbon::createFromFormat('d/m/Y', $this->tanggal_mulai[$key])->format('Y-m-d'),
                    'tanggal_berakhir' => empty($this->tanggal_berakhir[$key]) ? null : Carbon::createFromFormat('d/m/Y', $this->tanggal_berakhir[$key])->format('Y-m-d'),
                ]
            );
        };
        // foreach ($this->status_kepegawaian as $key => $value) {
        //     $this->pegawai->statusKepegawaian()->attach(
        //         $this->status_kepegawaian[$key], [
        //             'tanggal_mulai' => $this->tanggal_mulai[$key],
        //             'tanggal_berakhir' => empty($this->tanggal_berakhir[$key]) ? null : $this->tanggal_berakhir[$key],
        //         ]
        //     );
        // };
    }

    public function render()
    {
        $daftar_stat_kep = StatusKepegawaian::all();
        return view('livewire.status-kepegawaian-modal', [
            'daftar_stat_kep' => $daftar_stat_kep,
        ]);
    }
}
