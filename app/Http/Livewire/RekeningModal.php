<?php

namespace App\Http\Livewire;

use App\Rekening;
use Illuminate\Validation\Rule;
use Livewire\Component;

class RekeningModal extends Component
{
    public $pegawai, $nama_bank, $no_rek, $atas_nama;
    public $updateMode = false;
    public $inputs = [];
    public $i = 0;

    public function mount($pegawai)
    {
        // dd($pegawai);
        $this->pegawai = $pegawai;
        if ( isset($pegawai) ) {
            $this->showData();
            $this->updateMode = true;
        };
    }

    public function add($i)
    {
        $i = $i + 1;
        $this->i = $i;
        array_push($this->inputs ,$i);
        $this->emit('newSelect', $i);
    }

    public function remove($i)
    {
        unset($this->inputs[$i]);
        unset($this->nama_bank[$i]);
        unset($this->atas_nama[$i]);
        unset($this->no_rek[$i]);
    }

    public function showData()
    {
        if(count($this->pegawai->rekening) > 0) {
            foreach ($this->pegawai->rekening as $kunci => $rekening) {
                $this->i = $kunci;
                array_push($this->inputs ,$kunci);
                $this->nama_bank[$kunci] = $rekening->pivot->getOriginal()['rekening_id'];
                $this->atas_nama[$kunci] = $rekening->pivot->getOriginal()['atas_nama'];
                $this->no_rek[$kunci] = $rekening->pivot->getOriginal()['no_rek'];
            }
        }
    }

    public function render()
    {
        $daftar_rekening = Rekening::all();
        return view('livewire.rekening-modal', [
            'daftar_rekening' => $daftar_rekening,
        ]);
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            // 'nama_bank.0' => 'required',
            // 'no_rek.0' => 'required|numeric',
            // 'atas_nama.0' => 'required',
            'nama_bank.*' => 'required',
            'no_rek.*' =>   ['required', 'numeric', Rule::unique('pegawai_rekening', 'no_rek')->ignore($this->pegawai->id ?? null, 'pegawai_id')],
            'atas_nama.*' => 'required',
        ],
        [
            // 'nama_bank.0.required' => 'nama_bank field is required',
            // 'no_rek.0.required' => 'no_rek field is required',
            // 'no_rek.0.numeric' => 'this field must be a number',
            // 'atas_nama.0.required' => 'atas_nama field is required',
            'nama_bank.*.required' => 'nama_bank field is required',
            'no_rek.*.required' => 'no_rek field is required',
            'no_rek.*.unique' => 'this nomor rekening has been already taken',
            'no_rek.*.numeric' => 'this field must be a number',
            'atas_nama.*.required' => 'atas_nama field is required',
        ]);
    }

    public function storeRekening()
    {
        // dd($this->nama_bank, $this->no_rek, $this->atas_nama);
        if (isset($this->pegawai) && $this->updateMode) {
            $this->pegawai->rekening()->detach();
        }
        $this->validate([
                // 'nama_bank.0' => 'required',
                // 'no_rek.0' => 'required|unique:pegawai_rekening,no_rek|numeric',
                // 'atas_nama.0' => 'required',
                'nama_bank.*' => 'required',
                'no_rek.*' => ['required', 'numeric', Rule::unique('pegawai_rekening', 'no_rek')->ignore($this->pegawai->id ?? null, 'pegawai_id')],
                'atas_nama.*' => 'required',
            ],
            [
                // 'nama_bank.0.required' => 'nama_bank field is required',
                // 'no_rek.0.required' => 'no_rek field is required',
                // 'no_rek.0.unique' => 'this nomor rekening has been already taken',
                // 'no_rek.0.numeric' => 'this field must be a number',
                // 'atas_nama.0.required' => 'atas_nama field is required',
                'nama_bank.*.required' => 'nama_bank field is required',
                'no_rek.*.required' => 'no_rek field is required',
                'no_rek.*.unique' => 'this nomor rekening has been already taken',
                'no_rek.*.numeric' => 'this field must be a number',    
                'atas_nama.*.required' => 'atas_nama field is required',
            ]
        );

        if (isset($this->pegawai) && $this->updateMode) {
            $this->attachData();
            session()->flash('message', 'Data rekening berhasil disimpan');
        } else{
            $this->emit('sendRekData', $this->nama_bank, $this->no_rek, $this->atas_nama);
        }

        $this->emit('closeModal');
    }

    public function attachData()
    {
        foreach ($this->no_rek as $key => $value) {
            $this->pegawai->rekening()->attach(
                $this->nama_bank[$key], [
                    'no_rek' => $this->no_rek[$key],
                    'atas_nama' => $this->atas_nama[$key],
                ]
            );
        };
    }
}
