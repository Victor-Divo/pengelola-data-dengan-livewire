<?php

namespace App\Http\Livewire;

use App\Pegawai;
use App\User;
use Illuminate\Support\Facades\Http;
use Livewire\Component;
use Illuminate\Support\Str;

class Navigate extends Component
{
    public $src_foto; 
    public $response; 
    public $user; 
    public $pegawai; 

    protected $listeners = [
        'updatedFoto' => 'handleUpFoto'
    ];

    public function mount($user)
    {
        $this->user = $user;
        $this->pegawai = $user->pegawai;
        if (isset($this->pegawai->foto_profil)) {
            $this->src_foto = route('preview', [
                'pegawai' => $this->pegawai,
                'folder' => Str::before($this->pegawai->foto_profil, '/'),
                'file' => Str::after($this->pegawai->foto_profil, '/')
            ]);
            $this->response = Http::get($this->src_foto)->successful();
        }

    }

    public function handleUpFoto($src_foto, $response)
    {
        $this->src_foto = $src_foto;
        $this->response = $response;
    }

    public function render()
    {
        return view('livewire.navigate');
    }
}
