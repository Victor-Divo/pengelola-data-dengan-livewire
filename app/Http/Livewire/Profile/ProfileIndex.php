<?php

namespace App\Http\Livewire\Profile;

use App\Rules\MatchPassword;
use Livewire\Component;
use App\User;
use Illuminate\Support\Facades\Hash;

class ProfileIndex extends Component
{
    public $user;
    public $username;
    public $password;
    public $old_password;
    public $password_confirmation;

    public function mount(User $user)
    {
        $this->user = $user;
    }
    public function render()
    {
        return view('livewire.profile.profile-index');
    }

    public function updated($field)
    {
        // dd($field);
        $this->validateOnly($field, 
        [
            'old_password' => ['required_without:username', new MatchPassword, 'nullable'],
            'password_confirmation' => 'same:password|required_with:password',
            'password' => 'required_with:old_password|min:8|nullable',
            'username' => 'required_without:old_password|min:3|nullable',
        ],
        [
            'password_confirmation.same' => 'this value must be same as password'
        ]);
    }

    public function changeProfile()
    {
        $validatedData =  $this->validate([
            'old_password' => ['required_without:username', new MatchPassword, 'nullable'],
            'password' => 'required_with:old_password|min:8|confirmed|nullable',
            'username' => 'required_without:old_password|min:3|nullable',
        ]);

        if ($this->password) {
            $this->user->update([
                'password' => Hash::make($this->password)
            ]);
        }
        if ($this->username) {
            $this->user->update([
                'username' => $this->username
            ]);
        }
        session()->flash('profil', 'Profil Berhasil Di Ubah');
        $this->clear();
    }

    private function clear() {
        $this->reset(['username', 'password', 'old_password', 'password_confirmation']);
    }
}
