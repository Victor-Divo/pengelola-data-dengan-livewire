<?php

namespace App\Http\Livewire;

use Carbon\Carbon;
use Carbon\CarbonImmutable;
use App\Pegawai;
use Livewire\Component;

class Home extends Component
{
    public function render()
    {
        $now = CarbonImmutable::now()->isoFormat('dddd, D MMMM Y');
        $thisMonth = Carbon::now()->isoFormat('MM');
        // dd($thisMonth);
        $birthThisMonth = Pegawai::whereMonth('tanggal_lahir', $thisMonth)->orderBy('nama')->get();
        return view('livewire.home', compact('now', 'birthThisMonth'));
    }
}
