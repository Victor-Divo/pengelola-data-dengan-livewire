<?php

namespace App\Http\Livewire\Pegawai;

use App\Departemen;
use Illuminate\Support\Str;
use Livewire\Component;
use Livewire\WithFileUploads;
use App\Pegawai;
use App\Jabatan;
use App\Pendidikan;
use App\Rekening;
use App\StatusKepegawaian;
use App\StatusPerkawinan;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Spatie\PdfToImage\Pdf;
use Org_Heigl\Ghostscript\Ghostscript;

class PegawaiCreate extends Component
{
    // Use livewire file upload
    use WithFileUploads;

    protected $listeners = [
        'sendRekData' => 'receiveRekData',
        'sendStatData' => 'receiveStatData',
    ];

    // Make public property to store data from form
    public $pegawaiId;
    public $nip, $nama, $tempat_lahir, $tanggal_lahir, $alamat, $jenis_kelamin, $agama;
    public $status_perkawinan_id, $golongan_darah, $pendidikan_id, $nik, $no_kk, $no_npwp;
    public $no_bpjs_ket, $status_kepegawaian, $tanggal_mulai, $tanggal_berakhir;
    public $jabatan_id, $email, $phone, $departemen_id, $no_bpjs_kes, $file_npwp;
    public $file_ktp, $file_kk, $file_bpjs_kes, $file_bpjs_ket;
    public $foto_profil, $file_cv, $file_ijazah;
    public $nama_bank, $no_rek, $atas_nama;
    public $submited = false;

    // initialize data
    public function mount()
    {
        // Make nip input auto fill
        $this->autoNip();
    }

    // Where to return the view and send some data
    public function render()
    {
        $daftar_jabatan = Jabatan::all();
        $daftar_stat_perk = StatusPerkawinan::all();
        $daftar_pendidikan = Pendidikan::all();
        $daftar_departemen = Departemen::all();
        $daftar_rekening = Rekening::all();
        $daftar_stat_kep = StatusKepegawaian::all();
        return view('livewire.pegawai.pegawai-create',[
                'daftar_jabatan' => $daftar_jabatan,
                'daftar_stat_perk' => $daftar_stat_perk,
                'daftar_departemen' => $daftar_departemen,
                'daftar_pendidikan' => $daftar_pendidikan,
                'daftar_rekening' => $daftar_rekening,
            ]);
    }

    // Automatically generates nip based on highest value in table
    public function autoNip()
    {
        $pegawai = Pegawai::max('nip');
        $num = (int) $pegawai;
        $num++;
        $this->nip = sprintf("%08s", $num);
    }

    // Live validation, check when any inputs is updated and validate it
    public function updated($field)
    {
        $this->validateOnly($field, [
            'nip' => 'numeric|unique:pegawai',
            'nama' => 'required|min:3',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'alamat' => 'required',
            'jenis_kelamin' => 'required',
            'agama' => 'required',
            'status_perkawinan_id' => 'required',
            'golongan_darah' => 'required',
            'pendidikan_id' => 'required',
            'nik' => 'required|numeric|unique:pegawai',
            'no_kk' => 'nullable|numeric',
            'no_npwp' => 'nullable|numeric',
            'no_bpjs_kes' => 'nullable|numeric',
            'no_bpjs_ket' => 'nullable|numeric',
            'departemen_id' => 'required',
            'jabatan_id' => 'required',
            'phone' => 'numeric|unique:pegawai',
            'email' => 'required|email|unique:pegawai',
        ]);
    }

    // Handle form action to store all first form data
    public function storePegawai()
    {   
        if ($this->status_kepegawaian == null) {
            $this->emit('errorOccurr');
        } else {
            // Validate the data and store it to $validatedData
            $validatedData = $this->validate([
                'nip' => 'numeric|unique:pegawai',
                'nama' => 'required|min:3',
                'tempat_lahir' => 'required',
                'tanggal_lahir' => 'required',
                'alamat' => 'required',
                'jenis_kelamin' => 'required',
                'agama' => 'required',
                'status_perkawinan_id' => 'required',
                'golongan_darah' => 'required',
                'pendidikan_id' => 'required',
                'nik' => 'required|numeric|unique:pegawai',
                'no_kk' => 'nullable|numeric',
                'no_npwp' => 'nullable|numeric',
                'no_bpjs_kes' => 'nullable|numeric',
                'no_bpjs_ket' => 'nullable|numeric',
                'departemen_id' => 'required',
                'jabatan_id' => 'required',
                'phone' => 'numeric|unique:pegawai',
                'email' => 'required|email|unique:pegawai',
            ]);
    
            // Validate other data that doesn't available in pegawai table
            // $this->validate([
            //     'status_kepegawaian' => 'required'
            // ]);
    
            // Create pegawai data from $validatedData
            $pegawai = Pegawai::create($validatedData);
            
            // Change date format so it's acceptable for database
    
            // Attach data to pivot table
            // Using many to many relationship with status kepegawaian 
            // foreach ($this->status_kepegawaian as $key => $value) {
            //     $pegawai->statusKepegawaian()->attach(
            //         $this->status_kepegawaian[$key], [
            //             'tanggal_mulai' => $this->tanggal_mulai[$key],
            //             'tanggal_berakhir' => empty($this->tanggal_berakhir[$key]) ? null : $this->tanggal_berakhir[$key],
            //         ]
            //     );
            // };
            foreach ($this->status_kepegawaian as $key => $value) {
                $pegawai->statusKepegawaian()->attach(
                    $this->status_kepegawaian[$key], [
                        'tanggal_mulai' => Carbon::createFromFormat('d/m/Y', $this->tanggal_mulai[$key])->format('Y-m-d'),
                        'tanggal_berakhir' => empty($this->tanggal_berakhir[$key]) ? null : Carbon::createFromFormat('d/m/Y', $this->tanggal_berakhir[$key])->format('Y-m-d'),
                    ]
                );
            };
    
            // Attach data to pivot table
            // Using many to many relationship with rekening
            if ($this->no_rek) {
                foreach ($this->no_rek as $key => $value) {
                    $pegawai->rekening()->attach(
                        $this->nama_bank[$key], [
                            'no_rek' => $this->no_rek[$key],
                            'atas_nama' => $this->atas_nama[$key],
                        ]
                    );
                };
            }
    
            // Fill $pegawaiId, used for update the next form
            $this->pegawaiId = $pegawai->id;
    
            // Show next form
            $this->submited = true;

        }
    }

    // Action when 'abaikan' button pressed
    public function ignore()
    {
        session()->flash('pesan', 'Pegawai Ditambahkan!');
        $this->emitTo('pegawai.pegawai-index', 'notified');
        return redirect()->route('pegawai');
    }

    // Store the second form
    public function storeFile()
    {
        // Validate data and store to $validatedData
        $validatedData = $this->validate([
            'file_ktp'=> 'nullable|required_without_all:file_kk,file_bpjs_kes,file_bpjs_ket,file_npwp,file_cv,file_ijazah|mimes:pdf,png,jpg,jpeg|max:1024',
            'file_kk'=> 'nullable|required_without_all:file_ktp,file_bpjs_kes,file_bpjs_ket,file_npwp,file_cv,file_ijazah|mimes:pdf,png,jpg,jpeg|max:1024',
            'file_bpjs_kes'=> 'nullable|required_without_all:file_ktp,file_kk,file_bpjs_ket,file_npwp,file_cv,file_ijazah|mimes:pdf,png,jpg,jpeg|max:1024',
            'file_bpjs_ket'=> 'nullable|required_without_all:file_ktp,file_kk,file_bpjs_kes,file_npwp,file_cv,file_ijazah|mimes:pdf,png,jpg,jpeg|max:1024',
            'file_npwp'=> 'nullable|required_without_all:file_ktp,file_kk,file_bpjs_kes,file_bpjs_ket,file_cv,file_ijazah|mimes:pdf,png,jpg,jpeg|max:1024',
            'file_cv'=> 'nullable|required_without_all:file_ktp,file_kk,file_bpjs_kes,file_bpjs_ket,file_npwp,file_ijazah|mimes:pdf,png,jpg,jpeg|max:1024',
            'file_ijazah'=> 'nullable|required_without_all:file_ktp,file_kk,file_bpjs_kes,file_bpjs_ket,file_npwp,file_cv|mimes:pdf,png,jpg,jpeg|max:1024',
        ]);
        
        // Check the id is exist or not
        if ( $this->pegawaiId ) {
            $pegawai = pegawai::find($this->pegawaiId);

            // Upload the file from second form
            foreach ($validatedData as $key=>$data) {
                if ($data) {
                    $this->deleteFileExist($pegawai->$key);
                    if ($this->$key->extension() == 'pdf' && !Str::of('file_cv file_ijazah')->contains($key)) {
                        Ghostscript::setGsPath(env('APP_ENV') == 'local' ? 'C:\Program Files\gs\gs9.53.3\bin\gswin64c.exe' : '/app/vendor/gs/bin/gs');
                        $pdf = $this->$key->storeAs('temp', 'temp-pdf'.Str::substr(rand(), -32).'.'.$this->$key->extension()); 
                        // dd(public_path('storage/'.$pdf) );
                        $dbFileName = $key.'/'.'pegawai-'.Str::substr(rand(), -32).'-!-'.Str::slug($pegawai->nama, '-').'.png';
                        $files = new Pdf(public_path('storage/'.$pdf));
                        $files->setOutputFormat('png')->saveImage(public_path('storage/'.$dbFileName));
                        $this->deleteFileExist($pdf);
                        $pegawai->update([
                            $key => $dbFileName,
                        ]);
                    } else {
                        $pegawai->update([
                            $key => $this->$key->storeAs($key, 'pegawai-'.Str::substr(rand(), -32).'-!-'.Str::slug($pegawai->nama, '-').'.'.$this->$key->extension()),
                        ]);
                    }
                }
            };

            // Send the message
            session()->flash('pesan', 'Pegawai Ditambahkan!');
            $this->emit('notified');
            return redirect()->route('pegawai');
        }
    }

    // Handle 'kembali' button
    public function back()
    {
        return redirect()->route('pegawai');
    }
    
    // parse the date
    public function parseDate($date)
    {
        if (!$date == null) {
            $data = Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
            if ($date == $this->tanggal_mulai) {
                $this->tanggal_mulai = $data;
            } elseif ($date == $this->tanggal_berakhir) {
                $this->tanggal_berakhir = $data;
            } 
        } else {
            $this->tanggal_berakhir = null;
        }
    }

    public function receiveRekData($nama_bank, $no_rek, $atas_nama)
    {
        $this->nama_bank = $nama_bank;
        $this->no_rek = $no_rek;
        $this->atas_nama = $atas_nama;
    }

    public function receiveStatData($status_kepegawaian, $tanggal_mulai, $tanggal_berakhir)
    {
        $this->status_kepegawaian = $status_kepegawaian;
        $this->tanggal_mulai = $tanggal_mulai;
        $this->tanggal_berakhir = $tanggal_berakhir; 
    }

    // Search file,and delete
    private function deleteFileExist($data)
    {
        $dataCheck = Storage::exists($data);
        if ($dataCheck) {
            Storage::delete($data);
        }
    }
}
