<?php

namespace App\Http\Livewire\Pegawai;

use App\Departemen;
use App\Jabatan;
use Livewire\Component;
use Livewire\WithFileUploads;
use App\pegawai;
use App\Pendidikan;
use App\Rekening;
use App\StatusKepegawaian;
use App\StatusPerkawinan;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Org_Heigl\Ghostscript\Ghostscript;
use Spatie\PdfToImage\Pdf;

class PegawaiUpdate extends Component
{
    use WithFileUploads;
    protected $firstfour;
    public $pegawai;
    public $pegawaiId;
    public $nip;
    public $nama;
    public $tempat_lahir;
    public $tanggal_lahir;
    public $alamat;
    public $jenis_kelamin;
    public $agama;
    public $status_perkawinan_id;
    public $golongan_darah;
    public $pendidikan_id;
    public $nik;
    public $no_kk;
    public $no_npwp;
    public $no_bpjs_kes;
    public $no_bpjs_ket;
    public $departemen_id;
    public $jabatan_id;
    public $email;
    public $phone;
    public $file_ktp;
    public $file_kk;
    public $file_bpjs_kes;
    public $file_bpjs_ket;
    public $file_npwp;
    public $updateFile = false;

    
    public function mount(Pegawai $pegawai)
    {
        $this->pegawai = $pegawai;
        $this->showPegawai($pegawai);
    }

    public function render()
    {
        $daftar_jabatan = Jabatan::all();
        $daftar_stat_perk = StatusPerkawinan::all();
        $daftar_pendidikan = Pendidikan::all();
        $daftar_departemen = Departemen::all();
        return view('livewire.pegawai.pegawai-update',[
            'daftar_jabatan' => $daftar_jabatan,
            'daftar_stat_perk' => $daftar_stat_perk,
            'daftar_departemen' => $daftar_departemen,
            'daftar_pendidikan' => $daftar_pendidikan,
        ]);
    }

    public function showPegawai($pegawai)
    {
        $this->pegawaiId = $pegawai['id'];
        $this->nama = $pegawai['nama'];
        $this->nip = $pegawai['nip'];
        $this->tempat_lahir = $pegawai['tempat_lahir'];
        $this->tanggal_lahir = $pegawai['tanggal_lahir'];
        $this->alamat = $pegawai['alamat'];
        $this->jenis_kelamin = $pegawai['jenis_kelamin'];
        $this->agama = $pegawai['agama'];
        $this->status_perkawinan_id = $pegawai['status_perkawinan_id'];
        $this->golongan_darah = $pegawai['golongan_darah'];
        $this->pendidikan_id = $pegawai['pendidikan_id'];
        $this->nik = $pegawai['nik'];
        $this->no_kk = $pegawai['no_kk'];
        $this->no_bpjs_kes = $pegawai['no_bpjs_kes'];
        $this->no_bpjs_ket = $pegawai['no_bpjs_ket'];
        $this->no_npwp = $pegawai['no_npwp'];
        $this->departemen_id = $pegawai['departemen_id'];
        $this->jabatan_id = $pegawai['jabatan_id'];
        $this->phone = $pegawai['phone'];
        $this->email = $pegawai['email'];
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'nip' => ['required', 'numeric', Rule::unique('pegawai')->ignore($this->pegawai)],
            'nama' => 'required|min:3',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'alamat' => 'required',
            'jenis_kelamin' => 'required',
            'agama' => 'required',
            'status_perkawinan_id' => 'required',
            'golongan_darah' => 'required',
            'pendidikan_id' => 'required',
            'nik' => ['required', 'numeric', Rule::unique('pegawai')->ignore($this->pegawai)],
            'no_kk' => 'nullable|numeric',
            'no_npwp' => 'nullable|numeric',
            'no_bpjs_kes' => 'nullable|numeric',
            'no_bpjs_ket' => 'nullable|numeric',
            'departemen_id' => 'required',
            'jabatan_id' => 'required',
            'phone' => ['required', 'numeric', Rule::unique('pegawai')->ignore($this->pegawai)],
            'email' => ['required', 'email', Rule::unique('pegawai')->ignore($this->pegawai)],
        ]);
    }

    public function updatePegawai()
    {
        $validatedData = $this->validate([
            'nip' => ['numeric', Rule::unique('pegawai')->ignore($this->pegawai)],
            'nama' => 'required|min:3',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'alamat' => 'required',
            'jenis_kelamin' => 'required',
            'agama' => 'required',
            'status_perkawinan_id' => 'required',
            'golongan_darah' => 'required',
            'pendidikan_id' => 'required',
            'nik' => ['required', 'numeric', Rule::unique('pegawai')->ignore($this->pegawai)],
            'no_kk' => 'nullable|numeric',
            'no_npwp' => 'nullable|numeric',
            'no_bpjs_kes' => 'nullable|numeric',
            'no_bpjs_ket' => 'nullable|numeric',
            'departemen_id' => 'required',
            'jabatan_id' => 'required',
            'phone' => ['numeric', Rule::unique('pegawai')->ignore($this->pegawai)],
            'email' => ['required', 'email', Rule::unique('pegawai')->ignore($this->pegawai)],
        ]);

        if ( $this->pegawaiId ) {
            $pegawai = pegawai::find($this->pegawaiId);
            $pegawai->update($validatedData);
            if ($pegawai->user) {
                $pegawai->user->update([
                    'email' => $pegawai->email,
                ]);
            }
            
            session()->flash('pesan', 'Data pegawai berhasil diperbarui');
            redirect()->route('pegawai.show', ['pegawai' => $pegawai]);
        }
    }
    public function updateFilePegawai()
    {
        $validatedData = $this->validate([
            'file_ktp'=> 'nullable|required_without_all:file_kk,file_bpjs_kes,file_bpjs_ket,file_npwp|mimes:pdf,png,jpg,jpeg|max:1024',
            'file_kk'=> 'nullable|required_without_all:file_ktp,file_bpjs_kes,file_bpjs_ket,file_npwp|mimes:pdf,png,jpg,jpeg|max:1024',
            'file_bpjs_kes'=> 'nullable|required_without_all:file_ktp,file_kk,file_bpjs_ket,file_npwp|mimes:pdf,png,jpg,jpeg|max:1024',
            'file_bpjs_ket'=> 'nullable|required_without_all:file_ktp,file_kk,file_bpjs_kes,file_npwp|mimes:pdf,png,jpg,jpeg|max:1024',
            'file_npwp'=> 'nullable|required_without_all:file_ktp,file_kk,file_bpjs_kes,file_bpjs_ket|mimes:pdf,png,jpg,jpeg|max:1024',
        ]);

        if ( $this->pegawai->id ) {
            $pegawai = pegawai::find($this->pegawai->id);

            // Upload the file from second form
            foreach ($validatedData as $key=>$data) {
                if ($data) {
                    $this->deleteFileExist($pegawai->$key);
                    if ($this->$key->extension() == 'pdf') {
                        Ghostscript::setGsPath(env('APP_ENV') == 'local' ? 'C:\Program Files\gs\gs9.53.3\bin\gswin64c.exe' : '/app/vendor/gs/bin/gs');
                        $pdf = $this->$key->storeAs('temp', 'temp-pdf'.Str::substr(rand(), -32).'.'.$this->$key->extension()); 
                        // dd(public_path('storage/'.$pdf) );
                        $dbFileName = $key.'/'.'pegawai-'.Str::substr(rand(), -32).'-!-'.Str::slug($pegawai->nama, '-').'.png';
                        $temp_file = new Pdf(Storage::path($pdf));
                        $file = $temp_file->setOutputFormat('png')->saveImage(public_path('storage/'.$dbFileName));
                        $this->deleteFileExist($pdf);
                        $pegawai->update([
                            $key => $dbFileName,
                        ]);
                    } else {
                        $pegawai->update([
                            $key => $this->$key->storeAs($key, 'pegawai-'.Str::substr(rand(), -32).'-!-'.Str::slug($pegawai->nama, '-').'.'.$this->$key->extension()),
                        ]);
                    }
                }
            };

            // Send the message
            session()->flash('pesan', 'Data Pegawai Berhasil Diubah!');
            return redirect()->route('pegawai.show', ['pegawai' => $this->pegawai]);
        }
    }

    // Search file,and delete
    private function deleteFileExist($data)
    {
        $dataCheck = Storage::exists($data);
        if ($dataCheck) {
            Storage::delete($data);
        }
    }
}
