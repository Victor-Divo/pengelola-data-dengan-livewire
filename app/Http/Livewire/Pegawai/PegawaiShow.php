<?php

namespace App\Http\Livewire\Pegawai;

use App\NoUrutSurat;
use Illuminate\Support\Facades\Http;
use Livewire\Component;
use App\Pegawai;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Livewire\WithFileUploads;
use Illuminate\Support\Str;
use Spatie\PdfToImage\Pdf;
use Org_Heigl\Ghostscript\Ghostscript;

class PegawaiShow extends Component
{
    use WithFileUploads;
 
    public $pegawai; 
    public $uploadFoto = false; 
    public $data_kantor = false; 
    public $file_cv; 
    public $file_ijazah; 
    public $src_foto; 
    public $response; 
    public $temp_foto; 
    public $penomoran;
    public $selected;
    public $verify = true; 
    public $cropped_foto = true; 
    public $cetakbtn = false;
    public $no_surat;

    protected $listeners = [
        'deleteServ'=> 'destroy',
    ];

    public function mount(Pegawai $pegawai)
    {
        $this->pegawai = $pegawai;
        if (!$pegawai->foto_profil == null) {
            $this->src_foto = route('preview', [
                'pegawai' => $pegawai,
                'folder' => Str::before($pegawai->foto_profil, '/'),
                'file' => Str::after($pegawai->foto_profil, '/')
            ]);
            // dd(Http::get($this->src_foto)->headers(), $this->src_foto);
            // $this->response = !Http::get($this->src_foto)->successful();
        }
        $this->no_surat = NoUrutSurat::max('nomor');
    }

    public function render()
    {
        return view('livewire.pegawai.pegawai-show');
    }
    
    public function getPegawai($id)
    {    
        $pegawai = pegawai::find($id);
        
        return redirect()->route('pegawai.update', ['pegawai' => $pegawai]);
        
    }

    public function deleteConf($kunci)
    {
        $this->emit('deleteReq', $kunci, false);
    }
    
    public function destroy($id, $conf)
    {
        if($id && $conf){
            $data = pegawai::find($id);
            $data->delete();
            session()->flash('pesan', 'Pegawai Terhapus!');
            return redirect()->route('pegawai');
        }
    }

    public function updatedPenomoran($data)
    {
        if ($data == null) {
            $this->penomoran = '000';
        };
    }

    public function updatedTempFoto()
    {
        $this->validate([
            'temp_foto' => 'image|mimes:png,jpg,jpeg|max:1024', // 1MB Max
            ]);
        $this->response = true;
        $this->src_foto = $this->temp_foto->temporaryUrl();
        // dd($this->verify);
        if ($this->verify) {
            $this->emit('changeUrl', $this->temp_foto->temporaryUrl());
        }
    }

    public function confirmPrint($selected)
    {
        $this->selected = $selected;
    }

    public function confirmSelected($cetakbtn)
    {
        $this->cetakbtn = $cetakbtn;
    }

    public function updateFoto()
    {
        $this->validate([
            'temp_foto'=> 'image|mimes:png,jpg,jpeg|max:1024',
        ]);
        if ($this->pegawai->id) {
           $pegawai = Pegawai::findOrFail($this->pegawai->id);
           $this->deleteFileExist($pegawai->foto_profil);
           $pegawai->update([ 
               'foto_profil'=> $this->temp_foto->storeAs('foto_profil', 'pegawai-'.Str::substr(rand(), -32).'-'.$pegawai->nama.'.'.$this->temp_foto->extension()),
            ]);
        }
        $this->uploadFoto = false;
        session()->flash('foto', 'Foto pegawai telah diperbarui');
        $this->src_foto = asset("storage/".$pegawai->foto_profil);
        if (Auth::user()->pegawai_id == $pegawai->id) {
            $this->emit('updatedFoto', $this->src_foto, $this->response);
        };
        return redirect()->route('pegawai.show', ['pegawai' => $pegawai]);
    }

    public function updateDataKantor()
    {
        $validatedData = $this->validate([
            'file_cv'=> 'nullable|required_without:file_ijazah|mimes:pdf,png,jpg,jpeg|max:1024',
            'file_ijazah'=> 'nullable|required_without:file_cv|mimes:pdf,png,jpg,jpeg|max:1024',
        ]);
        $pegawai = Pegawai::findOrFail($this->pegawai->id);
        foreach ($validatedData as $key=>$data) {
            if ($data) {
                $this->deleteFileExist($pegawai->$key);
                $pegawai->update([
                    $key => $this->$key->storeAs($key, 'pegawai-'.Str::substr(rand(), -32).'-!-'.Str::slug($pegawai->nama, '-').'.'.$this->$key->extension()),
                ]);
            }
        };
        $this->uploadFoto = false;
        session()->flash('data-kantor', 'Data kantor pegawai telah diperbarui!');
        $url = route('pegawai.show', $pegawai).'#data-kantor';
        redirect()->to($url);
    }

    public function handledUpdated()
    {
        session()->flash('pesan', 'Data pegawai telah diperbarui');
    }

    public function downResp()
    {
        session()->flash('data-kantor', 'Data Berhasil Didownload');
    }

    private function deleteFileExist($data)
    {
        $dataCheck = Storage::exists($data);
        if ($dataCheck) {
            Storage::delete($data);
        }
    }

    public function suratTercetak()
    {
        $num = (int) $this->no_surat;
        $num++;
        $this->no_surat = sprintf("%03s", $num);
        NoUrutSurat::create(['nomor' => $this->no_surat]);
    }

    public function imageCheck($file)
    {
        $file = \File::extension($file);
        return Str::of('jpg jpeg png')->contains($file);
    }
}
