<?php

namespace App\Http\Livewire\Pegawai;

use Livewire\Component;
use App\Pegawai;

class PegawaiIndex extends Component
{

    public function render()
    {
        return view('livewire.pegawai.pegawai-index');
    }

    public function show($id)
    {
        $pegawai = pegawai::find($id);
        return redirect()->route('pegawai.show', ['pegawai' => $pegawai]);
    }

    public function export()
    {
        session()->flash('pesan', 'data telah diunduh!');
        $this->emit('notified');
    }
    
}
