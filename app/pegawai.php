<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\SoftDeletes;


class Pegawai extends Model
{
    use SoftDeletes;
    
    protected $table='pegawai';
    protected $date = ['tanggal_lahir'];


    protected $fillable=[
        'id',
        'departemen_id',
        'jabatan_id',
        'nip',
        'nik',
        'no_npwp',
        'no_kk',
        'no_bpjs_kes',
        'no_bpjs_ket',
        'nama',
        'tempat_lahir', 
        'tanggal_lahir',
        'alamat',
        'jenis_kelamin',
        'agama',
        'status_perkawinan_id',
        'golongan_darah',
        'pendidikan_id',
        'phone',
        'email',
        'file_ktp',
        'file_npwp',
        'file_kk',
        'file_bpjs_kes',
        'file_bpjs_ket',
        'file_cv',
        'file_ijazah',
        'foto_profil',
    ];
    
    protected $appends = ['mulai', 'akhir']; 

    public function getRouteKeyName()
    {
        return 'nip';
    } 

    public function getTanggalLahirAttribute($tanggal_lahir)
    {
        $tanggal_lahir = Carbon::parse($tanggal_lahir)->format('d/m/Y');
        return $tanggal_lahir;
    }

    public function setTanggalLahirAttribute($tanggal_lahir)
    {
        $tanggal_lahir = $tanggal_lahir ? Carbon::createFromFormat('d/m/Y', $tanggal_lahir)->format('Y-m-d') : null;
        $this->attributes['tanggal_lahir'] = $tanggal_lahir;
    }

    public function getMulaiAttribute()
    {
        $arr_tanggal_mulai = [];
        foreach ($this->statusKepegawaian as $stat ) {
            $getTgl = Carbon::parse($stat->pivot->tanggal_mulai)->format('d/m/Y');
            array_push($arr_tanggal_mulai, $getTgl);
        };
        return $arr_tanggal_mulai;
    }

    public function getAkhirAttribute()
    {
        $arr_tanggal_berakhir = [];
        foreach ($this->statusKepegawaian as $stat ) {
            $getTgl =  $stat->pivot->tanggal_berakhir ? Carbon::parse($stat->pivot->tanggal_berakhir)->format('d/m/Y') : '';
            array_push($arr_tanggal_berakhir, $getTgl);
        };
        return $arr_tanggal_berakhir;
    }

    public function jabatan()
    {
        return $this->belongsTo('App\jabatan', 'jabatan_id');
    }
    
    public function departemen()
    {
        return $this->belongsTo('App\Departemen', 'departemen_id');
    }
    
    public function user()
    {
        return $this->hasOne('App\User', 'pegawai_id', 'id');
    }
    
    public function statusPerkawinan()
    {
        return $this->belongsTo('App\StatusPerkawinan', 'status_perkawinan_id');
    }
    
    public function pendidikan()
    {
        return $this->belongsTo('App\Pendidikan', 'pendidikan_id');
    }

    public function keluarga()
    {
        return $this->hasMany('App\keluarga', 'pegawai_id', 'id');
    }

    public function statusKepegawaian()
    {
        return $this->belongsToMany('App\StatusKepegawaian', 'pegawai_status_kepegawaian', 'pegawai_id', 'status_kepegawaian_id')->withPivot('tanggal_mulai', 'tanggal_berakhir');
    }
    
    public function rekening()
    {
        return $this->belongsToMany('App\Rekening', 'pegawai_rekening', 'pegawai_id', 'rekening_id')->withPivot('no_rek', 'atas_nama');
    }
}
