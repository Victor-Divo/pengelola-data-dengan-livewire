<?php

namespace App\Imports;

use App\Departemen;
use App\Jabatan;
use App\Pegawai;
use App\Pendidikan;
use App\StatusPerkawinan;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Str;

class FirstSheetImport implements ToCollection, WithHeadingRow
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        $record = 0;
        try {
            foreach ($rows as $row) 
            {
                if (Str::of($row['status'])->lower()->words(1, '')->is('active')) {
                    if (gettype($row['pendidikan_terakhir']) != 'integer'){
                        foreach (Pendidikan::all() as $pendidikan) {
                            if ($pendidikan->nama == $row['pendidikan_terakhir']) {
                                $row['pendidikan_terakhir'] = $pendidikan->id;
                                break;
                            } 
                        }
                    }
        
                    if (gettype($row['status_perkawinan']) != 'integer'){
                        foreach (StatusPerkawinan::all() as $status_perkawinan) {
                            if ($status_perkawinan->nama == $row['status_perkawinan']) {
                                $row['status_perkawinan'] = $status_perkawinan->id;
                                break;
                            } 
                        }
                    }
                    
                    if (gettype($row['jabatan']) != 'integer'){
                        foreach (Jabatan::all() as $jabatan) {
                            if ($jabatan->nama == $row['jabatan']) {
                                $row['jabatan'] = $jabatan->id;
                                break;
                            } 
                        }
                    }
                    
                    if (gettype($row['divisi']) != 'integer'){
                        foreach (Departemen::all() as $departemen) {
                            if ($departemen->nama == $row['divisi']) {
                                $row['divisi'] = $departemen->id;
                                break;
                            } 
                        }
                    }
                    // dd($row['pendidikan_terakhir'], $row['status_perkawinan'], $row['jabatan'], $row['divisi'], $row['tanggal_lahir']);
                    Pegawai::create([
                        'nip' => $row['nomor_pegawai'],
                        'nama' => $row['nama'],
                        'tempat_lahir' => $row['tempat_lahir'],
                        // dd(Date::excelToDateTimeObject($row['tanggal_lahir'])->format('Y-m-d')),
                        'tanggal_lahir' => $this->formatDateExcel($row['tanggal_lahir']),
                        'alamat' => $row['alamat'],
                        'jenis_kelamin' => $row['jenis_kelamin'],
                        'agama' => $row['agama'] ,
                        'golongan_darah' => $row['golongan_darah'],
                        'pendidikan_id' => $row['pendidikan_terakhir'],
                        'status_perkawinan_id' => $row['status_perkawinan'],
                        'nik' => $row['nik'],
                        'no_kk' => $row['no_kk'],
                        'no_npwp' => $row['no_npwp'],
                        'no_bpjs_kes' => $row['no_bpjs_kesehatan'],
                        'no_bpjs_ket' => $row['no_bpjs_ketenagakerjaan'],
                        'jabatan_id' => $row['jabatan'],
                        'departemen_id' => $row['divisi'],
                        'phone' => $row['nomor_tlp'],
                        'email' => $row['email'],
                    ]);
                } else {
                    $record ++;
                }
            }
            session()->flash('pesan', 'Berhasil mengimport data! '.$record.' data tidak sesuai aturan diabaikan');

        } catch (\Exception $err) {
            throw $err;
            session()->flash('pesan-error', 'Gagal mengimport data!!');
        };
    }

    public function headingRow(): int
    {
        return 2;
    }

    protected function formatDateExcel($date)
    {
        if (gettype($date) === 'integer' || gettype($date) === 'double') { 
            $birthday = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($date);
            return $birthday->format('d/m/Y'); 
        } 
        return null; 
    }

}
