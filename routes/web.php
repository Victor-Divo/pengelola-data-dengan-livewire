<?php

use App\Pegawai;
use App\TemplateSurat;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/hash', function () {
    $pw = Hash::make('password');
    return $pw;
});
// Route::livewire('/', 'home');

Route::livewire('/profile/{user}', 'profile.profile-index')->name('profile')->middleware('can:onlyUser,user');
Route::get('download/{pegawai}/{folder}/{file}', 'PegawaiController@download')->middleware('can:isSame,pegawai')->name('download');
Route::get('preview/{pegawai}/{folder}/{file}', 'PegawaiController@preview')->middleware('can:isSame,pegawai')->name('preview');

Route::middleware('auth')->group(function () {
    
    Route::middleware('can:isAdmin')->group(function () {
        Route::livewire('/home', 'home')->name('home');
        Route::livewire('/user', 'user.user-index')->name('user');
        Route::livewire('/pegawai', 'pegawai.pegawai-index')->name('pegawai');
        Route::name('template.surat.')->prefix('template-surat')->group(function () {
            Route::livewire('', 'template-surat.template-surat-index')->name('index');
            Route::livewire('/{templateSurat}', 'template-surat.template-surat-show')->name('show');
        });
        Route::group(['prefix' => 'master-data'], function () {
            Route::livewire('/departemen', 'master-data.departemen-index')->name('departemen');
            Route::livewire('/hubungan', 'master-data.hubungan-index')->name('hubungan');
            Route::livewire('/jabatan', 'master-data.jabatan-index')->name('jabatan');
            Route::livewire('/pekerjaan', 'master-data.pekerjaan-index')->name('pekerjaan');
            Route::livewire('/pendidikan', 'master-data.pendidikan-index')->name('pendidikan');
            Route::livewire('/bank', 'master-data.rekening-index')->name('bank');
            Route::livewire('/status-kepegawaian', 'master-data.status-kepegawaian-index')->name('status.kepegawaian');
            Route::livewire('/status-perkawinan', 'master-data.status-perkawinan-index')->name('status.perkawinan');
        });
        Route::get('/export', 'PegawaiController@export')->name('export');
        Route::get('/import/{file}', 'PegawaiController@import')->name('import');
    });
    Route::middleware('can:isSame,pegawai')->prefix('pegawai')->name('pegawai.')->group(function () {
        Route::livewire('/create', 'pegawai.pegawai-create')->name('create')->middleware('can:isAdmin');
        Route::livewire('/update/{pegawai}', 'pegawai.pegawai-update')->name('update');
        Route::livewire('/{pegawai}', 'pegawai.pegawai-show')->name('show');
        Route::get('print/{pegawai}', 'PegawaiController@print')->name('print');
        Route::get('print-surat/{pegawai}/{penomoran}/{jenis}', 'PegawaiController@printLetter')->name('print.letter');
    });
    Route::middleware('can:isSame,pegawai')->prefix('pegawai/{pegawai}/keluarga')->name('keluarga.')->group(function () {
        Route::livewire('/create', 'keluarga.keluarga-create')->name('create');
        Route::livewire('/update/{keluarga}', 'keluarga.keluarga-update')->name('update');
        Route::livewire('/{keluarga}', 'keluarga.keluarga-show')->name('show');
    });

});

Route::get('getdatapegawai', [
    'uses'=>'PegawaiController@getDataPegawai',
    'as'=>'ajax.get.data.pegawai',
]);

Auth::routes();

Route::get('lojot', function(){
    return view('pegawai');
});
Route::get('printes', function(){
    $template = TemplateSurat::findOrFail(1);
    $header = $template->header;
    $main = $template->main;
    $footer = $template->footer;

    return view('surat-ket-bekerja', [
        'header' => $header,
        'main' => $main,
        'footer' => $footer,
        'penomoran' => '001',
        'bulan' => '12',
        'tahun' => '2021',
        'nama' => 'Hans',
        'ttl' => 'Jakarta, 14 Mei 1999',
        'alamat' => 'Jalan pedurungan raya sari no 99',
        'tgl_mulai' => '12 Maret 2017',
        'tgl_brkhr' => '30 Desember 2020',
        'jabatan' => 'Manager',
        'hari_ini' => '11 Januari 2021',
    ]);   
});

Route::get('ori', function()
{
    return view('surat-testing');
});
