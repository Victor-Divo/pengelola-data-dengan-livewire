<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePegawaiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pegawai', function (Blueprint $table) {
            $table->id();
            $table->foreignId('jabatan_id')->references('id')->on('jabatan')->onDelete('cascade');
            $table->foreignId('departemen_id')->references('id')->on('departemen')->onDelete('cascade');
            $table->string('nip');
            $table->string('nama');
            $table->string('tempat_lahir')->nullable();
            $table->date('tanggal_lahir')->nullable();
            $table->text('alamat')->nullable();
            $table->enum('jenis_kelamin', ['Laki-Laki', 'Perempuan'])->nullable();
            $table->enum('agama', ['Islam', 'Kristen', 'Katolik', 'Hindu', 'Budha', 'Konghucu', 'Lainnya'])->nullable();
            $table->foreignId('status_perkawinan_id')->nullable()->references('id')->on('status_perkawinan')->onDelete('cascade');
            $table->enum('golongan_darah', ['A', 'B', 'O', 'AB', '-'])->nullable();
            $table->foreignId('pendidikan_id')->nullable()->references('id')->on('pendidikan')->onDelete('cascade');
            $table->string('nik')->nullable();
            $table->string('no_npwp')->nullable();
            $table->string('no_kk')->nullable();
            $table->string('no_bpjs_kes')->nullable();
            $table->string('no_bpjs_ket')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('file_ktp')->nullable();
            $table->string('file_npwp')->nullable();
            $table->string('file_kk')->nullable();
            $table->string('file_bpjs_kes')->nullable();
            $table->string('file_bpjs_ket')->nullable();
            $table->string('file_cv')->nullable();
            $table->string('file_ijazah')->nullable();
            $table->string('foto_profil')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pegawai');
    }
}
