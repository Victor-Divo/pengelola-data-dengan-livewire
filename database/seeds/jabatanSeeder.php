<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\QueryException;
use App\jabatan;

class JabatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        jabatan::create([
            'nama' => 'CEO',
            'deskripsi' => 'Pemimpin perusahaan',
        ]);
        jabatan::create([
            'nama' => 'COO',
            'deskripsi' => 'Pengambil keputusan operasional perusahaan',
        ]);
        jabatan::create([
            'nama' => 'Project Manager',
            'deskripsi' => 'Memimpin suatu project dengan efektif dan efisien',
        ]);
        jabatan::create([
            'nama' => 'Junior Project Manager',
            'deskripsi' => 'Memimpin suatu project dengan baik dan benar',
        ]);
        jabatan::create([
            'nama' => 'Senior Software Engineer',
            'deskripsi' => 'Melakukan coding aplikasi dengan profesional',
        ]);
        jabatan::create([
            'nama' => 'Software Engineer',
            'deskripsi' => 'Melakukan coding aplikasi dengan cepat',
        ]);
        jabatan::create([
            'nama' => 'Junior Software Engineer',
            'deskripsi' => 'Melakukan coding aplikasi sesuai kebutuhan',
        ]);
        jabatan::create([
            'nama' => 'UI/UX Designer',
            'deskripsi' => 'Membuat desain dan ilustrasi',
        ]);
        jabatan::create([
            'nama' => 'UI/UX Engineer',
            'deskripsi' => 'Melakukan konversi UI/UX kedalam kode',
        ]);
        jabatan::create([
            'nama' => 'Finance',
            'deskripsi' => 'Mengurus keuangan perusahaan agar semakin meningkat',
        ]);
        jabatan::create([
            'nama' => 'Administration',
            'deskripsi' => 'Melakukan tugas administrasi persuratan dan perizinan',
        ]);
        jabatan::create([
            'nama' => 'IT Support',
            'deskripsi' => 'Melakukan troubleshooting apapun',
        ]);
        jabatan::create([
            'nama' => 'System Analyst',
            'deskripsi' => 'Menganalisis kebutuhan pasar berdasar data, untuk kemajuan perusahaan',
        ]);
        jabatan::create([
            'nama' => 'Junior System Analyst',
            'deskripsi' => 'Menganalisis kebutuhan suatu projek',
        ]);
        jabatan::create([
            'nama' => 'Tester',
            'deskripsi' => 'Melakukan pengujian pada aplikasi dan menyampaikannya ke developer',
        ]);
    }
}