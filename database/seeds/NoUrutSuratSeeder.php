<?php

use Illuminate\Database\Seeder;
use App\NoUrutSurat;

class NoUrutSuratSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        NoUrutSurat::create([
            'nomor' => '001',
        ]);
    }
}
