<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\QueryException;
use App\pegawai;
use Carbon\Carbon;

class PegawaiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Development
        // $faker = Faker\Factory::create('id_ID');

        // for ($i = 1; $i <= 1; $i++) {
        //     $num = 0;
        //     $nip = sprintf("%08s", $num);
        //     Pegawai::insert([
        //         'id'=> $i,
        //         'departemen_id'=> $faker->randomElement(['1', '2', '3']),
        //         'jabatan_id'=> $faker->randomElement(['1', '2', '3']),
        //         'nama'=> $faker->name(),
        //         'tempat_lahir'=> $faker->city, 
        //         'tanggal_lahir'=> $faker->dateTime,
        //         'alamat'=> $faker->address(),
        //         'jenis_kelamin'=> $faker->randomElement($array=['Laki-laki', 'Perempuan']),
        //         'agama'=> $faker->randomElement($array=['Islam', 'Kristen', 'Katolik', 'Hindu', 'Budha']),
        //         'status_perkawinan_id'=> $faker->randomElement(['1', '2', '3']),
        //         'golongan_darah'=> $faker->randomElement($array=['A', 'B', 'AB', 'O']),
        //         'pendidikan_id'=> $faker->randomElement(['1', '2', '3']),
        //         'nip'=> $nip,
        //         'phone'=> $faker->e164PhoneNumber,
        //         'email'=> $faker->email,
        //         'nik'=> $faker->unique()->randomNumber($nbDigits = 8),
        //         'no_npwp'=> $faker->randomNumber($nbDigits = NULL, $strict=false),
        //         'no_kk'=> $faker->randomNumber($nbDigits = NULL, $strict=false),
        //         'no_bpjs_kes'=> $faker->randomNumber($nbDigits = NULL, $strict=false),
        //         'no_bpjs_ket'=> $faker->randomNumber($nbDigits = NULL, $strict=false),
        //     ]);
        //     $pegawai = Pegawai::find($i);
        //     $pegawai->rekening()->attach(2, [
        //         'no_rek' => rand(1, 9999999),
        //         'atas_nama' => $faker->name(),
        //     ]);
            
        //     $pegawai->statusKepegawaian()->attach(1, [
        //         'tanggal_mulai' => $faker->date($format = 'Y-m-d', $max = 'now'),
        //         'tanggal_berakhir' => $faker->date($format = 'Y-m-d', $max = 'now'),
        //     ]);
        // }

        // Production
        for ($i = 1; $i <= 1; $i++) {
            $num = 0;
            $nip = sprintf("%08s", $num);
            Pegawai::insert([
                'departemen_id'=> 4,
                'jabatan_id'=> 11,
                'nama'=> 'Administator',
                'tempat_lahir'=> 'Azura Labs', 
                'tanggal_lahir'=> '1999-01-01',
                'alamat'=> 'Indraprasta',
                'jenis_kelamin'=> 'Laki-Laki',
                'agama'=> 'Lainnya',
                'status_perkawinan_id'=> 1,
                'golongan_darah'=> 'O',
                'pendidikan_id'=> 1,
                'nip'=> $nip,
                'phone'=> '081234567890',
                'email'=> 'admin@gmail.com',
                'nik'=> '0000000000000000',
                'no_npwp'=> '0000000000000000',
                'no_kk'=> '0000000000000000',
                'no_bpjs_kes'=> '0000000000000000',
                'no_bpjs_ket'=> '0000000000000000',
            ]);
            $pegawai = Pegawai::find($i);
            $pegawai->rekening()->attach(2, [
                'no_rek' => '0000',
                'atas_nama' => 'Admin',
            ]);
            
            $pegawai->statusKepegawaian()->attach(1, [
                'tanggal_mulai' => '2020-07-01',
                'tanggal_berakhir' => '2021-07-01',
            ]);
        }
    }
}