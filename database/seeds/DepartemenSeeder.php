<?php

use App\Departemen;
use Illuminate\Database\Seeder;

class DepartemenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Departemen::create([
            'nama' => 'Board of Direction',
            'deskripsi' => 'Merecanakan tujuan skalabilitas',
        ]);
        Departemen::create([
            'nama' => 'Business Operation',
            'deskripsi' => 'Melakukan urusan bidang bisnis',
        ]);
        Departemen::create([
            'nama' => 'Partners',
            'deskripsi' => 'Rekan kerja dalam bidang bisnis maupun non-bisnis',
        ]);
        Departemen::create([
            'nama' => 'Product Development',
            'deskripsi' => 'Melakukan proses pengembangan produk',
        ]);
        Departemen::create([
            'nama' => 'Marketing',
            'deskripsi' => 'Melakukan pemasaran produk lewat berbagai media',
        ]);
        Departemen::create([
            'nama' => 'Design Lab',
            'deskripsi' => 'Melakukan tugas di bidang desain',
        ]);
    }
}
