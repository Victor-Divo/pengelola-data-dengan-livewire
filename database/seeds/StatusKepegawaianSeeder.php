<?php

use App\StatusKepegawaian;
use Illuminate\Database\Seeder;

class StatusKepegawaianSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StatusKepegawaian::create([
            'jenis' => 'Training',
            'deskripsi' => 'Masa pelatihan pegawai',
        ]);
        StatusKepegawaian::create([
            'jenis' => 'Kontrak',
            'deskripsi' => 'Pegawai yang sudah memiliki tanda tangan kontrak',
        ]);
        StatusKepegawaian::create([
            'jenis' => 'Fulltime Remote',
            'deskripsi' => 'Pegawai tetap namun remote',
        ]);
        StatusKepegawaian::create([
            'jenis' => 'Fulltime Onsite',
            'deskripsi' => 'Pegawai tetap dan datang ke kantor',
        ]);
        StatusKepegawaian::create([
            'jenis' => 'Part Time',
            'deskripsi' => 'Pegawai Part Time',
        ]);
        StatusKepegawaian::create([
            'jenis' => 'Magang',
            'deskripsi' => 'Pegawai yang sedang melakukan magang',
        ]);
    }
}
