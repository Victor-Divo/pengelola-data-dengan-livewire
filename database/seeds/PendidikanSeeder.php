<?php

use App\Pendidikan;
use Illuminate\Database\Seeder;

class PendidikanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pendidikan::create([
            'nama' => 'SD',
        ]);
        Pendidikan::create([
            'nama' => 'SMP',
        ]);
        Pendidikan::create([
            'nama' => 'SMA',
        ]);
        Pendidikan::create([
            'nama' => 'SMK',
        ]);
        Pendidikan::create([
            'nama' => 'D1',
        ]);
        Pendidikan::create([
            'nama' => 'D2',
        ]);
        Pendidikan::create([
            'nama' => 'D3',
        ]);
        Pendidikan::create([
            'nama' => 'S1',
        ]);
        Pendidikan::create([
            'nama' => 'S2',
        ]);
        Pendidikan::create([
            'nama' => 'S3',
        ]);
    }
}
