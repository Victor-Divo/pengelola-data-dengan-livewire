<?php

use App\Pegawai;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 1; $i++) {
            $num = 0;
            $nip = sprintf("%08s", $num);
            User::insert([
                'pegawai_id'=> $i,
                'username'=> 'admin',
                'email'=> Pegawai::find($i)->email,
                'role' => 'Admin',
                'password' => Hash::make('azuralabs2021'),
            ]);
        }
    }
}
