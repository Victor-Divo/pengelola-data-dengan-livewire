<?php

use App\StatusPerkawinan;
use Illuminate\Database\Seeder;

class StatusPerkawinanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StatusPerkawinan::create([
            'nama' => 'Belum Menikah',
        ]);
        StatusPerkawinan::create([
            'nama' => 'Menikah',
        ]);
        StatusPerkawinan::create([
            'nama' => 'Janda',
        ]);
        StatusPerkawinan::create([
            'nama' => 'Duda',
        ]);
    }
}
