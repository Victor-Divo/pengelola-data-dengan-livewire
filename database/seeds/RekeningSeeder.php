<?php

use App\Rekening;
use Illuminate\Database\Seeder;

class RekeningSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Rekening::create([
            'nama' => 'BCA',
        ]);
        Rekening::create([
            'nama' => 'Mandiri',
        ]);
        Rekening::create([
            'nama' => 'BRI',
        ]);
    }
}
