<?php

use App\Pekerjaan;
use Illuminate\Database\Seeder;

class PekerjaanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pekerjaan::create([
            'nama' => 'Guru',
        ]);
        Pekerjaan::create([
            'nama' => 'Dokter',
        ]);
        Pekerjaan::create([
            'nama' => 'DPR',
        ]);
        Pekerjaan::create([
            'nama' => 'Wiraswasta',
        ]);
        Pekerjaan::create([
            'nama' => 'Pelajar/Mahasiswa',
        ]);
    }
}
