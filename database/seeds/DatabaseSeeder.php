<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\QueryException;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(JabatanSeeder::class);
        $this->call(DepartemenSeeder::class);
        $this->call(HubunganSeeder::class);
        $this->call(PekerjaanSeeder::class);
        $this->call(PendidikanSeeder::class);
        $this->call(RekeningSeeder::class);
        $this->call(StatusPerkawinanSeeder::class);
        $this->call(StatusKepegawaianSeeder::class);
        $this->call(NoUrutSuratSeeder::class);
        $this->call(PegawaiSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(KeluargaSeeder::class);
        $this->call(TemplateSuratSeeder::class);
    }
}
