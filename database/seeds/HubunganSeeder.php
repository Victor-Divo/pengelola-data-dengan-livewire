<?php

use App\Hubungan;
use Illuminate\Database\Seeder;

class HubunganSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Hubungan::create([
            'nama' => 'Anak',
        ]);
        Hubungan::create([
            'nama' => 'Ibu',
        ]);
        Hubungan::create([
            'nama' => 'Ayah',
        ]);
        Hubungan::create([
            'nama' => 'Suami',
        ]);
        Hubungan::create([
            'nama' => 'Istri',
        ]);
    }
}
