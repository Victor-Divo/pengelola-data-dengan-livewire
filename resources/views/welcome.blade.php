<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Azurees</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="icon" href="{{ asset('img/logo.png')}}">

        <!-- Styles -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
        <style>
            html, body {
                /* background-color: #fff; */
                background-image: url( "{{url(asset('img/desktop8.png'))}}" );
                background-repeat:no-repeat;
                background-size: 100%;
                color: #0F4C75;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 15px;
                top: 30px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }
            a:hover {
                /* width: 120px;
                height: 50px;
                left: 0px;
                top: 0px; */
                /* padding: 50px; */
                text-align: center;
                background: #0F4C75;
                border-radius: 10px;
            }
            .links > a {
                color: #fdfdfd;
                padding: 10px 25px;
                font-family: 'Poppins', sans-serif;
                font-size: 17px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-align: center;
                text-decoration: none;
                text-transform: uppercase;
                display: inline-block;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
        
    </head>
    
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        @can('isAdmin')
                            <a href="{{ route('home') }}">Home</a>
                        @else
                            <a href="{{ route('pegawai.show', ['pegawai' => Auth::user()->pegawai ]) }}">My Data</a>
                        @endcan
                    @else
                        <a href="{{ route('login') }}"><strong>Login</strong></a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}"><strong>Register</strong></a>
                        @endif
                    @endauth
                </div>
                {{-- @auth
                    <div>
                        <h3>Hello There, go to home screen by clicking at the top-right of the screen</h3>
                    </div>
                    <div>
                        <h3></h3>
                    </div>
                @else
                    <div>
                        <h3>Hello There, please login or register</h3>
                        <h3> </h3>
                    </div>
                    <div>
                        <h3></h3>
                    </div>
                @endauth --}}
            @endif
        </div>
        

        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    </body>
</html>
