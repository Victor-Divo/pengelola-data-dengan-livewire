<div style="display: inline-block">
    <!-- Modal -->
    <div wire:ignore.self class="modal fade" id="rekeningInput" tabindex="-1" aria-labelledby="rekeningInputLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div wire:loading.class="overlay d-flex justify-content-center align-items-center" class="">
                <span wire:loading.class="fas fa-2x fa-sync fa-spin"></span>
            </div>
            <div class="modal-header">
            <h5 class="modal-title" id="rekeningInputLabel">Input Rekening</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span id="close" aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                {{-- <form id="formRekeningInput" wire:submit.prevent="storeRekening"> --}}
                    @forelse($inputs as $key => $value)
                        <div class=" add-input">
                            <div class="row">
                                <div class="col-md-3">
                                    <select placeholder="Pilih Bank" class="select2bs4modal custom-select" id="nama_bank.{{ $key }}" wire:model.lazy="nama_bank.{{ $key }}" required>
                                        <option value="">Pilih Bank</option>
                                        @foreach ($daftar_rekening as $rekening)
                                        <option value="{{ $rekening->id }}" class="">{{ $rekening->nama }}</option>
                                        @endforeach
                                    </select>
                                    @error('nama_bank.'.$key) <span class="text-danger error">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="number" class="form-control" placeholder="Masukkan Nomor Rekening" wire:model.lazy="no_rek.{{ $key }}" required>
                                        @error('no_rek.'.$key) <span class="text-danger error">{{ $message }}</span>@enderror
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="text" class="form-control" wire:model.lazy="atas_nama.{{ $key }}" placeholder="Masukkan Atas Nama Rekening" required>
                                        @error('atas_nama.'.$key) <span class="text-danger error">{{ $message }}</span>@enderror
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <button class="btn btn-danger" wire:click.prevent="remove({{$key}})">hapus</button>
                                </div>
                            </div>
                        </div>
                    @empty
                        <div class="row">
                            <div class="col-12 text-center">
                                Silahkan tambah data 
                            </div>
                        </div>
                    @endforelse
                {{-- </form> --}}
            </div>
            <div class="modal-footer">
                <button class="btn text-white btn-success " wire:click.prevent="add({{$i}})">
                    Tambah
                </button>
                <button type="" wire:click.prevent="storeRekening" wire:loading.attr='disabled' class="btn btn-primary" form="formRekeningInput">Simpan Perubahan</button>
            </div>
        </div>
        </div>
    </div>
</div>

@push('scripts')
    <script type="application/javascript">
        $(document).ready(function(){
            jQuery.noConflict();
            livewire.on('closeModal', () => {
                $('#close').trigger('click');
                livewire.emit('dataChange')
                console.log(';closed')
            });
        })
    </script>    
@endpush