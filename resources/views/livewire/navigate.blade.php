<div>
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light" wire:key="main-header-navbar">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      @can('isAdmin')
      <li class="nav-item d-none d-sm-inline-block">
        <a href="{{ route('home') }}" class="nav-link">Home</a>
      </li>
      @endcan
    </ul>

    <!-- SEARCH FORM -->

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Messages Dropdown Menu -->
      <li class="nav-item dropdown user-menu">
        <a href="" class="nav-link dropdown-toggle" data-toggle="dropdown">
          <img src="{{$response||isset($pegawai->foto_profil) ? $src_foto : asset('/adminlte/dist/img/default-150x150.png') }}" class="user-image img-circle elevation-1" alt="User Image">
          <span class="d-none d-md-inline">{{$pegawai->nama}}</span>
        </a>
        <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right" style="border-radius: 5px 5px 5px 5px;">
          <!-- User image -->
          <li class="user-header bg-primary" style="border-radius: 5px 5px 0px 0px;">
            <img src="{{$response||isset($pegawai->foto_profil) ? $src_foto : asset('/adminlte/dist/img/default-150x150.png') }}" class="user-image img-circle elevation-1" alt="User Image">
            <p>
              {{$pegawai->nama}} - {{$pegawai->jabatan->nama}}
              <small>Bergabung {{ isset($pegawai->statusKepegawaian->last()->pivot->tanggal_mulai) ? \Carbon\Carbon::now()->diff(\Carbon\Carbon::parse($pegawai->statusKepegawaian->last()->pivot->tanggal_mulai))->format('%y Tahun, %m Bulan and %d Hari') : '0 hari'}} lalu</small>
            </p>
          </li>
          <!-- Menu Body -->
          {{-- <li class="user-body">
            <div class="row">
              <div class="col-4 text-center">
                <a href="#">Followers</a>
              </div>
              <div class="col-4 text-center">
                <a href="#">Sales</a>
              </div>
              <div class="col-4 text-center">
                <a href="#">Friends</a>
              </div>
            </div>
            <!-- /.row -->
          </li> --}}
          <!-- Menu Footer-->
          <li class="user-footer" style="border-radius: 0px 0px 5px 5px;">
            <a href="{{ route('profile', ['user' => Auth::user()]) }}" class="btn btn-outline-primary">Profile</a>
            <a href="{{ route('logout') }}" class="btn btn-outline-secondary float-right" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Sign out</a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
            </form>
          </li>
        </ul>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
      <!-- Brand Logo -->
      <a href="https://www.azuralabs.id/" target="_blank" class="brand-link">
      <img src="{{ asset('img/logo.png') }}"
          alt="AdminLTE Logo"
          class="brand-image img-circle elevation-3"
          style="opacity: .8">
      <span class="brand-text font-weight-light">Azura Labs</span>
      </a>

      <!-- Sidebar -->
      <div class="sidebar">
        <!-- Sidebar Menu -->
      <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column nav-flat nav-legacy nav-child-indent" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
              with font-awesome or any other icon font library -->
          @can('isAdmin')
          <li class="nav-item">
              <a href="{{ route('home') }}" data-turbolinks-action="replace" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                  Dashboard
              </p>
              </a>
          </li>
          <li class="nav-item has-treeview">
            <a class="nav-link">
              <i class="nav-icon fas fa-user-shield"></i>
              <p>
                Admin
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('user') }}" data-turbolinks-action="replace" class="nav-link">
                  <i class="nav-icon fas fa-user-cog"></i>
                  <p>
                    User
                  </p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a class="nav-link">
              <i class="nav-icon fas fa-archive"></i>
              <p>
                Master Data
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('departemen') }}" data-turbolinks-action="replace" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Departemen</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('hubungan') }}" data-turbolinks-action="replace" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Hubungan</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('jabatan') }}" data-turbolinks-action="replace" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Jabatan</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('pekerjaan') }}" data-turbolinks-action="replace" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Pekerjaan</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('pendidikan') }}" data-turbolinks-action="replace" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Pendidikan</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('bank') }}" data-turbolinks-action="replace" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Bank</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('status.kepegawaian') }}" data-turbolinks-action="replace" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Status Kepegawaian</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('status.perkawinan') }}" data-turbolinks-action="replace" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Status Perkawinan</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Pegawai
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('pegawai') }}" data-turbolinks-action="replace" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Daftar Pegawai</p>
                </a>
              </li>
              {{-- <li class="nav-item">
                <a class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Absensi</p>
                </a>
              </li> --}}
            </ul>
          </li>
          <li class="nav-item">
            <a href="{{ route('template.surat.index') }}" data-turbolinks-action="replace" class="nav-link">
            <i class="nav-icon fas fa-file-alt"></i>
            <p>
                Template Surat
            </p>
            </a>
          </li>
        </ul>
        @else
        <li class="nav-item">
          <a href="{{ route('pegawai.show', ['pegawai' => Auth::user()->pegawai]) }}" data-turbolinks-action="replace" class="nav-link">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Pegawai
            </p>
          </a>
        </li>
        @endcan
      </nav>
      <!-- /.sidebar-menu -->
      </div>
      <!-- /.sidebar -->
  </aside>
</div>