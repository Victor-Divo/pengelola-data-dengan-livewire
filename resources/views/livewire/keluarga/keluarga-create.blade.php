@section('title', 'Tambah Keluarga')

@section('header', 'Tambah Keluarga')

@section('breadcumb')
    @can('isAdmin')
        <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="fa fa-home" aria-hidden="true"></i>                    </a></li>
        <li class="breadcrumb-item"><a href="{{route('pegawai')}}">Pegawai</a></li>
    @endcan
    <li class="breadcrumb-item"><a href="{{route('pegawai.show', $pegawai)}}">Detail Pegawai</a></li>
    <li class="breadcrumb-item active">Tambah Keluarga</li>
@endsection

{{-- content --}}
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Form Tambah Data Anggota Keluarga</h3>
        <div class="card-tools">
        </div>
    </div>
    <div class="card-body">
        <form wire:submit.prevent="storeKeluarga">
            @csrf
            <div class="row">
                <div class="form-group col">
                    <div class="row-6 mb-3">
                        <label for="nik">NIK</label><span class="text-red"> *</span>
                        <input wire:model.lazy="nik" type="text" class="form-control @error('nik') is-invalid @enderror" id="nik" required>
                        @error('nik')
                            <span class="invalid-feedback">
                                <strong>{{$message}}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="row-6 mb-3">
                        <label for="nama">Nama Lengkap</label><span class="text-red"> *</span>
                        <input wire:model.lazy="nama" type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" required>
                        @error('nama')
                            <span class="invalid-feedback">
                                <strong>{{$message}}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="row-6 mb-3">
                        <label for="tempat_lahir">Tempat Lahir</label><span class="text-red"> *</span>
                        <input wire:model.lazy="tempat_lahir" type="tempat_lahir" class="form-control text-capitalize @error('tempat_lahir') is-invalid @enderror " id="tempat_lahir" required>
                        @error('tempat_lahir')
                            <span class="invalid-feedback">
                                <strong>{{$message}}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="row-6 mb-3" wire:key="tlkeluarga">
                        <label for="tanggal_lahir">Tanggal Lahir</label><span class="text-red"> *</span>
                        <div wire:ignore>
                            <div class="input-group date " id="datepicker" data-target-input="nearest">
                                <input name="tanggal_lahir" id="tanggal_lahir" data-toggle="datetimepicker" type="text" class="tanggal form-control @error('tanggal_lahir') is-invalid @enderror" data-target="#datepicker" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask  name="tanggal-l" required/>
                                <div class="input-group-append" data-target="#datepicker" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row-6 mb-3">
                        <label for="jenis_kelamin">Jenis Kelamin</label><span class="text-red"> *</span>
                        <select wire:model.lazy="jenis_kelamin" class="custom-select @error('jenis_kelamin') is-invalid @enderror" id="jenis_kelamin" required>
                            <option selected value="">Pilih jenis kelamin</option>
                            <option value="Laki-Laki">Laki-Laki</option>
                            <option value="Perempuan">Perempuan</option>
                        </select>
                        @error('jenis_kelamin')
                            <span class="invalid-feedback">
                                <strong>{{$message}}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group col">
                    <div class="row-6 mb-3">
                        <label for="alamat">Alamat</label><span class="text-red"> *</span>
                        <textarea wire:model.lazy="alamat" type="textarea" class="form-control @error('alamat') is-invalid @enderror" id="alamat" style=" height: 121px;"></textarea required>
                        @error('alamat')
                            <span class="invalid-feedback">
                                <strong>{{$message}}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="row-6 mb-3">
                        <label for="agama">Agama</label><span class="text-red"> *</span>
                        <select wire:model.lazy="agama" class="custom-select @error('agama') is-invalid @enderror" id="agama" required>
                            <option selected value="">Pilih agama</option>
                            <option value="Islam">Islam</option>
                            <option value="Kristen">Kristen</option>
                            <option value="Katolik">Katolik</option>
                            <option value="Hindu">Hindu</option>
                            <option value="Budha">Budha</option>
                            <option value="Konghucu">Konghucu</option>
                            <option value="Lainnya">Lainnya</option>
                        </select>
                        @error('agama')
                            <span class="invalid-feedback">
                                <strong>{{$message}}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="row-6 mb-3">
                        <label for="pekerjaan_id">Pekerjaan</label><span class="text-red"> *</span>
                        <select wire:model.lazy="pekerjaan_id" class="custom-select @error('pekerjaan_id') is-invalid @enderror" id="pekerjaan_id" required>
                            <option selected value="">Pilih pekerjaan</option>
                            @foreach ($daftar_pekerjaan as $pekerjaan)
                                <option value="{{$pekerjaan->id}}">{{$pekerjaan->nama}}</option>
                            @endforeach
                        </select>
                        @error('pekerjaan_id')
                            <span class="invalid-feedback">
                                <strong>{{$message}}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="row-6 mb-3">
                        <label for="hubungan_id">Hubungan</label><span class="text-red"> *</span>
                        <select wire:model.lazy="hubungan_id" class="custom-select @error('hubungan_id') is-invalid @enderror" id="hubungan_id" required>
                            <option selected value="">Pilih hubungan</option>
                            @foreach ($daftar_hubungan as $hubungan)
                                <option value="{{$hubungan->id}}">{{$hubungan->nama}}</option>
                            @endforeach
                        </select>
                        @error('hubungan_id')
                            <span class="invalid-feedback">
                                <strong>{{$message}}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="row-6 text-right">
                        <button type="submit" wire:loading.attr="disabled" class="btn btn-primary">
                            <span wire:loading.remove>Tambah</span>
                            <span wire:loading>
                                <i class="fa fa-sync fa-spin" aria-hidden="true"></i>
                            </span>
                        </button>
                        <a href="../" class="btn btn-secondary">Batal</a>
                    </div> 
                </div>     
            </div>
        </form>
    </div>
</div>

@push('scripts')
    <script type="application/javascript">
        $( document ).ready(function(){
            jQuery.noConflict();
            // Date range picker
            $('#datepicker').datetimepicker({
            format: 'DD/MM/YYYY'
            });
            $('.tanggal').on('blur', function (ev){
                @this.set(ev.target.id, ev.target.value);
            });
            // Datemask dd/mm/yyyy
            $('[data-mask]').inputmask();
        })
    </script>
@endpush