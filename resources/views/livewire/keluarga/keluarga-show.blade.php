@section('title', 'Detail Keluarga')

@section('header', 'Detail Keluarga')

@section('breadcumb')
    @can('isAdmin')
        <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="fa fa-home" aria-hidden="true"></i>                    </a></li>
        <li class="breadcrumb-item"><a href="{{route('pegawai')}}">Pegawai</a></li>
    @endcan
    <li class="breadcrumb-item"><a href="{{route('pegawai.show', $pegawai)}}">Detail Pegawai</a></li>
    <li class="breadcrumb-item active">Detail Keluarga</li>
@endsection

{{-- content --}}
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Detail Keluarga</h3>
        <div class="card-tools">
            <a href="{{route('keluarga.update', [$pegawai, $keluarga])}}" class="btn btn-sm btn-success"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit"><path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path><path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path></svg>
            </a>
            <button data-toggle="modal" data-target="#deletemodal{{$keluarga->id}}" class="btn btn-sm btn-danger">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
            </button>
            <div class="modal fade" id="deletemodal{{$keluarga->id}}" tabindex="-1" aria-labelledby="deletemodalModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-md">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="deletemodalModalLabel">Konfirmasi</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            Apakah anda yakin?
                        </div>
                        <div class="modal-footer">
                            <button type="button" wire:click="destroy(true)" class="btn btn-danger" data-dismiss="modal">Hapus</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card-body"> 
        @if (session()->has('pesan'))
        <div class="alert alert-success">
            {{ session('pesan') }}
        </div>
        @endif
        {{-- {{ dd($keluarga) }} --}}
        <table class="table table-borderless ">
            <tbody>
                {{-- {{dd($keluarga->nik)}} --}}
                <tr>
                    <td style="width: 18%">NIK</td>
                    <td>: {{ $keluarga->nik }}</td>
                </tr>
                <tr>
                    <td>Nama Pegawai</td>
                    <td>: {{ $keluarga->nama }}</td>
                </tr>
                <tr>
                    <td>Tempat & Tanggal Lahir</td>
                    <td class="text-capitalize">: {{ $keluarga->tempat_lahir }}, {{ \Carbon\Carbon::createFromFormat('d/m/Y', $keluarga->tanggal_lahir)->isoFormat('D MMMM Y') }}</td>
                </tr>
                <tr>
                    <td>Jenis Kelamin</td>
                    <td>: {{ $keluarga->jenis_kelamin }}</td>
                </tr>
                <tr>
                    <td>Alamat</td>
                    <td>: {{ $keluarga->alamat }}</td>
                </tr>
                <tr>
                    <td>Agama</td>
                    <td>: {{ $keluarga->agama }}</td>
                </tr>
                <tr>
                    <td>Pekerjaan</td>
                    <td>: {{ $keluarga->pekerjaan->nama }}</td>
                </tr>
                <tr>
                    <td>Hubungan</td>
                    <td>: {{ $keluarga->hubungan->nama }}</td>    
                </tr>
            </tbody>
        </table>
    </div>
</div>
{{-- ./content --}}
