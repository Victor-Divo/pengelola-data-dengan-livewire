@section('title', 'Profil Akun')

@section('header', 'Profil Akun')

@section('breadcumb')
    @can('isAdmin')
        <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="fa fa-home" aria-hidden="true"></i>                    </a></li>
    @endcan
    <li class="breadcrumb-item active">Profil Akun</li>
@endsection

@section('styles')
    <style>
        #hapus {
            background-color: #eb4b4b;
            box-shadow:  8px 8px 17px rgba(0,0,0,.125), 0 1px 3px rgba(0,0,0,.2);
        }

        .card{
            box-shadow: 8px 10px 17px rgba(0,0,0,.125), 0 3px 3px rgba(0,0,0,.2)
        }

        #hapus:hover{
            background-color: #cf3f3f;
        }
        #username:focus, #password:focus{
            box-shadow:none;
        }
    </style>
@endsection

{{-- content --}}
<div class="container-fluid">
    <div class="row">
        <div class="col-md-9 mx-auto">
            <div class="card text-white" style="background-color:#3a87cf">
                <div class="card-header border-0" style="background-color: transparent; ">
                    <h3 class="card-title mb-0" style="line-height:1.5rem">Informasi Akun</h3>
                    <div class="card-tools">
                    </div>
                </div>
                
                <div class="card-body pt-0">
                    <table class="table table-sm rounded table-borderless ">
                        <tbody>
                            <tr>
                                <td style="width: 10%">Nama</td>
                                <td>: {{ $user->pegawai->nama }}</td>    
                            </tr>
                            <tr>
                                <td>Username</td>
                                <td>: {{ $user->username }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
    <div class="row">
        <div class="col-md-9 mx-auto">
            <div class="card">
                <div class="card-header border-0 background-color-transparent">
                    <h3 class="card-title mb-0" style="line-height:1.5rem">Ubah Profil</h3>
                    <div class="card-tools">
                    </div>
                </div>
                <div class="card-body">
                    @if (session()->has('profil'))
                        <div class="alert alert-success">
                            {{session('profil')}}
                        </div>
                    @endif
                    <form wire:submit.prevent="changeProfile">
                        @csrf
                        <div class="form-group row d-flex">
                            <div class="col-md-12" style="">
                                <div class="input-group">
                                    <input wire:model="username" id="username" type="text" style="border-right:0;" class="form-control @error('username') is-invalid @enderror" name="username" placeholder="Username Baru" autocomplete="current-username">
                                    <div class="input-group-append">
                                        <div class="input-group-text btn" style="background-color: white; padding : 0px 6px; border-left:0;">
                                            <span class="far fa-user"></span>
                                        </div>
                                    </div>
                                    @error('username')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="form-group row d-flex">
                            <div class="col-md-12" style="">
                                <div class="input-group">
                                    <input wire:model.debounce.750ms="old_password" id="old_password" type="password" style="border-right:0;" class="form-control @error('old_password') is-invalid @enderror" name="old_password" placeholder="Old Password" autocomplete="current-old_password">
                                    <div class="input-group-append">
                                        <div wire:ignore class="input-group-text btn"  id="toggleOldPassword" style="background-color: white; padding : 0px 6px; border-left:0;">
                                            <span class="far fa-eye"></span>
                                        </div>
                                    </div>
                                    @error('old_password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group row d-flex">
                            <div class="col-md-12" style="">
                                <div class="input-group">
                                    <input wire:model.debounce.750ms="password" id="password" value="" type="password" style="border-right:0;" class="form-control pwbosq @error('password') is-invalid @enderror" name="password" placeholder="New Password">
                                    <div class="input-group-append">
                                        <div class="input-group-text btn" id="toggleNewPassword" style="background-color: white; padding : 0px 6px; border-left:0;">
                                            <span class="far fa-eye" ></span>
                                        </div>
                                    </div>
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="form-group row d-flex">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col d-flex justify-content-start">
                                        <input wire:model.debounce.750ms="password_confirmation" id="password_confirmation" type="password" class="form-control pwbosq  @error('password_confirmation') is-invalid @enderror" name="password_confirmation" placeholder="Confirm New Password">
                                    </div>
                                </div>
                                @if($errors->has('password_confirmation'))
                                    <span style="font-size: 80%; color: #e3342f;"><strong>{{ $errors->first('password_confirmation') }}</strong></span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row d-flex">
                            <div class="col-md-12 text-left">
                                <button type="submit" wire:loading.attr="disabled" wire:target="username, password, old_password, password_confirmation" class="btn btn-primary btn-block">
                                    <span wire:loading.remove wire:target="username, password, old_password, password_confirmation">Ubah Profil</span>
                                    <span wire:loading wire:target="username, password, old_password, password_confirmation">
                                        <i class="fa fa-sync fa-spin" aria-hidden="true"></i>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
</div>

@push('scripts')
<script type="application/javascript">
    $(function(){
        const toggleOldPassword = document.querySelector('#toggleOldPassword');
        const old_password = document.querySelector('#old_password');
        toggleOldPassword.addEventListener('click', function (e) {
            // toggle the type attribute
            const type = old_password.getAttribute('type') === 'password' ? 'text' : 'password';
            old_password.setAttribute('type', type);
            // toggle the eye slash icon
            this.children[0].classList.toggle('fa-eye-slash');
        });
        const toggleNewPassword = document.querySelector('#toggleNewPassword');
        const password = document.querySelectorAll('.pwbosq');
        toggleNewPassword.addEventListener('click', function (e) {
            password.forEach(function(e) {
                // toggle the type attribute
                const type = e.getAttribute('type') === 'password' ? 'text' : 'password';
                e.setAttribute('type', type);
            });
            // toggle the eye slash icon
            this.children[0].classList.toggle('fa-eye-slash');
        });
    });
</script>
@endpush