@section('title', 'Pegawai')

@section('header', 'Daftar Pegawai')

@section('breadcumb')
    <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="fa fa-home" aria-hidden="true"></i>                    </a></li>
    <li class="breadcrumb-item"><a href="{{route('pegawai')}}">Pegawai</a></li>
    <li class="breadcrumb-item active">Daftar Pegawai</li>
@endsection

{{-- content --}}
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Daftar Pegawai</h3>
            <div class="card-tools">
                @auth
                <livewire:import-modal key="'importExcelModal'">
                <a href="{{route('export')}}" target="_blank" wire:click="export" class="btn btn-sm btn-success" style=""><i class="fa fa-file-excel-o" aria-hidden="true"></i>export</a>
                <a href="{{ route('pegawai.create') }}" class="btn btn-sm btn-primary"><i class="fa fa-plus-square" aria-hidden="true"></i> Tambah Pegawai</a>
                @endauth
            </div>
        </div>
        <div class="card-body">
            @if (session()->has('pesan'))
            <div class="alert alert-success" id="pesan"> 
                {{ session('pesan') }}
            </div>
            @elseif (session()->has('pesan-error'))
            <div class="alert alert-danger" id="pesan"> 
                {{ session('pesan-error') }}
            </div>
            @endif
            
            <div wire:ignore wire:key="pegtabell">
                <table class="table table-bordered table-responsive-md table-hover mx-auto" id="pegtable" width="100%">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nama</th>
                            <th scope="col">NIP</th>
                            <th scope="col">Jabatan</th>
                            <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->  
{{-- /.content --}}

@push('scripts')
<script type="application/javascript">
        $(document).ready(function(){
            jQuery.noConflict();
            livewire.on('notified', () => {
                $(".alert").fadeTo(5000, 500).slideUp(500, function(){
                    $(".alert").slideUp(500);
                });
            });
            // const comp = @th;
            document.querySelector('#pegtable');
            let pegtable =  $('#pegtable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                stateSave: true,
                order: [[1,'asc']],
                ajax: '{{ route('ajax.get.data.pegawai') }}',
                columns: [
                    { data: 'DT_RowIndex', class: 'text-center', width: '20px', orderable: false, searchable: false },                    
                    { data: 'nama', name: 'nama', width: '250px' },
                    { data: 'nip', name: 'nip', width: '200px' },
                    { data: 'jabatan_id', name: 'jabatan_id', class: 'text-center', width: '100px' },
                    { data: 'aksi', name: 'aksi', orderable: false, searchable: false, class: 'text-center', width: '50px' },
                    
                ],
                // "dom": '<"toolbar">frtip',
                // fnInitComplete: function(){
                // $('div.toolbar').html('<a href="{{route('export')}}" target="_blank" wire:click="export" class="btn btn btn-light mb-2" style=""><strong class="">Export to excel</strong></a>');
                // }
            });

            document.addEventListener("turbolinks:before-cache", function() {
                if (pegtable != null) {
                    pegtable.destroy();
                    pegtable = null;
                }
            });
            
            // document.addEventListener('turbolinks:load', function () {
            //     const dtlength = document.querySelectorAll('.dataTables_length')
            //     if ( dtlength.length != 0) {
            //         while (dtlength.length != 0) {
            //             // element.parentNode.removeChild(element);
            //             console.log(dtlength.length);
            //         }
            //     }
            // });
        });
    </script>
@endpush