<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>CV Pegawai</title>
    <link rel="icon" href="{{ asset('img/logo.png')}}">
    <!-- Bootstrap CSS -->
    {{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous"> --}}
    <link rel="stylesheet" href="{{ asset('adminlte/dist/css/adminlte.min.css') }}">
    
    <style>
        @page {
            margin: 0cm 0cm;
        }

        * {
            /* border: 1px solid #000000; */
        }

        body{
            position: fixed;
            top: 4cm ;
            right: 2cm ;
            bottom: 3cm ;
            left: 2cm ;
        }

        header {
            position: fixed;
            top: 1cm;
            left: 0cm;
            right: 0cm;
            height: 2cm;

            /** Extra personal styles **/
            /* background-color: #03a9f4;
            color: white;
            text-align: center;
            line-height: 1.5cm; */
        }

        /** Define the footer rules **/
        footer {
            position: fixed; 
            bottom: 1cm; 
            left: 0cm; 
            right: 0cm;
            height: 2cm;

            /** Extra personal styles **/
            /* background-color: #03a9f4;
            color: white;
            text-align: center;
            line-height: 1.5cm; */
        }

        .main table {
            font-size: 12pt;
            font-family: TimesNewRoman, "Times New Roman", Times, Baskerville, Georgia, serif;
        }
        h3 {
            width:170px ;
            font-size: 20pt;
            margin: 0 auto;
            /* text-align: center; */
            font-family: TimesNewRoman, "Times New Roman", Times, Baskerville, Georgia, serif;
        }

        h4 {
            width:auto ;
            font-size: 16pt;
            /* margin: 0 auto; */
            /* text-align: center; */
            font-family: TimesNewRoman, "Times New Roman", Times, Baskerville, Georgia, serif;
        }

        table.table-bordered{
            border:1px solid black;
            margin-top:20px;
        }
        table.table-bordered > thead > tr > th{
            border:1px solid black;
        }
        table.table-bordered > tbody > tr > td{
            border:1px solid black;
        }
        * {
            /* border: 1px solid #4f4f4f; */
        }
    </style>
  </head>
  <body>
    <header>
        {{-- <div style="background-color: #3b78b5">      --}}
            <img src="{{ asset('img/header.png') }}" style="width: 100%; height:90%" alt="">
        {{-- </div> --}}
    </header>

    <footer>
        <img class="float-right" src="{{ asset('img/footer.png') }}" style="width: 25%;height:50%" alt="">
    </footer>
    <h3>Detail Pegawai</h3>
    <br>
    <div class="main">
        {{-- {{dd($pegawai)}} --}}
        <div class="row">
            <div class="col-md-10">
                <h4>Data Diri</h4>
                <table class="table-borderless" style="width: 88%; table-layout:fixed; ">
                    <thead>
                        <tr>
                            <th style="width: 44%"></th>
                            <th style="width: 2%"></th>
                            <th ></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>NIP</td>
                            <td>:</td>
                            <td>{{ $pegawai->nip ?? '-'}}</td>    
                        </tr>
                        <tr>
                            <td>Nama</td>
                            <td>:</td>
                            <td>{{ $pegawai->nama ?? '-'}}</td>
                        </tr>
                        <tr>
                            <td>Tempat, Tanggal Lahir</td>
                            <td>:</td>
                            <td class="text-capitalize">{{ $pegawai->tempat_lahir ?? '-'}}, {{ \Carbon\Carbon::createFromFormat('d/m/Y', $pegawai->tanggal_lahir)->isoFormat('D MMMM Y') ?? '-'}}</td>
                        </tr>
                        <tr>
                            <td style="vertical-align: baseline">Alamat</td>
                            <td style="vertical-align: baseline">:</td>
                            <td style="vertical-align: baseline">{{$pegawai->alamat ?? '-'}}</td>
                        </tr>
                        <tr>
                            <td>Jenis Kelamin</td>
                            <td>:</td>
                            <td>{{ $pegawai->jenis_kelamin ?? '-'}}</td>
                        </tr>
                        <tr>
                            <td>Agama</td>
                            <td>:</td>
                            <td>{{ $pegawai->agama ?? '-'}}</td>
                        </tr>
                        <tr>
                            <td>Status Perkawinan</td>
                            <td>:</td>
                            <td>{{ $pegawai->statusPerkawinan->nama ?? '-'}}</td>    
                        </tr>
                        <tr>
                            <td>Golongan Darah</td>
                            <td>:</td>
                            <td>{{ $pegawai->golongan_darah ?? '-'}}</td>
                        </tr>
                        <tr>
                            <td>Pendidikan Terakhir</td>
                            <td>:</td>
                            <td>{{ $pegawai->pendidikan->nama ?? '-'}}</td>   
                        </tr>
                        <tr>
                            <td>Departemen</td>
                            <td>:</td>
                            <td>{{ $pegawai->departemen->nama ?? '-'}}</td>
                        </tr>
                        <tr>
                            <td>Jabatan</td>
                            <td>:</td>
                            <td>{{ $pegawai->jabatan->nama ?? '-'}}</td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>:</td>
                            <td>{{ $pegawai->email ?? '-'}}</td>    
                        </tr>
                        <tr>
                            <td>Nomor HP</td>
                            <td>:</td>
                            <td>{{ $pegawai->phone ?? '-'}}</td>    
                        </tr>
                        <tr>
                            <td style="vertical-align: baseline">Nomor Rekening</td>
                            <td style="vertical-align: baseline">:</td>
                            <td style="vertical-align: baseline">
                                @foreach ($pegawai->rekening as $rekening)
                                • {{ $rekening->nama ?? '-'}} ({{ $rekening->pivot->no_rek ?? '-'}})
                                <br>
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <td>Nomor KTP</td>
                            <td>:</td>
                            <td>{{ $pegawai->nik ?? '-'}}</td>
                        </tr>
                        <tr>
                            <td>Nomor KK</td>
                            <td>:</td>
                            <td>{{ $pegawai->no_kk ?? '-'}}</td>
                        </tr>
                        <tr>
                            <td>Nomor NPWP</td>
                            <td>:</td>
                            <td>{{ $pegawai->no_npwp ?? '-'}}</td>
                        </tr>
                        <tr>
                            <td>Nomor BPJS Kesehatan</td>
                            <td>:</td>
                            <td>{{ $pegawai->no_bpjs_kes ?? '-'}}</td>
                        </tr>
                        <tr>
                            <td style="wi">Nomor BPJS Ketenagakerjaan</td>
                            <td>:</td>
                            <td>{{ $pegawai->no_bpjs_ket ?? '-'}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <img src="{{empty($pegawai->foto_profil) ? asset('/adminlte/dist/img/avatar5.png') : Storage::path($pegawai->foto_profil)}}" width="25%" class="float-right rounded" style="margin-top:12px;">
        </div>
        <br>
        <br>
        <div style="page-break-before: always;"></div>
        <div class="row" style="">
            <div class="col-md-12">
                <h4>Riwayat Status Kepegawaian</h4>
                <table style="width: 100%;" class="table-bordered text-center">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Jenis</th>
                            <th scope="col">Tanggal Mulai</th>
                            <th scope="col">Tanggal Berakhir</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($pegawai->statusKepegawaian as $stat_kep)
                            <tr>
                                <td scope="row"> {{ $loop->iteration }} </td>
                                <td>{{ $stat_kep->jenis }}</td>
                                <td>{{ \Carbon\Carbon::parse($stat_kep->pivot->tanggal_mulai)->isoFormat('D MMMM Y') }}</td>
                                <td>{{ $stat_kep->pivot->tanggal_berakhir ? \Carbon\Carbon::parse($stat_kep->pivot->tanggal_berakhir)->isoFormat('D MMMM Y') : '-'}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>
