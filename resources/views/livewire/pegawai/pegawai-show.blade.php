@section('title', 'Detail Pegawai')

@section('header', 'Detail Pegawai')

@section('breadcumb')
    @can('isAdmin')
        <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="fa fa-home" aria-hidden="true"></i>                    </a></li>
        <li class="breadcrumb-item"><a href="{{route('pegawai')}}">Pegawai</a></li>
    @endcan
    <li class="breadcrumb-item active">Detail Pegawai</li>
@endsection

@section('styles')
    <style>
        /* Ensure the size of the image fit the container perfectly */
        #image {
        display: block;

        /* This rule is very important, please don't ignore this */
        max-width: 100%;
        }

        #data-pegawai td{
            /* vertical-align: middle; */
        }

        .file-container {
            position: relative;
            width: 10rem;
            border-radius: 0.5rem;
        }

        .gambar {
            display: block;
            border-radius: 0.5rem;
        }

        .c-overlay {
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            height: 100%;
            width: 100%;
            opacity: 0;
            transition: .5s ease;
            background-color: #000;
            border-radius: 0.5rem;
        }

        .file-container:hover .c-overlay {
            opacity: 0.7;
        }

        .teks {
            color: white;
            font-size: 20px;
            position: absolute;
            top: 50%;
            left: 50%;
            -webkit-transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
            text-align: center;
        }
    </style>    
@endsection

{{-- content --}}
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8">
            <div class="card" wire:key="detail-pegawai-card">
                <div class="card-header">
                    <h3 class="card-title">Detail Pegawai</h3>
                    <div class="card-tools">
                        <button wire:click='getPegawai({{$pegawai->id}})' class="btn btn-success mr-1 mb-1 btn-sm">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit"><path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path><path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path></svg>
                        </button>
                        @can('isAdmin')
                        <button data-toggle="modal" data-target="#deletemodal{{$pegawai->id}}" id="delete" class="btn btn-danger mr-1 mb-1 btn-sm">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                        </button>
                        <div class="modal fade" id="deletemodal{{$pegawai->id}}" tabindex="-1" aria-labelledby="deletemodalModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-md">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="deletemodalModalLabel">Konfirmasi</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Apakah anda yakin?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" wire:click="destroy('{{ $pegawai->id }}', true)" class="btn btn-danger" data-dismiss="modal">Hapus</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endcan
                        <a href="{{ route('pegawai.print', $pegawai) }}" target='_blank' class="btn btn-warning mr-1 mb-1 btn-sm">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-printer"><polyline points="6 9 6 2 18 2 18 9"></polyline><path d="M6 18H4a2 2 0 0 1-2-2v-5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-2"></path><rect x="6" y="14" width="12" height="8"></rect>
                            </svg>
                        </a>
                    </div>
                </div>
                
                <div class="card-body">
                    @if (session()->has('pesan'))
                        <div class="alert alert-success">
                            {{ session('pesan') }}
                        </div>
                    @endif
                    <table class="table table-sm rounded table-borderless" id="data-pegawai">
                        <thead>
                        <tr>
                            <th scope="col" style="width: 35%"></th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>NIP</td>
                                <td>:</td>
                                <td>{{ $pegawai->nip ?? '-' }}</td>    
                            </tr>
                            <tr>
                                <td>Nama Pegawai</td>
                                <td>:</td>
                                <td>{{ $pegawai->nama ?? '-' }}</td>
                            </tr>
                            <tr>
                                <td>Tempat & Tanggal Lahir</td>
                                <td>:</td>
                                <td class="text-capitalize">{{ $pegawai->tempat_lahir ?? '-' }}, {{ $pegawai->tanggal_lahir ? \Carbon\Carbon::createFromFormat('d/m/Y', $pegawai->tanggal_lahir)->isoFormat('D MMMM Y') : '-'}}</td>
                            </tr>
                            <tr>
                                <td>Alamat</td>
                                <td>:</td>
                                <td>{{ $pegawai->alamat ?? '-' }}</td>
                            </tr>
                            <tr>
                                <td>Jenis Kelamin</td>
                                <td>:</td>
                                <td>{{ $pegawai->jenis_kelamin ?? '-' }}</td>
                            </tr>
                            <tr>
                                <td>Agama</td>
                                <td>:</td>
                                <td>{{ $pegawai->agama ?? '-' }}</td>
                            </tr>
                            <tr>
                                <td>Status Perkawinan</td>
                                <td>:</td>
                                <td>{{ $pegawai->statusPerkawinan->nama ?? '-' }}</td>    
                            </tr>
                            <tr>
                                <td>Golongan Darah</td>
                                <td>:</td>
                                <td>{{ $pegawai->golongan_darah ?? '-' }}</td>
                            </tr>
                            <tr>
                                <td>Pendidikan Terakhir</td>
                                <td>:</td>
                                <td> {{ $pegawai->pendidikan->nama ?? '-' }}</td>   
                            </tr>
                            <tr>
                                <td>NIK</td>
                                <td>:</td>
                                <td>{{ $pegawai->nik ?? '-' }}</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>:</td>
                                <td>
                                    @if (isset($pegawai->file_ktp))
                                        <div class="file-container">
                                            <img src="{{ route('preview', [
                                                'pegawai' => $pegawai,
                                                'folder' => Str::before($pegawai->file_ktp, '/'),
                                                'file' => Str::after($pegawai->file_ktp, '/')
                                            ]) }}" class="gambar" style="width: 10rem;height:6rem;" alt="User Image">  
                                            <div class="c-overlay">
                                                <a style="display: block; margin:2rem; opacity:1" class="text btn btn-sm btn-outline-primary"
                                                href="{{route('preview', [
                                                    'pegawai' => $pegawai,
                                                    'folder' => Str::before($pegawai->file_ktp, '/'),
                                                    'file' => Str::after($pegawai->file_ktp, '/')
                                                    ])}}"
                                                target="_blank">
                                                    Lihat
                                                </a>
                                            </div>
                                        </div>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>Nomor KK</td>
                                <td>:</td>
                                <td>{{ $pegawai->no_kk ?? '-'}}</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>:</td>
                                <td>
                                    @if (isset($pegawai->file_kk))
                                        {{-- @if ($this->imageCheck($pegawai->file_kk)) --}}
                                            <div class="file-container">
                                                <img src="{{ route('preview', [
                                                    'pegawai' => $pegawai,
                                                    'folder' => Str::before($pegawai->file_kk, '/'),
                                                    'file' => Str::after($pegawai->file_kk, '/')
                                                ]) }}" class="gambar" style="width: 10rem;height:6rem;" alt="User Image">  
                                                <div class="c-overlay">
                                                    <a style="display: block; margin:2rem; opacity:1" class="text btn btn-sm btn-outline-primary"
                                                    href="{{route('preview', [
                                                        'pegawai' => $pegawai,
                                                        'folder' => Str::before($pegawai->file_kk, '/'),
                                                        'file' => Str::after($pegawai->file_kk, '/')
                                                        ])}}"
                                                    target="_blank">
                                                        Lihat
                                                    </a>
                                                </div>
                                            </div>
                                        {{-- @else 
                                            <a href="{{route('preview', [
                                                'pegawai' => $pegawai,
                                                'folder' => Str::before($pegawai->file_kk, '/'),
                                                'file' => Str::after($pegawai->file_kk, '/')
                                                ])}}"
                                            target="_blank">
                                                {{Str::after($pegawai->file_kk, '/')}}
                                            </a> 
                                        @endif --}}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>Nomor NPWP</td>
                                <td>:</td>
                                <td>{{ $pegawai->no_npwp ?? '-' }}</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>:</td>
                                <td>          
                                    @if (isset($pegawai->file_npwp))
                                        {{-- @if ($this->imageCheck($pegawai->file_npwp)) --}}
                                            <div class="file-container">
                                                <img src="{{ route('preview', [
                                                    'pegawai' => $pegawai,
                                                    'folder' => Str::before($pegawai->file_npwp, '/'),
                                                    'file' => Str::after($pegawai->file_npwp, '/')
                                                ]) }}" class="gambar" style="width: 10rem;height:6rem;" alt="User Image">  
                                                <div class="c-overlay">
                                                    <a style="display: block; margin:2rem; opacity:1" class="text btn btn-sm btn-outline-primary"
                                                    href="{{route('preview', [
                                                        'pegawai' => $pegawai,
                                                        'folder' => Str::before($pegawai->file_npwp, '/'),
                                                        'file' => Str::after($pegawai->file_npwp, '/')
                                                        ])}}"
                                                    target="_blank">
                                                        Lihat
                                                    </a>
                                                </div>
                                            </div>
                                        {{-- @else 
                                            <a href="{{route('preview', [
                                                'pegawai' => $pegawai,
                                                'folder' => Str::before($pegawai->file_npwp, '/'),
                                                'file' => Str::after($pegawai->file_npwp, '/')
                                                ])}}"
                                            target="_blank">
                                                {{Str::after($pegawai->file_npwp, '/')}}
                                            </a> 
                                        @endif --}}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>Nomor BPJS Kesehatan</td>
                                <td>:</td>
                                <td>{{ $pegawai->no_bpjs_kes ?? '-' }}</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>:</td>
                                <td>
                                    @if (isset($pegawai->file_bpjs_kes))
                                        {{-- @if ($this->imageCheck($pegawai->file_bpjs_kes)) --}}
                                            <div class="file-container">
                                                <img src="{{ route('preview', [
                                                    'pegawai' => $pegawai,
                                                    'folder' => Str::before($pegawai->file_bpjs_kes, '/'),
                                                    'file' => Str::after($pegawai->file_bpjs_kes, '/')
                                                ]) }}" class="gambar" style="width: 10rem;height:6rem;" alt="User Image">  
                                                <div class="c-overlay">
                                                    <a style="display: block; margin:2rem; opacity:1" class="text btn btn-sm btn-outline-primary"
                                                    href="{{route('preview', [
                                                        'pegawai' => $pegawai,
                                                        'folder' => Str::before($pegawai->file_bpjs_kes, '/'),
                                                        'file' => Str::after($pegawai->file_bpjs_kes, '/')
                                                        ])}}"
                                                    target="_blank">
                                                        Lihat
                                                    </a>
                                                </div>
                                            </div>
                                        {{-- @else 
                                            <a href="{{route('preview', [
                                                'pegawai' => $pegawai,
                                                'folder' => Str::before($pegawai->file_bpjs_kes, '/'),
                                                'file' => Str::after($pegawai->file_bpjs_kes, '/')
                                                ])}}"
                                            target="_blank">
                                                {{Str::after($pegawai->file_bpjs_kes, '/')}}
                                            </a> 
                                        @endif --}}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>Nomor BPJS Ketenagakerjaan</td>
                                <td>:</td>
                                <td>{{ $pegawai->no_bpjs_ket ?? '-' }}</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>:</td>
                                <td>
                                    @if (isset($pegawai->file_bpjs_ket))
                                        {{-- @if ($this->imageCheck($pegawai->file_bpjs_ket)) --}}
                                            <div class="file-container">
                                                <img src="{{ route('preview', [
                                                    'pegawai' => $pegawai,
                                                    'folder' => Str::before($pegawai->file_bpjs_ket, '/'),
                                                    'file' => Str::after($pegawai->file_bpjs_ket, '/')
                                                ]) }}" class="gambar" style="width: 10rem;height:6rem;" alt="User Image">  
                                                <div class="c-overlay">
                                                    <a style="display: block; margin:2rem; opacity:1" class="text btn btn-sm btn-outline-primary"
                                                    href="{{route('preview', [
                                                        'pegawai' => $pegawai,
                                                        'folder' => Str::before($pegawai->file_bpjs_ket, '/'),
                                                        'file' => Str::after($pegawai->file_bpjs_ket, '/')
                                                        ])}}"
                                                    target="_blank">
                                                        Lihat
                                                    </a>
                                                </div>
                                            </div>
                                        {{-- @else 
                                            <a href="{{route('preview', [
                                                'pegawai' => $pegawai,
                                                'folder' => Str::before($pegawai->file_bpjs_ket, '/'),
                                                'file' => Str::after($pegawai->file_bpjs_ket, '/')
                                                ])}}"
                                            target="_blank">
                                                {{Str::after($pegawai->file_bpjs_ket, '/')}}
                                            </a> 
                                        @endif --}}
                                    @endif    
                                </td>
                            </tr>
                            @if (count($pegawai->statusKepegawaian) > 0)
                                <tr>
                                    <td>Status Kepegawaian</td>
                                    <td>:</td>
                                    <td>
                                        {{$pegawai->statusKepegawaian->last()->jenis}}
                                        <span class="ml-1">
                                            <button type="button" class="btn btn-sm btn-outline-primary" data-toggle="modal" data-target="#statpeg">
                                                Detail
                                            </button>
                                        </span>
                                        <!-- Modal -->
                                        <div wire:ignore.self class="modal fade" id="statpeg" tabindex="-1" aria-labelledby="statpegModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                <h5 class="modal-title" id="statpegModalLabel">Riwayat Status Kepegawaian</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                </div>
                                                <div class="modal-body">
                                                    <table class="table table-borderless " style="text-align:center;">
                                                        <thead class="thead-light">
                                                        <tr>
                                                            <th scope="col">#</th>
                                                            <th scope="col">Jenis</th>
                                                            <th scope="col">Tanggal Mulai</th>
                                                            <th scope="col">Tanggal Berakhir</th>
                                                            @can('isAdmin')
                                                                <th scope="col" style="width: 17rem">Aksi</th>
                                                            @endcan
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                            @forelse ($pegawai->statusKepegawaian as $stat_kep)
                                                                <tr>
                                                                    <th scope="row"> {{ $loop->iteration }} </th>
                                                                    <td>{{ $stat_kep->jenis }}</td>
                                                                    <td>{{ \Carbon\Carbon::parse($stat_kep->pivot->tanggal_mulai)->isoFormat('D MMMM Y') }}</td>
                                                                    <td>{{ $stat_kep->pivot->tanggal_berakhir ? \Carbon\Carbon::parse($stat_kep->pivot->tanggal_berakhir)->isoFormat('D MMMM Y') : '-'}}</td>
                                                                    @can('isAdmin')
                                                                        <td>
                                                                            @if ($stat_kep->pivot->tanggal_berakhir)
                                                                                @if($selected === $stat_kep->pivot->status_kepegawaian_id)
                                                                                    {{-- <form class="form-inline"> --}}
                                                                                        <input style="width: 10rem; display: inline-block;" type="number" class="form-control form-control-sm" placeholder="Masukkan Nomor Surat" id="penomoran" wire:model.debounce.500ms="penomoran" required>
                                                                                        {{-- @error('penomoran') <span class="text-danger error">{{ $message }}</span>@enderror --}}
                                                                                        <span style="{{$penomoran ? '' : 'cursor:not-allowed'}}">
                                                                                            <a class="{{$penomoran ? '' : 'disabled'}} btn btn-sm bg-teal" 
                                                                                            href="{{route('pegawai.print.letter', [
                                                                                                'pegawai' => $pegawai, 
                                                                                                'penomoran' => $penomoran ?? '000', 
                                                                                                'jenis' => $stat_kep->pivot->status_kepegawaian_id
                                                                                                ])}}" target="_blank"
                                                                                            >Cetak</a>
                                                                                        </span>
                                                                                        <button wire:click="confirmPrint('0')" class="btn btn-sm btn-secondary">X</button>
                                                                                    {{-- </form> --}}
                                                                                @else
                                                                                    @if ($cetakbtn === $stat_kep->pivot->status_kepegawaian_id)
                                                                                        <button wire:click="confirmPrint({{ $stat_kep->pivot->status_kepegawaian_id }})" class="btn btn-sm bg-teal py-1">No. Custom</button>
                                                                                        <a href= "{{route('pegawai.print.letter', [
                                                                                            'pegawai' => $pegawai, 
                                                                                            'penomoran' => $no_surat, 
                                                                                            'jenis' => $stat_kep->pivot->status_kepegawaian_id
                                                                                            ])}}" wire:click="suratTercetak" class="btn btn-sm bg-teal py-1"
                                                                                            target="_blank">
                                                                                            No. Sesuai Urutan
                                                                                        </a>
                                                                                        <button wire:click="confirmSelected('0')" class="btn btn-sm btn-secondary py-1"><i class="fas fa-arrow-left"></i></button>
                                                                                    @else 
                                                                                        <button wire:click="confirmSelected({{ $stat_kep->pivot->status_kepegawaian_id }})" class="btn btn-sm bg-teal py-1">Cetak</button>
                                                                                    @endif
                                                                                @endif
                                                                            @endif
                                                                        </td>
                                                                    @endcan
                                                                </tr>
                                                            @empty
                                                            <tr>
                                                                <td colspan="5" class="text-center">
                                                                    Tidak ada riwayat status kepegawaian dari pegawai ini
                                                                </td>
                                                            </tr>
                                                            @endforelse
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endif
                            <tr>
                                <td>Departemen</td>
                                <td>:</td>
                                <td>{{ $pegawai->departemen->nama  ?? '-'}}</td>
                            </tr>
                            <tr>
                                <td>Jabatan</td>
                                <td>:</td>
                                <td>{{ $pegawai->jabatan->nama  ?? '-'}}</td>
                            </tr>
                            <tr>
                                <td>Masa Kerja</td>
                                <td>:</td>
                                <td>{{ isset($pegawai->statusKepegawaian->last()->pivot->tanggal_mulai) ? \Carbon\Carbon::now()->diff(\Carbon\Carbon::parse($pegawai->statusKepegawaian->last()->pivot->tanggal_mulai))->format('%y Tahun, %m Bulan and %d Hari') : ''}}</td>
                            </tr>
                            <tr>
                                <td>Nomor HP</td>
                                <td>:</td>
                                <td>{{ $pegawai->phone ?? '-' }}</td>    
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td>:</td>
                                <td>{{ $pegawai->email ?? '-' }}</td>    
                            </tr>
                            @if (count($pegawai->rekening) > 0)
                                <tr>
                                    <td>Rekening Pegawai</td>
                                    <td>:</td>
                                    <td>
                                    <button type="button" class="btn btn-outline-success btn-sm" data-toggle="modal" data-target="#rekening">
                                        Lihat Data Rekening
                                    </button>
                                    <!-- Modal -->
                                    <div class="modal fade" id="rekening" tabindex="-1" aria-labelledby="rekeningModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                            <h5 class="modal-title" id="rekeningModalLabel">Data Rekening</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                                <table class="table table-borderless table-hover">
                                                    <thead class="thead-light">
                                                    <tr>
                                                        <th scope="col">#</th>
                                                        <th scope="col">Bank</th>
                                                        <th scope="col">Nomor Rekening</th>
                                                        <th scope="col">Atas Nama</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                        @forelse($pegawai->rekening as $rekening)
                                                            <tr>
                                                                <th scope="row"> {{ $loop->iteration }} </th>
                                                                <td>{{ $rekening->nama }}</td>
                                                                <td>{{ $rekening->pivot->no_rek }}</td>
                                                                <td>{{ $rekening->pivot->atas_nama }}</td>
                                                            </tr>
                                                        @empty
                                                        <tr>
                                                            <td colspan="5" class="text-center">
                                                                Tidak ada data keluarga dari pegawai ini
                                                            </td>
                                                        </tr>
                                                        @endforelse
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    </td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Foto Profil</h3>
                    <div class="card-tools">
                        <button wire:click="$toggle('uploadFoto')" class="btn btn-sm btn-primary">
                            @if ($pegawai->foto_profil == null)
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-upload"><path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"></path><polyline points="17 8 12 3 7 8"></polyline><line x1="12" y1="3" x2="12" y2="15"></line></svg>
                            @else
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit"><path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path><path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path></svg> 
                            @endif
                        </button>
                    </div>
                <button type="button" hidden class="btn btn-primary" id="openmodalcrop" data-toggle="modal" data-target="#cropfoto">
                </button>
                <div wire:ignore wire:key="modalcropfoto-fade" class="modal fade" id="cropfoto" tabindex="-1" data-backdrop="static" data-keyboard="false" aria-labelledby="cropfotoModalLabel" aria-hidden="true">
                    <div wire:key="modalcropfoto-dialog" class="modal-dialog modal-md">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="cropfotoModalLabel">Potong Foto</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div>
                                    <img src="" id="image" class="rounded img-thumbnail" alt="Tidak ada foto profil" >  
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" id="potong" class="btn btn-primary" data-dismiss="modal">Potong</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                <div class="card-body">
                    @if (session()->has('foto'))
                        <div class="alert alert-success">
                            {{ session('foto') }}
                        </div>
                    @endif
                    <div wire:key="foto" class="row d-flex justify-content-center">
                        @if (isset($pegawai->foto_profil))
                            <img src="{{ $src_foto }}" width="70%" class="rounded img-thumbnail" id="fotoprof" alt="Tidak ada foto profil" >  
                        @elseif (empty($pegawai->foto_profil))
                            <div class="">Foto Belum Diupload</div>
                        @else 
                            <div class="">Data Foto Tidak Ditemukan</div> 
                        @endif                             
                    </div>
                    @if ($uploadFoto)
                        <form wire:submit.prevent="updateFoto" class="mt-2">
                            @csrf
                            <input type="hidden" wire:model="verify" id="verify">
                            <input wire:model="temp_foto" type="file" class="form-control-file @error('temp_foto') is-invalid @enderror" id="temp_foto">
                            @error('temp_foto')
                                <span class="invalid-feedback">
                                    <strong>{{$message}}</strong>
                                </span>
                            @enderror
                            <button type="submit" wire:loading.attr="disabled" wire:target="temp_foto" class="btn btn-primary btn-sm mt-2" {{$cropped_foto ? '' : 'disabled'}}>Submit</button>
                            <div wire:loading wire:target="temp_foto"><i class="fa fa-sync fa-spin align-middle" aria-hidden="true"></i></div>
                        </form>
                    @endif
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
    <div class="row pt-5" id="data-kantor">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Data Kantor</h3>
                    <div class="card-tools">
                        <button class="btn btn-sm btn-success" wire:click.prevent="$toggle('data_kantor')">
                            @if ($data_kantor)
                                <i class="fas fa-times"></i>
                            @else
                                <i class="far fa-edit"></i>
                            @endif
                        </button>
                        {{-- <button data-toggle="modal" data-target="#cetakSuratModal{{$pegawai->id}}" class="btn bg-teal btn-sm">Cetak surat kantor</button>
                        <div wire:ignore.self class="modal fade" id="cetakSuratModal{{$pegawai->id}}" tabindex="-1" aria-labelledby="cetakSuratModalModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-md">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="cetakSuratModalModalLabel">Cetak Surat Keterangan Bekerja</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-10">
                                                <form>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" placeholder="Masukkan Nomor Surat" id="penomoran" wire:model.lazy="penomoran" required>
                                                        @error('penomoran') <span class="text-danger error">{{ $message }}</span>@enderror
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        @if (isset($penomoran))
                                        @endif
                                        <button type="button" class="btn btn-secondary" id="closeLetterModal" data-dismiss="modal">Batal</button>
                                    </div>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                </div>
                <div class="card-body" wire:key="data-kantor-body">
                    @if (session()->has('data-kantor'))
                        <div class="alert alert-success">
                            {{ session('data-kantor') }}
                        </div>
                    @endif
                    <div wire:key="data-kantor-if-wrapper">
                        @if ($data_kantor)
                            <form class="form-group" wire:submit.prevent="updateDataKantor" wire:key="data-kantor-form">
                                @csrf
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="file_cv">CV</label>
                                        <input wire:model="file_cv" type="file" name="file_cv" id="file_cv" class="form-control-file @error('file_cv') is-invalid @enderror">
                                        @error('file_cv')
                                            <span class="invalid-feedback">
                                                {{ $message }}
                                            </span> 
                                        @enderror
                                    </div>
                                    <div class="col-md-6">
                                        <label for="file_ijazah">Ijazah</label>
                                        <input wire:model="file_ijazah" type="file" name="file_ijazah" id="file_ijazah" class="form-control-file @error('file_ijazah') is-invalid @enderror">
                                        @error('file_ijazah')
                                            <span class="invalid-feedback">
                                                {{ $message }}
                                            </span> 
                                        @enderror
                                    </div>
                                </div>
                                <button wire:loading.attr="disabled" wire:target="file_cv, file_ijazah" type="submit" class="btn btn-primary btn-sm mt-3">
                                    upload
                                </button>
                                <span wire:loading wire:target="file_cv, file_ijazah" class="fa fa-sync fa-spin mt-3 align-middle" aria-hidden="true"></span>
                            </form>
                        @else
                            <table class="table table-bordered table-hover">
                                <thead class="thead-light">
                                    <tr>
                                        <th class="w-50">Data</th>
                                        <th class="text-center">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="align-middle">CV Pegawai</td>
                                        <td class="text-center">
                                            @if ($pegawai->file_cv)
                                                <a href="{{route('download', [
                                                        'pegawai' => $pegawai,
                                                        'folder' => Str::before($pegawai->file_cv, '/'),
                                                        'file' => Str::after($pegawai->file_cv, '/')
                                                    ])}}" 
                                                 wire:click="downResp" target="_blank" class="btn btn-sm btn-outline-primary">
                                                    Download
                                                </a> 
                                                <a href="{{route('preview', [
                                                        'pegawai' => $pegawai,
                                                        'folder' => Str::before($pegawai->file_cv, '/'),
                                                        'file' => Str::after($pegawai->file_cv, '/')
                                                    ])}}" 
                                                 target="_blank" class="btn btn-sm btn-outline-danger">
                                                    Lihat
                                                </a> 
                                            @else
                                                Pegawai Belum Memiliki Data Kantor CV    
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Ijazah Pegawai</td>
                                        <td class="text-center">
                                            @if ($pegawai->file_ijazah)
                                                <a href="{{route('download', [
                                                        'pegawai' => $pegawai,
                                                        'folder' => Str::before($pegawai->file_ijazah, '/'),
                                                        'file' => Str::after($pegawai->file_ijazah, '/')
                                                    ])}}" 
                                                target="_blank" class="btn btn-sm btn-outline-primary">
                                                    Download
                                                </a>
                                                <a href="{{route('preview', [
                                                        'pegawai' => $pegawai,
                                                        'folder' => Str::before($pegawai->file_ijazah, '/'),
                                                        'file' => Str::after($pegawai->file_ijazah, '/')
                                                    ])}}" 
                                                target="_blank" class="btn btn-sm btn-outline-danger">
                                                    Lihat
                                                </a>
                                            @else
                                            Pegawai Belum Memiliki Data Kantor Ijazah    
                                            @endif
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="keluarga">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Anggota Keluarga</h3>
                    <div class="card-tools">
                        <a href="{{ route('keluarga.create', [$pegawai]) }}" class="btn btn-sm btn-primary">Tambah Data Keluarga</a>
                    </div>
                </div>
                <div class="card-body">
                    @if (session()->has('keluarga'))
                        <div class="alert alert-success">
                            {{ session('keluarga') }}
                        </div>
                    @endif
                    <table class="table table-bordered table-hover" style="border-radius: 150px; border-color:greenyellow">
                        <thead class="thead-light">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">NIK</th>
                            <th scope="col">Nama</th>
                            <th scope="col">Hubungan</th>
                            <th scope="col">Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                            @forelse($pegawai->keluarga as $kel)
                                <tr>
                                    <th scope="row"> {{ $loop->iteration }} </th>
                                    <td>{{ $kel->nik }}</td>
                                    <td>{{ $kel->nama }}</td>
                                    <td>{{ $kel->hubungan->nama }}</td>
                                    <td> <a href="{{route('keluarga.show', [$pegawai, $kel])}}" class="btn btn-info btn-sm"> Detail </a> </td>
                                </tr>
                            @empty
                            <tr>
                                <td colspan="5" class="text-center">
                                    Tidak ada data keluarga dari pegawai ini
                                </td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
</div>
{{-- /.content --}}

@push('scripts')

<script type="application/javascript">
     $( document ).ready(function(){
        const image = document.getElementById('image');
        let cropper;
        $('#cropfoto').on('shown.bs.modal', function (e) {
            cropper = new Cropper(image, {
                viewMode:1,
                aspectRatio: 3 / 4,
            });
            @this.set('cropped_foto', false);
            return cropper;
        });

        $('#potong').on('click', function() {
            cropper.getCroppedCanvas({
                width: 160,
                height: 160,
                minWidth: 256,
                minHeight: 256,
                maxWidth: 4096,
                maxHeight: 4096,
                fillColor: '#fff',
                imageSmoothingEnabled: false,
                imageSmoothingQuality: 'high',
            })
            .toBlob((blob) => {
                let link = URL.createObjectURL(blob);
                console.log(blob);
                const file = new File([blob], 'untitled', { type: blob.type });
                @this.set('verify', false);
                @this.upload('temp_foto', file, (uploadedFilename) => {
                    // Success callback.
                    @this.set('verify', true);
                    @this.set('cropped_foto', true);
                }, () => {
                    // Error callback.
                }, (event) => {
                    // Progress callback.
                    // event.detail.progress contains a number between 1 and 100 as the upload progresses.
                });
                cropper = cropper.replace('{{$src_foto ?? ''}}');
            },'image/png');
        });

        $('#printLetter').on('click', function () {
            setTimeout(document.querySelector('#closeLetterModal').click(), 3000)
        });

        $('#cropfoto').on('hidden.bs.modal', function (e) {
            cropper.destroy();
        })

        livewire.on('changeUrl', (url) => {
            $('#cropfoto').modal('show');
            // $('#openmodalcrop').trigger('click');
            $('#image').attr('src', url);
        });

    })
</script>
@endpush