@section('title', 'Edit Pegawai')

@section('header', 'Edit Pegawai')

@section('breadcumb')
    @can('isAdmin')
        <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="fa fa-home" aria-hidden="true"></i>                    </a></li>
        <li class="breadcrumb-item"><a href="{{route('pegawai')}}">Pegawai</a></li>
    @endcan
    <li class="breadcrumb-item"><a href="{{route('pegawai.show', $pegawai->nip)}}">Detail Pegawai</a></li>
    <li class="breadcrumb-item active">Edit Pegawai</li>
@endsection

@section('styles')
    <style> 
    .has-error .select2-selection {
        border-color: rgb(227, 52, 47) !important;
    }

    .has-error .tanggal {
        border-color: rgb(227, 52, 47) !important;
    }
    </style>
@endsection

{{-- content --}}
@if ($updateFile)
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Form Edit Pegawai</h3>
            <div class="card-tools"> 
            </div>
        </div>
        <div class="card-body">
            <p>Silahkan upload file dalam bentuk image</p>
            <form wire:submit.prevent="updateFilePegawai">
                @csrf
                {{-- <input type="hidden" wire:model="pegawaiId"> --}}
                <div class="row">
                    <div class="form-group col">
                        <div class="row-5 mb-3">
                            <label for="file_ktp">File KTP</label>
                            <input wire:model="file_ktp" type="file" class="form-control-file @error('file_ktp') is-invalid @enderror" id="file_ktp" lang="ID">
                            @error('file_ktp')
                                <span class="invalid-feedback">
                                    <strong>{{$message}}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="row-5 mb-3">
                            <label for="file_kk">File KK</label>
                            <input wire:model="file_kk" type="file" class="form-control-file @error('file_kk') is-invalid @enderror" id="file_kk" lang="ID">
                            @error('file_kk')
                                <span class="invalid-feedback">
                                    <strong>{{$message}}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="row-5 mb-3">
                            <label for="file_npwp">File NPWP</label>
                            <input wire:model="file_npwp" type="file" class="form-control-file @error('file_npwp') is-invalid @enderror" id="file_npwp" lang="ID">
                            @error('file_npwp')
                                <span class="invalid-feedback">
                                    <strong>{{$message}}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group col">
                        <div class="row-5 mb-3">
                            <label for="file_bpjs_kes">File BPJS kesehatan</label>
                            <input wire:model="file_bpjs_kes" type="file" class="form-control-file @error('file_bpjs_kes') is-invalid @enderror" id="file_bpjs_kes" lang="ID">
                            @error('file_bpjs_kes')
                            <span class="invalid-feedback">
                                    <strong>{{$message}}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="row-5 mb-3">
                            <label for="file_bpjs_ket">File BPJS ketenagakerjaan</label>
                            <input wire:model="file_bpjs_ket" type="file" class="form-control-file @error('file_bpjs_ket') is-invalid @enderror" id="file_bpjs_ket" lang="ID">
                            @error('file_bpjs_ket')
                                <span class="invalid-feedback">
                                    <strong>{{$message}}</strong>
                                </span>
                            @enderror
                        </div>
                        <div style="margin-top: 42px">
                            <button wire:loading.attr="disabled" type="submit" class="btn btn-sm btn-primary">
                                <span wire:loading.class="d-none">Upload Berkas</span>
                                <span wire:loading><i class="fa fa-sync fa-spin mr-1" aria-hidden="true"></i> Mohon Tunggu</span>
                            </button>
                            <a href="" class="btn btn-sm btn-secondary">Kembali</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
@else
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Form Edit Pegawai</h3>
            <div class="card-tools"> 
            </div>
        </div>
        <div class="card-body">
            <form wire:submit.prevent="updatePegawai">
                @csrf
                <input wire:model="pegawaiId" type="hidden">
                <div class="row">
                    <div class="form-group col">
                        <div class="row-5 mb-3">
                            <label for="nip">NIP </label><span class="text-red"> *</span>
                            <input wire:model.lazy="nip" type="text" class="form-control @error('nip') is-invalid @enderror" id="nip" required>
                            @error('nip')
                                <span class="invalid-feedback">
                                    <strong>{{$message}}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="row-5 mb-3">
                            <label for="nama">Nama Lengkap</label><span class="text-red"> *</span>
                            <input wire:model.lazy="nama" type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" name="n" required>
                            @error('nama')
                            <span class="invalid-feedback">
                                {{ $message }}
                            </span> 
                            @enderror
                        </div>
                        <div class="row-5 mb-3">
                            <label for="tempat_lahir">Tempat Lahir</label><span class="text-red"> *</span>
                            <input wire:model.lazy="tempat_lahir" type="tempat_lahir" class="form-control text-capitalize @error('tempat_lahir') is-invalid @enderror " id="tempat_lahir" name="tempat-l" required>
                            @error('tempat_lahir')
                                <span class="invalid-feedback">
                                    <strong>{{$message}}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="row-sm-6 mb-3 @if($errors->has('tanggal_lahir')) has-error @endif" wire:key="unique1">
                            <label for="tanggal_lahir">Tanggal Lahir</label><span class="text-red"> *</span>
                            <div wire:ignore>
                                <div class="input-group date " id="datepicker" data-target-input="nearest">
                                    <input name="tanggal_lahir" id="tanggal_lahir"  type="text" class="tanggal form-control" data-toggle="datetimepicker" data-target="#datepicker" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask  name="tanggal-l" required/>
                                    <div class="input-group-append" data-target="#datepicker" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                            </div>
                            @if($errors->has('tanggal_lahir'))
                                <span style="font-size: 80%; color: #e3342f;"><strong>{{ $errors->first('tanggal_lahir') }}</strong></span>
                            @endif
                        </div>
                        <div class="row-5 mb-3">
                            <label for="alamat">Alamat</label><span class="text-red"> *</span>
                            <textarea wire:model.lazy="alamat" type="textarea" class="form-control @error('alamat') is-invalid @enderror" id="alamat" name="alamat" style=" height: 120px;" required></textarea>
                            @error('alamat')
                                <span class="invalid-feedback">
                                    <strong>{{$message}}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="row-sm-6 mb-3 @if($errors->has('jenis_kelamin')) has-error @endif" wire:key="unique2">
                            <label for="jenis_kelamin">Jenis Kelamin</label><span class="text-red"> *</span>
                            <div wire:ignore>   
                                <select style="width: 100%" class="select2bs4 custom-select" id="jenis_kelamin" data-placeholder="Pilih Jenis Kelamin" required>
                                    <option value="">Pilih Jenis Kelamin</option>
                                    <option value="Laki-Laki">Laki-Laki</option>
                                    <option value="Perempuan">Perempuan</option>
                                </select>
                            </div>
                            @if($errors->has('jenis_kelamin'))
                                <span style="font-size: 80%; color: #e3342f;"><strong>{{ $errors->first('jenis_kelamin') }}</strong></span>
                            @endif
                        </div>
                        <div class="row-sm-6 mb-3 @if($errors->has('agama')) has-error @endif" wire:key="unique3">
                            <label for="agama">Agama</label><span class="text-red"> *</span>
                            <div wire:ignore >
                                <select data-placeholder="Pilih Agama" style="width: 100%" class="select2bs4 custom-select @error('agama') is-invalid @enderror" id="agama" required>
                                    <option value="">Pilih Agama</option>
                                    <option value="Islam">Islam</option>
                                    <option value="Kristen">Kristen</option>
                                    <option value="Katolik">Katolik</option>
                                    <option value="Hindu">Hindu</option>
                                    <option value="Budha">Budha</option>
                                    <option value="Konghucu">Konghucu</option>
                                    <option value="Lainnya">Lainnya</option>
                                </select>
                            </div>
                            @if($errors->has('agama'))
                                <span style="font-size: 80%; color: #e3342f;"><strong>{{ $errors->first('agama') }}</strong></span>
                            @endif
                        </div>
                        <div class="row-sm-6 mb-3 @if($errors->has('status_perkawinan_id')) has-error @endif" wire:key="unique4">
                            <label for="status_perkawinan_id">Status Perkawinan</label><span class="text-red"> *</span>
                            <div wire:ignore>
                                <select data-placeholder="Pilih Status Perkawinan" style="width: 100%" class="select2bs4 custom-select" id="status_perkawinan_id" required>
                                    <option value="">Pilih Status Perkawinan</option>
                                    @foreach ($daftar_stat_perk as $stat_perk)
                                    <option value="{{ $stat_perk->id }}">{{ $stat_perk->nama }}</option>
                                    @endforeach
                                </select>
                            </div>    
                            @if($errors->has('status_perkawinan_id'))
                                <span style="font-size: 80%; color: #e3342f;"><strong>{{ $errors->first('status_perkawinan_id') }}</strong></span>
                            @endif
                        </div>
                        <div class="row-sm-6 mb-3 @if($errors->has('golongan_darah')) has-error @endif" wire:key="unique5">
                            <label for="golongan_darah">Golongan Darah</label><span class="text-red"> *</span>
                            <div wire:ignore>
                                <select data-placeholder="Pilih Golongan Darah" style="width: 100%" class="select2bs4 custom-select @error('golongan_darah') is-invalid @enderror" id="golongan_darah" required>
                                    <option value="">Pilih Golongan Darah</option>
                                    <option value="A">A</option>
                                    <option value="B">B</option>
                                    <option value="AB">AB</option>
                                    <option value="O">O</option>
                                </select>
                            </div>
                            @if($errors->has('golongan_darah'))
                                <span style="font-size: 80%; color: #e3342f;"><strong>{{ $errors->first('golongan_darah') }}</strong></span>
                            @endif
                        </div>
                        <!-- Button trigger modal -->
                        <button wire:loading.attr='disabled' wire:key="'showModalRek'" type="button" id="showModalRek" class="btn btn-outline-primary d-inline-block" data-toggle="modal" data-target="#rekeningInput">
                            <span wire:loading.class="d-none">Data rekening</span>
                            <span wire:loading><i class="fa fa-sync fa-spin align-middle" aria-hidden="true"></i> Mohon Tunggu</span>
                        </button>
                        <div wire:ignore class="d-inline-block">
                            <livewire:rekening-modal :pegawai="$pegawai" :key="'rekinmodal'.Str::random()" >
                        </div>
                        
                        @can('isAdmin')
                            <button wire:loading.attr='disabled' wire:key="'showModalStat'" type="button" id="showModalStat" class="btn btn-outline-primary d-inline-block" data-toggle="modal" data-target="#statKepInput">
                                <span wire:loading.class="d-none">Data status kepegawaian<span class="text-red"> *</span></span>
                                <span wire:loading><i class="fa fa-sync fa-spin align-middle" aria-hidden="true"></i> Mohon Tunggu</span>
                            </button>
                            <div wire:ignore class="d-inline-block">
                                <livewire:status-kepegawaian-modal :pegawai="$pegawai" :key="'statkepinmodal'.Str::random()" >
                            </div>
                        @endcan
                            
                    </div>
                    <div class="form-group col">
                        <div class="row-sm-6 mb-3 @if($errors->has('pendidikan_id')) has-error @endif"  wire:key="unique6">
                            <label for="pendidikan_id">Pendidikan Terakhir</label><span class="text-red"> *</span>
                            <div wire:ignore>
                                <select data-placeholder="Pilih Pendidikan Terakhir" style="width: 100%" class="select2bs4 custom-select" id="pendidikan_id" required>
                                    <option value="">Pilih Pendidikan Terakhir</option>
                                    @foreach ($daftar_pendidikan as $pendidikan)
                                        <option value="{{ $pendidikan->id }}" class="">{{ $pendidikan->nama }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @if($errors->has('pendidikan_id'))
                                <span style="font-size: 80%; color: #e3342f;"><strong>{{ $errors->first('pendidikan_id') }}</strong></span>
                            @endif
                        </div>
                        <div class="row-sm-6 mb-3">
                            <label for="nik">NIK</label><span class="text-red"> *</span>
                            <input wire:model.lazy="nik" type="text" class="form-control mb-2 @error('nik') is-invalid @enderror" id="nik" required >
                            @error('nik')
                                <span class="invalid-feedback">
                                    <strong>{{$message}}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="row-sm-6 mb-3">
                            <label for="no_kk">Nomor KK</label>
                            <input wire:model.lazy="no_kk" type="text" class="form-control mb-2 @error('no_kk') is-invalid @enderror" id="no_kk">
                            @error('no_kk')
                                <span class="invalid-feedback">
                                    <strong>{{$message}}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="row-sm-6 mb-3">
                            <label for="no_npwp">Nomor Pokok Wajib Pajak (NPWP)</label>
                            <input wire:model.lazy="no_npwp" type="text" class="form-control mb-2 @error('no_npwp') is-invalid @enderror" id="no_npwp">
                            @error('no_npwp')
                                <span class="invalid-feedback">
                                    <strong>{{$message}}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="row-5 mb-3">
                            <label for="no_bpjs_kes">Nomor BPJS Kesehatan</label>
                            <input wire:model.lazy="no_bpjs_kes" type="text" class="form-control mb-2 @error('no_bpjs_kes') is-invalid @enderror" id="no_bpjs_kes">
                            @error('no_bpjs_kes')
                                <span class="invalid-feedback">
                                    <strong>{{$message}}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="row-5 mb-3">
                            <label for="no_bpjs_ket">Nomor BPJS Ketenagakerjaan</label>
                            <input wire:model.lazy="no_bpjs_ket" type="text" class="form-control mb-2 @error('no_bpjs_ket') is-invalid @enderror" id="no_bpjs_ket">
                            @error('no_bpjs_ket')
                                <span class="invalid-feedback">
                                    <strong>{{$message}}</strong>
                                </span>
                            @enderror
                        </div>
                        @can('isAdmin')
                            <div class="row-sm-6 mb-3 @if($errors->has('departemen_id')) has-error @endif" wire:key="unique10">
                                <label for="departemen_id">Departemen</label><span class="text-red"> *</span>
                                <div wire:ignore>
                                    <select data-placeholder="Pilih Departemen" style="width: 100%" class="select2bs4 custom-select" id="departemen_id"  name="dp" required>
                                        <option value="">Pilih Departemen</option>
                                        @foreach ($daftar_departemen as $departemen)
                                        <option value="{{ $departemen->id }}" class="">{{ $departemen->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if($errors->has('departemen_id'))
                                    <span style="font-size: 80%; color: #e3342f;"><strong>{{ $errors->first('departemen_id') }}</strong></span>
                                @endif
                            </div>
                            <div class="row-sm-6 mb-3 @if($errors->has('jabatan_id')) has-error @endif" wire:key="unique11">
                                <label for="jabatan_id">Jabatan</label><span class="text-red"> *</span>
                                <div wire:ignore>
                                    <select data-placeholder="Pilih Jabatan" style="width: 100%" class="select2bs4 custom-select" id="jabatan_id"  name="jbtn" required>
                                        <option value="">Pilih Jabatan</option>
                                        @foreach ($daftar_jabatan as $jabatan)
                                        <option value="{{ $jabatan->id }}" class="">{{ $jabatan->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if($errors->has('jabatan_id'))
                                    <span style="font-size: 80%; color: #e3342f;"><strong>{{ $errors->first('jabatan_id') }}</strong></span>
                                @endif
                            </div>
                        @endcan
                        <div class="row-5 mb-3">
                            <label for="phone">Nomor Telepon</label><span class="text-red"> *</span>
                            <input wire:model.lazy="phone" type="text" class="form-control @error('phone') is-invalid @enderror" id="phone" name="phone" required>
                            @error('phone')
                                <span class="invalid-feedback">
                                    <strong>{{$message}}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="row-5 mb-3">
                            <label for="email">Email</label><span class="text-red"> *</span>
                            <input wire:model.lazy="email" type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" required>
                            @error('email')
                                <span class="invalid-feedback">
                                    <strong>{{$message}}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="text-right">
                            <button type="submit" wire:loading.attr="disabled" class="btn btn-primary d-inline-block">
                                <span wire:loading.remove>Simpan Perubahan</span>
                                <span wire:loading>
                                    <i class="fa fa-sync fa-spin" aria-hidden="true"></i>
                                </span>
                            </button>
                            <button wire:click="$toggle('updateFile')" type="button" class="btn btn-success d-inline-block">Update File Pegawai</button>
                            <a href="{{route('pegawai.show', $pegawai->nip)}}" class="btn btn-secondary d-inline-block" id="kembali" >Kembali</a>
                        </div>
                    </div>            
                </div>
            </form>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
@endif
{{-- /.content --}}

@push('scripts')
    <script type="application/javascript">
        $(document).ready(function(){
            jQuery.noConflict();
            $(document).on("turbolinks:before-cache", function () {
                if ($('.select2bs4').first().data('select2') != undefined) $('.select2bs4').select2('destroy');
            });

            $('#showModalStat').on('click', function (e) {
                $('.date').datetimepicker({
                    format: 'DD/MM/YYYY',
                });
                $('[data-mask]').inputmask();
            });

            // Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4',
                placeholder: function(){
                    $(this).data('placeholder');
                },
                allowClear: false,
            });
            //Initialize initial data
            $('#jenis_kelamin').val('{{$jenis_kelamin}}').trigger('change');
            $('#agama').val('{{$agama}}').trigger('change');
            $('#status_perkawinan_id').val('{{$status_perkawinan_id}}').trigger('change');
            $('#golongan_darah').val('{{$golongan_darah}}').trigger('change');
            $('#pendidikan_id').val('{{$pendidikan_id}}').trigger('change');
            $('#departemen_id').val('{{$departemen_id}}').trigger('change');
            $('#jabatan_id').val('{{$jabatan_id}}').trigger('change');
            $('#tanggal_lahir').val('{{$tanggal_lahir}}').trigger('change');
            // function to set livewire public properties on change
            $('.select2bs4').on('change', function (ev) {
                @this.set(ev.target.id, ev.target.value);
            });

            // $('#golongan_darah').on('change', function (e) {
            //     @this.set(e.target.id, e.target.value);
            // });
            

            //Date range picker
            $('#datepicker, #datepicker_tgl_m, #datepicker_tgl_b').datetimepicker({
            format: 'DD/MM/YYYY'
            });
            
            $('.tanggal').on('blur', function (ev){
                @this.set(ev.target.id, ev.target.value);
            }); 
            
        });
    </script>
@endpush