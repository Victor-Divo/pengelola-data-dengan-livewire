<div>
    <footer class="main-footer" wire:key="'footer-wrapper'">
        <div class="float-right d-none d-sm-block">
          <b>Version</b> 1.0
        </div>
        <strong>Copyright &copy; 2020 <a target="_blank" href="https://gitlab.com/Victor-Divo/pengelola-data-dengan-livewire">Victor D.M</a>.</strong> All rights
        reserved.
    </footer>
</div>
