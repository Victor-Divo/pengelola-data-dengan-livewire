@section('title', 'Bank')

@section('header', 'Bank')

@section('breadcumb')
    <li class="breadcrumb-item active"><a href="{{route('home')}}"><i class="fa fa-home" aria-hidden="true"></i>                    </a></li>
    <li class="breadcrumb-item active">Bank</li>
@endsection

{{-- content --}}
<div class="container-fluid" wire:key="rekening-index">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Form {{$statusUpdate ? 'Edit' : 'Tambah'}} Bank</h3>
        </div>
        <div class="card-body">
            <form wire:submit.prevent="submit" class="form-inline">
                @CSRF
                <input type="hidden" name="" wire:model="rekeningId">
                <div class="form-group mr-sm-3">
                    <label for="nama" class="mx-2">Nama</label>
                    <input wire:model.debounce.500ms="nama" type="text" class="mx-2 form-control @error('nama') is-invalid @enderror" id="nama" name="n" required>
                    <span class="invalid-feedback" style="max-width: max-content">
                        @error('nama')
                            {{ $message }}
                        @enderror
                    </span> 
                </div>
                <button type="submit" wire:loading.attr="disabled" wire:target="nama" class="btn btn-primary">
                    <span wire:loading.remove wire:target="nama">{{$statusUpdate ? 'Simpan' : 'Tambah'}}</span>
                    <span wire:loading wire:target="nama">
                        <i class="fa fa-sync fa-spin" aria-hidden="true"></i>
                    </span>
                </button>
            </form> 
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Daftar Bank</h3>
        </div>
        <div class="card-body">
            @if (session()->has('pesan'))
                <div class="alert alert-success" id="pesan">
                    {{ session('pesan') }}
                </div>
            @endif
            <div class="row mb-3">
                <div class="col">
                    <select wire:model="paginate" class="form-control form-control-sm w-auto">
                        <option value="5">5</option>
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                    </select>
                </div>
                <div class="col d-flex justify-content-end">
                    <input wire:model="search" type="text" class="form-control form-control-sm w-auto" placeholder="search" >
                </div>
            </div>
            <table class="table table-bordered table-hover rounded-circle table-striped mb-3" style="border-block-color: green;">
                <thead>
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nama</th>
                    <th scope="col" class="text-center">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ( $daftar_rekening as $rekening )
                        <tr>
                            <th scope="row" wire:key="{{ $loop->index.'#' }}">{{ $loop->iteration}}</th>
                            <td wire:key="{{ $loop->index.'nama' }}">{{ $rekening->nama }}</td>
                            <td wire:key="{{ $loop->index.'aksi' }}" class="text-center">
                                <button wire:click="showRekening({{ $rekening->id }})" class="btn btn-sm btn-info text-white mr-1">Edit</button>
                                <button data-toggle="modal" data-target="#deletemodal{{$loop->iteration}}" class="btn btn-sm btn-danger">Delete</button>
                                <div class="modal fade" id="deletemodal{{$loop->iteration}}" tabindex="-1" aria-labelledby="deletemodalModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-md">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="deletemodalModalLabel">Konfirmasi</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                Apakah anda yakin?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" wire:click="destroy('{{ $rekening->id }}', true)" class="btn btn-danger" data-dismiss="modal">Hapus</button>
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>                            
                            </td>
                        </tr>
                    @empty
                    <div>
                        @if ($search)
                            <tr>
                                <td colspan="4" class="text-center">Data "{{$search}}" tidak ditemukan</td>
                            </tr>
                        @else
                            <tr>
                                <td colspan="4" class="text-center">Tidak ada data untuk ditampilkan</td>
                            </tr>
                        @endif
                    </div>
                    @endforelse
                </tbody>
            </table>
            {{ $daftar_rekening->links() }}
        </div>
    </div>
</div>
{{-- /.content --}}