@section('title', 'Departemen')

@section('header', 'Departemen')

@section('breadcumb')
    <li class="breadcrumb-item active"><a href="{{route('home')}}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
    <li class="breadcrumb-item active">Departemen</li>
@endsection

{{-- content --}}
<div class="container-fluid" wire:key="departemen-index">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Form {{$statusUpdate ? 'Edit' : 'Tambah'}} Departemen</h3>
        </div>
        <div class="card-body">
            <form wire:submit.prevent="submit">
                @CSRF
                <input type="hidden" name="" wire:model="departemenId">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row-6 mb-3">
                                <label for="nama">Nama</label>
                                <input wire:model.debounce.500ms="nama" type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" name="n" required>
                                @error('nama')
                                    <span class="invalid-feedback">
                                        {{ $message }}
                                    </span> 
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="deskripsi">Deskripsi</label>
                            <input wire:model.debounce.500ms="deskripsi" type="text" class="form-control @error('deskripsi') is-invalid @enderror" id="deskripsi" name="n" required>
                            @error('deskripsi')
                                <span class="invalid-feedback">
                                    {{ $message }}
                                </span> 
                            @enderror
                        </div>
                    </div>
                </div>
                <button type="submit" wire:loading.attr="disabled" wire:target="deskripsi, nama" class="btn btn-primary mt-2">
                    <span wire:loading.remove wire:target="deskripsi, nama">{{$statusUpdate ? 'Simpan' : 'Tambah'}}</span>
                    <span wire:loading wire:target="deskripsi, nama">
                        <i class="fa fa-sync fa-spin" aria-hidden="true"></i>
                    </span>
                </button>
            </form> 
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Daftar Departemen</h3>
        </div>
        <div class="card-body">
            @if (session()->has('pesan'))
                <div class="alert alert-success" id="pesan">
                    {{ session('pesan') }}
                </div>
            @endif
            <div class="row mb-3">
                <div class="col">
                    <select wire:model="paginate" class="form-control form-control-sm w-auto">
                        <option value="5">5</option>
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                    </select>
                </div>
                <div class="col d-flex justify-content-end">
                    <input wire:model="search" type="text" class="form-control form-control-sm w-auto" placeholder="search" >
                </div>
            </div>
            <table class="table table-bordered table-hover rounded-circle table-striped mb-3" style="border-block-color: green;">
                <thead>
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Deskripsi</th>
                    <th scope="col" class="text-center">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ( $daftar_departemen as $departemen )
                        <tr>
                            <th scope="row">{{ $loop->iteration}}</th>
                            <td>{{ $departemen->nama }}</td>
                            <td>{{ $departemen->deskripsi }}</td>
                            <td class="text-center">
                            @can('isAdmin')
                                <button wire:click="showDepartemen({{ $departemen->id }})" class="btn btn-sm btn-info text-white mr-1">Edit</button>
                                <button data-toggle="modal" data-target="#deletemodal{{$loop->iteration}}" class="btn btn-sm btn-danger">Delete</button>
                                <div class="modal fade" id="deletemodal{{$loop->iteration}}" tabindex="-1" aria-labelledby="deletemodalModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-md">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="deletemodalModalLabel">Konfirmasi</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                Apakah anda yakin?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" wire:click="destroy('{{ $departemen->id }}', true)" class="btn btn-danger" data-dismiss="modal">Hapus</button>
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @else
                                <span class="text-danger">Anda tidak dapat melakukan aksi karena bukan admin</span>
                            </td>
                            @endcan
                        </tr>
                    @empty
                        @if ($search)
                            <tr>
                                <td colspan="4" class="text-center">Data "{{$search}}" tidak ditemukan</td>
                            </tr>
                        @else
                            <tr>
                                <td colspan="4" class="text-center">Tidak ada data untuk ditampilkan</td>
                            </tr>
                        @endif
                    @endforelse
                </tbody>
            </table>
            {{ $daftar_departemen->links() }}
        </div>
    </div>
</div>
{{-- /.content --}}