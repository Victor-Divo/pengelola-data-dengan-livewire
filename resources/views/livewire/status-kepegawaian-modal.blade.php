<div>
    <!-- Modal -->
    <div wire:ignore.self class="modal fade" id="statKepInput" tabindex="-1" aria-labelledby="statKepInputLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div wire:loading.class="overlay d-flex justify-content-center align-items-center" class="">
                    <span wire:loading.class="fas fa-2x fa-sync fa-spin"></span>
                </div>
                <div class="modal-header">
                <h5 class="modal-title" id="statKepInputLabel">Input Status Kepegawaian</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span id="closeStatModal" aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    {{-- <form id="formStatKepInput" wire:submit.prevent="storeStatKep"> --}}
                        <div class=" add-input">
                            <div class="row mt-2">
                                <div class="col-md-3">
                                    <select placeholder="Pilih Status Kepegawaian" class="select2bs4modal custom-select" id="status_kepegawaian.0" wire:model.lazy="status_kepegawaian.0">
                                        <option value="" selected hidden>Pilih Status Kepegawaian</option>
                                        @foreach ($daftar_stat_kep as $stat_kep)
                                        <option value="{{ $stat_kep->id }}" class="">{{ $stat_kep->jenis }}</option>
                                        @endforeach
                                    </select>
                                    @error('status_kepegawaian.0') <span class="text-danger error">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-md-3">
                                    {{-- <input type="date" wire:model.lazy="tanggal_mulai.{{$key}}" class="form-control"> --}}
                                    <div class="input-group date " id="datepicker_tgl_m-0" data-target-input="nearest">
                                        <input value="{{empty($pegawai->mulai[0]) ? '' : $pegawai->mulai[0]}}" wire:model="tanggal_mulai.0" data-toggle="datetimepicker" id="tanggal_mulai.0"  
                                        type="text" class="tanggalModal datetimepicker-input form-control" data-target="#datepicker_tgl_m-0" 
                                        data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask onblur="this.dispatchEvent(new InputEvent('input'))"/>
                                        <div class="input-group-append" data-target="#datepicker_tgl_m-0" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                    @if($errors->has('tanggal_mulai.0'))
                                        <span style="font-size: 80%; color: #e3342f;"><strong>{{ $errors->first('tanggal_mulai.0') }}</strong></span>
                                    @endif
                                </div>
                                <div class="col-md-3">
                                    {{-- <input type="date" wire:model.lazy="tanggal_berakhir.{{$key}}" class="form-control"> --}}
                                    <div class="input-group date " id="datepicker_tgl_b-0" data-target-input="nearest">
                                        <input value="{{empty($pegawai->akhir[0]) ? '' : $pegawai->akhir[0]}}" wire:model="tanggal_berakhir.0" 
                                        data-toggle="datetimepicker" id="tanggal_berakhir.0"  type="text" class="tanggalModal datetimepicker-input form-control" 
                                        data-target="#datepicker_tgl_b-0" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" 
                                        data-mask onblur="this.dispatchEvent(new InputEvent('input'))">
                                        <div class="input-group-append" data-target="#datepicker_tgl_b-0" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                    @if($errors->has('tanggal_berakhir.0'))
                                        <span style="font-size: 80%; color: #e3342f;"><strong>{{ $errors->first('tanggal_berakhir.0') }}</strong></span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @forelse($inputsStat as $key => $value)
                            <div class=" add-input">
                                <div class="row mt-2">
                                    <div class="col-md-3">
                                        <select placeholder="Pilih Status Kepegawaian" class="select2bs4modal custom-select" id="status_kepegawaian.{{ $value }}" wire:model.lazy="status_kepegawaian.{{ $value }}">
                                            <option value="" selected hidden>Pilih Status Kepegawaian</option>
                                            @foreach ($daftar_stat_kep as $stat_kep)
                                            <option value="{{ $stat_kep->id }}" class="">{{ $stat_kep->jenis }}</option>
                                            @endforeach
                                        </select>
                                        @error('status_kepegawaian.'.$value) <span class="text-danger error">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="col-md-3">
                                        {{-- <input type="date" wire:model.lazy="tanggal_mulai.{{$value}}" class="form-control"> --}}
                                        <div class="input-group date " id="datepicker_tgl_m-{{ $value }}" data-target-input="nearest">
                                            <input value="{{empty($pegawai->mulai[$value]) ? '' : $pegawai->mulai[$value]}}" wire:model="tanggal_mulai.{{$value}}" data-toggle="datetimepicker" id="tanggal_mulai.{{ $value }}"  type="text" class="tanggalModal datetimepicker-input form-control" data-target="#datepicker_tgl_m-{{ $value }}" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask onblur="this.dispatchEvent(new InputEvent('input'))"/>
                                            <div class="input-group-append" data-target="#datepicker_tgl_m-{{ $value }}" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>
                                        @if($errors->has('tanggal_mulai.'.$value))
                                            <span style="font-size: 80%; color: #e3342f;"><strong>{{ $errors->first('tanggal_mulai.'.$value) }}</strong></span>
                                        @endif
                                    </div>
                                    <div class="col-md-3">
                                        {{-- <input type="date" wire:model.lazy="tanggal_berakhir.{{$value}}" class="form-control"> --}}
                                        <div class="input-group date " id="datepicker_tgl_b-{{ $value }}" data-target-input="nearest">
                                            <input value="{{empty($pegawai->akhir[$value]) ? '' : $pegawai->akhir[$value]}}" wire:model="tanggal_berakhir.{{$value}}" data-toggle="datetimepicker" id="tanggal_berakhir.{{ $value }}"  type="text" class="tanggalModal datetimepicker-input form-control" data-target="#datepicker_tgl_b-{{ $value }}" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask onblur="this.dispatchEvent(new InputEvent('input'))" >
                                            <div class="input-group-append" data-target="#datepicker_tgl_b-{{ $value }}" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>
                                        @if($errors->has('tanggal_berakhir.'.$value))
                                            <span style="font-size: 80%; color: #e3342f;"><strong>{{ $errors->first('tanggal_berakhir.'.$value) }}</strong></span>
                                        @endif
                                    </div>
                                    <div class="col-md-1">
                                        <button class="btn btn-danger" wire:click.prevent="remove({{$key}})">hapus</button>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <div class="row mt-2">
                                <div class="col-12 text-center">
                                    Tambah data jika diperlukan
                                </div>
                            </div>
                        @endforelse
                    {{-- </form> --}}
                </div>
                <div class="modal-footer">
                    <button class="btn text-white btn-success " wire:click.prevent="add({{$i}})">
                        Tambah
                    </button>
                    <button type="button" wire:click.prevent="storeStatKep" wire:loading.attr='disabled' class="btn btn-primary" form="formStatKepInput">Simpan Perubahan</button>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script type="application/javascript">
        $(document).ready(function(){
            jQuery.noConflict();
            // Date range picker
            livewire.on('closeStatModal', () => {
                $('#closeStatModal').trigger('click');
            });

            livewire.on('newStat' , (i) => {
                // $('.date').datetimepicker('destroy');
                // Date range picker
               $('.date').datetimepicker({
                    format: 'DD/MM/YYYY',
                });

                $('[data-mask]').inputmask();
            });
            // livewire.on('rmStat' , (i) => {
            //     // $(`[id=datepicker_tgl_m-${i}], [id=datepicker_tgl_b-${i}]`).datetimepicker('destroy');
            //     // $('.date').datetimepicker('destroy');
            // });
        });
    </script>    
@endpush