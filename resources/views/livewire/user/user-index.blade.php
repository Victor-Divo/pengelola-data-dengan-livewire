@section('title', 'User')

@section('header', 'User')

@section('breadcumb')
    <li class="breadcrumb-item active"><a href="{{route('home')}}"><i class="fa fa-home" aria-hidden="true"></i>                    </a></li>
    <li class="breadcrumb-item active">User</li>
@endsection

{{-- content --}}
<div class="container-fluid" wire:key="user-index">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Kelola User</h3>
        </div>
        <div class="card-body">
            <form wire:submit.prevent="submit">
                @CSRF
                <input type="hidden" name="" wire:model="userId">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row-6 mb-3" wire:key="user-pegawai_id">
                                <label for="pegawai_id">NIP</label>
                                <div wire:ignore>
                                    <select class="select2bs4 custom-select @error('pegawai_id') is-invalid @enderror" id="pegawai_id" style="width: 100%" data-placeholder="Pilih NIP" required>
                                        <option value="">Pilih NIP</option>
                                        @foreach ($pegawai as $peg)
                                            @if (!json_decode($peg->user, true) == null)
                                                <option value="{{ $peg->id }}" disabled="disabled" aria-hidden="true">{{ $peg->nip }} (telah terdaftar!)</option>
                                            @else
                                                <option value="{{ $peg->id }}" class="">{{ $peg->nip }} ({{$peg->nama}})</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row-6">
                                <label for="username">Username</label>
                                <input wire:model.lazy="username" type="text" class="form-control @error('username') is-invalid @enderror" id="username" name="n">
                                @error('username')
                                    <span class="invalid-feedback">
                                        {{ $message }}
                                    </span> 
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6" wire:key="user-role">
                            <label for="role">Role</label>
                            <div wire:ignore>   
                                <select class="select2bs4 custom-select @error('role') is-invalid @enderror" id="role" style="width: 100%" data-placeholder="Pilih Role" required>
                                    <option value="">Pilih NIP</option>
                                    <option value="Admin">Admin</option>
                                    <option value="User">User</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="submit" wire:loading.attr="disabled" wire:target="pegawai_id, role, username" class="btn btn-primary mt-2">
                    <span wire:loading.remove wire:target="pegawai_id, role, username">{{$statusUpdate ? 'Simpan' : 'Tambah'}}</span>
                    <span wire:loading wire:target="pegawai_id, role, username">
                        <i class="fa fa-sync fa-spin" aria-hidden="true"></i>
                    </span>
                </button>
            </form> 
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Daftar User</h3>
        </div>
        <div class="card-body">
            @if (session()->has('pesan'))
                <div class="alert alert-success" id="pesan">
                    {{ session('pesan') }}
                </div>
            @elseif (session()->has('pesan-error'))
                <div class="alert alert-danger" id="pesan-error">
                    {{ session('pesan-error') }}
                </div>
            @endif
            <div class="row mb-3">
                <div class="col">
                    <select wire:model="paginate" name="" id="" class="form-control form-control-sm w-auto">
                        <option value="5">5</option>
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                    </select>
                </div>
                <div class="col d-flex justify-content-end">
                    <input wire:model="search" type="text" class="form-control form-control-sm w-auto" placeholder="search" >
                </div>
            </div>
            <table class="table table-bordered table-hover table-responsive-md table-striped mb-3" style="border-block-color: green;">
                <thead>
                    <tr>
                    <th scope="col" class="text-center">#</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Username</th>
                    <th scope="col">Role</th>
                    <th scope="col" class="text-center">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ( $users as $user )
                        <tr>
                            <th scope="row">{{ $loop->iteration}}</th>
                            <td>{{ $user->pegawai->nama }}</td>
                            <td>{{ $user->username }}</td>
                            <td>{{ $user->role }}</td>
                            <td class="text-center">
                            <button wire:click="showUser({{ $user->id }})" class="btn btn-sm btn-info text-white mr-1">Edit</button>
                            <button data-toggle="modal" {{Auth::user()->id == $user->id ? 'disabled' : ''}} data-target="#deletemodal{{$loop->iteration}}" class="btn btn-sm btn-danger">Delete</button>
                            <div class="modal fade" id="deletemodal{{$loop->iteration}}" tabindex="-1" aria-labelledby="deletemodalModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-md">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="deletemodalModalLabel">Konfirmasi</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            Apakah anda yakin?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" wire:click="destroy('{{ $user->id }}', true)" class="btn btn-danger" data-dismiss="modal">Hapus</button>
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </tr>
                    @empty
                        @if ($search)
                            <tr>
                                <td colspan="5" class="text-center">Data "{{$search}}" tidak ditemukan</td>
                            </tr>
                        @else
                            <tr>
                                <td colspan="5" class="text-center">Tidak ada data untuk ditampilkan</td>
                            </tr>
                        @endif
                    @endforelse
                </tbody>
            </table>
            {{ $users->links() }}
        </div>
    </div>
</div>
{{-- /.content --}}

@push('scripts')
<script type="application/javascript">
    $( document ).ready(function(){
        jQuery.noConflict();
        // //Initialize Select2 Elements
        $('#role, #pegawai_id').select2({
        theme: 'bootstrap4',
        placeholder: function(){
            $(this).data('placeholder');
            },
        allowClear: true,
        });
        $('.select2bs4').on('change', function (ev) {
            @this.set(ev.target.id, ev.target.value);
            document.querySelectorAll('.select2-selection__clear').forEach(function(el) {
                el.setAttribute('hidden', true);
            });
        });

        livewire.on('showed', (uid, nip, role, check) => {
                $('#role').select2('destroy');
                $('#role').select2({
                    theme: 'bootstrap4',
                    placeholder: function(){
                        $(this).data('placeholder');
                    },
                    disabled : check,
                    allowClear: false,
                });
            $('#pegawai_id').val(nip).trigger('change');
            $('#role').val(role).trigger('change');
        });
    });
</script>    
@endpush
