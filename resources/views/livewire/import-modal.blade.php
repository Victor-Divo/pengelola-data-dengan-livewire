<div style="display: inline-block">
    <!-- Button trigger modal -->
    <button type="button" class="btn btn-sm btn-secondary" data-toggle="modal" data-target="#importExcelInput">
        Import
    </button>
    
    <!-- Modal -->
    <div wire:ignore.self class="modal fade" id="importExcelInput" tabindex="-1" aria-labelledby="importExcelInputLabel" data-backdrop="static" aria-hidden="true">
        <div class="modal-dialog">
        <div class="modal-content">
            <div wire:loading.class="overlay d-flex justify-content-center align-items-center" class="">
                <span wire:loading.class="fas fa-2x fa-sync fa-spin"></span>
            </div>
            <form wire:submit.prevent="import">
            <div class="modal-header">
                <h5 class="modal-title" id="importExcelInputLabel">Import</h5>
            </div>
            <div class="modal-body">
                <div class="add-input">
                    <div class="row">
                        <div class="col">
                            <label for="file">File Excel</label>
                            <input required wire:model="file" type="file" name="file" id="file" class="form-control-file @error('file') is-invalid @enderror">
                            @error('file')
                            <span class="invalid-feedback">
                                {{ $message }}
                            </span> 
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="close" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" wire:loading.attr='disabled' class="btn btn-primary">Save changes</button>
            </div>
            </form>
        </div>
        </div>
    </div>
</div>

@push('scripts')
    <script type="application/javascript">
        $(document).ready(function(){
            jQuery.noConflict();
            livewire.on('closeModal', () => {
                $('#close').trigger('click');
            });
        })
    </script>    
@endpush