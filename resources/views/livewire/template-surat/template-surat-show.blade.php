@section('title', 'Surat Keterangan Kerja ('. $templateSurat->statusKepegawaian->jenis.')')

@section('header', 'Surat Keterangan Kerja ('. $templateSurat->statusKepegawaian->jenis.')')

@section('breadcumb')
    <li class="breadcrumb-item active"><a href="{{route('home')}}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
    <li class="breadcrumb-item active"><a href="{{ route('template.surat.index') }}">Template Surat</a></li>
    <li class="breadcrumb-item active">Surat Keterangan Kerja ({{$templateSurat->statusKepegawaian->jenis}})</li>
@endsection

@section('styles')
    <style>
        @import url('https://fonts.googleapis.com/css2?family=IBM+Plex+Sans&display=swap');

    </style>
@endsection

{{-- content --}}
<div class="container-fluid" wire:key="template-surat">
    @if (session()->has('pesan'))
        <div wire:ignore class="alert alert-success" id="pesan">
            {{ session('pesan') }}
        </div>
    @endif
    <div wire:ignore class="overlay ">
        <i class="d-flex justify-content-center align-items-center fas fa-2x fa-sync fa-spin"></i>
    </div>
    <div wire:ignore class="textarea-wrapper d-none">
        <div class="card">
            <form action="" wire:submit.prevent="saveTemplate">
                <div class="card-header">
                    <h3 class="card-title">Header & Footer<span class="text-red"> *</span></h3>
                    <div class="card-tools"><!-- Button trigger modal -->
                        <button type="submit" class="btn btn-sm btn-primary" >
                            <i class="fas fa-save" style=""></i>
                            Simpan
                        </button>
                        <a href="./" class="btn btn-sm btn-secondary" >
                            <i class="fas fa-window-close" style=""></i>
                            Kembali
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    @if($errors->has('header'))
                        <span style="font-size: 80%; color: #e3342f;"><strong>{{ $errors->first('header') }}</strong></span>
                    @endif
                    <h5><strong>Header</strong></h5>
                    <div wire:ignore class="mb-3 my-2 mx-auto ">
                        <textarea required class="" id="header">
                            {{$header ?? 'Header'}}
                        </textarea>
                    </div>
                    <h5><strong>Footer</strong></h5>
                    <div wire:ignore class="mt-3 mx-auto">
                        <textarea required class="" id="footer">
                            {{$footer ?? 'Footer'}}
                        </textarea>
                    </div>
                </div>
            </form>
        </div>
        <livewire:template-surat.main :templateSurat="$templateSurat" :key="'template-surat.main'.Str::random()" >
    </div>
</div>
{{-- /.content --}}

@push('scripts')
    <script type="application/javascript">
        $(document).on('turbolinks:load turbolinks:before-cache', function() {
            tinymce.remove()
            $('.textarea-wrapper').each(function(){
                $(this).addClass('d-none')
            })
            $('.overlay').each(function(){
                $(this).removeClass('d-none')
            })
        })

        $(document).ready(function (){
            // TinyMCE
                tinymce.init({
                    selector: '#header',
                    plugins: 'print preview paste importcss searchreplace autolink autosave autoresize save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons',
                    toolbar_mode: 'wrap',
                    menubar: 'file edit view insert format tools table help',
                    toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
                    toolbar_sticky: false,
                    relative_urls : false,
                    remove_script_host : true,
                    convert_urls : false,
                    forced_root_block: false,
                    setup: function (editor) {
                        editor.on('keydown', function (event) {
                            if (event.keyCode == 9){ // tab pressed
                                editor.execCommand('mceInsertContent', false, '&emsp;&emsp;'); // inserts tab
                                event.preventDefault();
                            }
                        });

                        editor.on('init change', function () {
                            editor.save();
                        });

                        editor.on('change', function (e) {
                            @this.set('header', editor.getContent());
                        });
                    },
                });

                tinymce.init({
                    selector: '#footer',
                    plugins: 'print preview paste importcss searchreplace autolink autosave autoresize save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons',
                    toolbar_mode: 'wrap',
                    menubar: 'file edit view insert format tools table help',
                    toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
                    toolbar_sticky: false,
                    relative_urls : false,
                    remove_script_host : false,
                    convert_urls : true,
                    forced_root_block: false,
                    setup: function (editor) {
                        editor.on('keydown', function (event) {
                            if (event.keyCode == 9){ // tab pressed
                                editor.execCommand('mceInsertContent', false, '&emsp;&emsp;'); // inserts tab
                                event.preventDefault();
                            }
                        });

                        editor.on('init', function () {

                            $('.textarea-wrapper').each(function(){
                                $(this).removeClass('d-none')
                            });
                            $('.overlay').each(function(){
                                $(this).addClass('d-none')
                            });
                        });

                        editor.on('init change', function () {
                            editor.save();
                        });

                        editor.on('change', function (e) {
                            @this.set('footer', editor.getContent());
                        });
                    },
                });
            // TinyMCE
        })
    </script>
@endpush
