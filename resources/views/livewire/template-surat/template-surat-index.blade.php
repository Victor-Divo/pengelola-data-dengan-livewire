@section('title', 'Template Surat')

@section('header', 'Template Surat')

@section('breadcumb')
    <li class="breadcrumb-item active"><a href="{{route('home')}}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
    <li class="breadcrumb-item active">Template Surat</li>
@endsection

{{-- content --}}
<div class="container-fluid" wire:key="template-surat-index">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Surat Keterangan Kerja</h3>
            <div class="card-tools"><!-- Button trigger modal -->
                <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#createTemplate">
                    Tambah Template
                </button>

                <!-- Modal -->
                <div wire:ignore.self class="modal fade" id="createTemplate" tabindex="-1" aria-labelledby="createTemplateLabel" data-backdrop="static" aria-hidden="true">
                    <div class="modal-dialog">
                    <div class="modal-content">
                        <div wire:loading.class="overlay d-flex justify-content-center align-items-center" class="">
                            <span wire:loading.class="fas fa-2x fa-sync fa-spin"></span>
                        </div>
                        <div class="modal-header">
                        <h5 class="modal-title" id="createTemplateLabel">Tambah Template</h5>
                        </div>
                        <div class="modal-body">
                            <div class=" add-input">
                                <form id="create-template-surat-form" wire:submit.prevent="createTemplate">
                                    <div class="row">
                                        <div class="col">
                                            <div class="row-sm-6 mb-3 @if($errors->has('jenisSurat')) has-error @endif" wire:key="unique5">
                                                <label for="jenisSurat">Jenis Status Kepegawaian</label><span class="text-red"> *</span>
                                                <select wire:model="jenisSurat" placeholder="Pilih Jenis Status Kepegawaian" class="custom-select @error('jenisSurat') is-invalid @enderror" id="jenisSurat" required>
                                                    @forelse ($statpegsNotRelated as $statpeg)
                                                        <option selected value="">Pilih jenis status kepegawaian</option>
                                                        <option value="{{ $statpeg->id }}">{{ $statpeg->jenis }}</option>
                                                    @empty
                                                        <option value="">Semua status kepegawaian sudah memiliki template surat</option>
                                                    @endforelse
                                                </select>
                                                @if($errors->has('jenisSurat'))
                                                    <span style="font-size: 80%; color: #e3342f;"><strong>{{ $errors->first('jenisSurat') }}</strong></span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="close" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                            <button type="submit" wire:loading.attr='disabled' class="btn btn-primary" form="create-template-surat-form">Tambah</button>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            @if (session()->has('pesan'))
                <div class="alert alert-success" id="pesan">
                    {{ session('pesan') }}
                </div>
            @endif
            <div wire:ignore wire:key="suratTable">
                <table class="table table-bordered table-hover" width="100%" id="template-surat">
                    <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Jenis</th>
                        <th scope="col" style="width: 300px">Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                         @foreach ($daftar_template as $template)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{ $template->statusKepegawaian->jenis }}</td>
                                <td>
                                    <a href="{{route('template.surat.show', $template)}}" class="btn btn-sm btn-success">Edit</a>

                                    <button type="button" class="btn btn-sm btn-info text-white" data-toggle="modal" data-target="#previewTemplate{{$template->statusKepegawaian->id}}">
                                        Preview
                                    </button>
                                    <!-- Modal -->
                                    <div wire:ignore.self class="modal fade" id="previewTemplate{{$template->statusKepegawaian->id}}" tabindex="-1" aria-labelledby="previewTemplate{{$template->statusKepegawaian->id}}Label" data-backdrop="static" aria-hidden="true">
                                        <div class="modal-dialog modal-xl">
                                        <div class="modal-content">
                                            <div wire:loading.class="overlay d-flex justify-content-center align-items-center" class="">
                                                <span wire:loading.class="fas fa-2x fa-sync fa-spin"></span>
                                            </div>
                                            <div class="modal-header">
                                            <h5 class="modal-title" id="previewTemplate{{$template->statusKepegawaian->id}}Label">Preview Template</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="embed-responsive embed-responsive-16by9">
                                                    <iframe class="embed-responsive-item" src="{{route('pegawai.print.letter', [
                                                        'pegawai' => '00000000',
                                                        'penomoran' => '000',
                                                        'jenis' => $template->statusKepegawaian->id
                                                        ])}}" allowfullscreen></iframe>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" id="close" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <button class="btn btn-sm btn-danger">Hapus</button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
{{-- /.content --}}

@push('scripts')
    <script>
        $(document).ready( function () {
            let dataTable_template = $('#template-surat')
            .DataTable({
                "paging": true,
                "ordering": true,
                "searching": true,
                "order": [[ 0, "asc" ]],
                "columnDefs":[{
                    "targets" : [2],
                    "orderable" : false,
                    "searchable" : false,
                    "class": "text-center",
                    }],
            });

            document.addEventListener("turbolinks:before-cache", function() {
                if (dataTable_template != null) {
                    dataTable_template.destroy();
                    dataTable_template = null;
                }
            });
        });
    </script>
@endpush
