<div class="card mt-5">
    <form action="" wire:submit.prevent="saveContent">
        <div class="card-header">
            <h3 class="card-title">Content<span class="text-red"> *</span></h3>
            <div class="card-tools"><!-- Button trigger modal -->
                <button type="submit" class="btn btn-sm btn-primary" >
                    <i class="fas fa-save" style=""></i>
                    Simpan
                </button>
            </div>
        </div>
        <div class="card-body">
            {{-- <div wire:ignore class="overlay justify-content-center align-items-center">
                <i class="fas fa-2x fa-sync fa-spin"></i>
            </div> --}}
            @if($errors->has('main'))
                <span style="font-size: 80%; color: #e3342f;"><strong>{{ $errors->first('main') }}</strong></span>
            @endif
            <div wire:ignore class="textarea-wrapper">
                <div style="max-width: 41rem; box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06); background-color: #E5E7EB; border: none" class="p-4 mx-auto">
                    <style>
                        @import url('https://fonts.googleapis.com/css2?family=IBM+Plex+Sans&display=swap');
                        .main-input {
                            border: none;
                            border-radius: 0.25rem !important;
                            max-width: 100%;
                            background-color: #f5f5f5;
                        }

                        .c9 {
                            -webkit-text-decoration-skip:none;
                            color:#000000;
                            font-weight:700;
                            text-decoration:underline;
                            vertical-align:baseline;
                            text-decoration-skip-ink:none;
                            font-size:18pt;
                            font-family:"Times New Roman";
                            font-style:normal
                        }
                        .c6 {
                            margin-left:252pt;
                            padding-top:0pt;
                            text-indent:36pt;
                            padding-bottom:0pt;
                            line-height:1.15;
                            orphans:2;
                            widows:2;
                            text-align:center
                        }

                        .c7 {
                            margin-left:36pt;
                            padding-top:0pt;
                            padding-bottom:0pt;
                            line-height:1.15;
                            orphans:2;
                            widows:2;
                            text-align:right
                        }

                        .c0 {
                            padding-top:0pt;
                            padding-bottom:0pt;
                            line-height:1.15;
                            orphans:2;
                            widows:2;
                            text-align:left;
                            height:11pt
                        }

                        .c4 {
                            padding-top:0pt;
                            padding-bottom:0pt;
                            line-height:1.15;
                            orphans:2;
                            widows:2;
                            text-align:left
                        }

                        .c11 {
                            padding-top:0pt;
                            padding-bottom:0pt;
                            line-height:1.15;
                            orphans:2;
                            widows:2;
                            text-align:justify
                        }

                        .c8 {
                            padding-top:0pt;
                            padding-bottom:0pt;
                            line-height:1.15;
                            orphans:2;
                            widows:2;
                            text-align:center
                        }

                        .c15 {
                            padding-top:0pt;
                            padding-bottom:0pt;
                            line-height:1.15;
                            orphans:2;
                            widows:2;
                            text-align:right
                        }

                        .c2 {
                            color:#000000;
                            text-decoration:none;
                            vertical-align:baseline;
                            font-style:normal
                        }

                        .c13 {
                            font-weight:400;
                            font-size:10pt;
                            font-family:"IBM Plex Sans"
                        }

                        .c3 {
                            font-weight:500;
                            font-size:14pt;
                            font-family:"IBM Plex Sans"
                        }

                        .c5 {
                            font-size:12pt;
                            font-family:"Times New Roman";
                            font-weight:400
                        }

                        .c10 {
                            font-weight:400;
                            font-size:11pt;
                            font-family:"Arial"
                        }

                        .c14 {
                            margin: 0px auto;
                        }

                        .c1 {
                            font-size:12pt;
                            font-family:"Times New Roman";
                            font-weight:700
                        }

                        .c12 {
                            height:11pt
                        }

                        .title {
                            padding-top:0pt;
                            color:#000000;
                            font-size:26pt;
                            padding-bottom:3pt;
                            font-family:"Arial";
                            line-height:1.15;
                            page-break-after:avoid;
                            orphans:2;
                            widows:2;
                            text-align:left
                        }

                        .subtitle {
                            padding-top:0pt;
                            color:#666666;
                            font-size:15pt;
                            padding-bottom:16pt;
                            font-family:"Arial";
                            line-height:1.15;
                            page-break-after:avoid;
                            orphans:2;
                            widows:2;
                            text-align:left
                        }

                        .p {
                            margin:0;
                            color:#000000;
                            font-size:11pt;
                            font-family:"Arial"
                        }
                    </style>
                    <div wire:ignore class="c14">
                        <br>
                        <p class="c8">
                            <span class="c9"><input required type="text" id="main.judul" style="" class="main-input " maxlength="" minlength="10" value="{{$main['judul']}}"></span>
                        </p>
                        <p class="c8">
                            <span class="c1">Nomor : 001/<input required type="text" id="main.jenis_surat" style="min-width: 2rem" minlength="2" maxlength="2" value="{{$main['jenis_surat']}}" class="main-input">/<input required type="text" style="min-width: 2rem" id="main.inisial" value="{{$main['inisial']}}" class="main-input" minlength="2" maxlength="2">/IV/2021</span>
                        </p>

                        <p class="c8 c12"><span class="c9"></span></p>
                        <br>

                        <p class="c4" style="margin-bottom:5px">
                            <span class="c2 c5"><input required style="max-width: 36rem" type="text" id="main.pembuka" value="{{$main['pembuka']}}" class="main-input"></span>
                        </p>

                        <table style="width: 300px;">
                            <thead>
                                <tr>
                                    <th style="width: 10px"></th>
                                    <th style="width: 1px"></th>
                                    <th style="width: 30px"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="vertical-align: text-top"><p class="p"><span class="c5">Nama</span></td>
                                    <td style="vertical-align: baseline">:</td>
                                    <td style="vertical-align: baseline"><span class="c2 c1"><input required type="text" class="main-input" style="max-width: 34rem" id="main.ttd_nama" value="{{$main['ttd_nama']}}"></span></p></td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: baseline"><p class="p"><span class="c5">Jabatan</span></td>
                                    <td style="vertical-align: baseline">:</td>
                                    <td style="vertical-align: baseline"><span class="c2 c1"><input required type="text" class="main-input" style="max-width: 34rem" id="main.ttd_jabatan" value="{{$main['ttd_jabatan']}}"></span></p></td>
                                </tr>
                            </tbody>
                        </table>

                        <p class="c0"><span class="c2 c5"></span></p>

                        <p class="c4">
                            <span class="c2 c5"><input required type="text" class="main-input" id="main.keterangan" value="{{$main['keterangan']}}"></span>
                        </p>

                        <table style="width: 300px;">
                            <thead>
                                <tr>
                                    <th style="width: 10px"></th>
                                    <th style="width: 1px"></th>
                                    <th style="width: 30px"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="vertical-align: baseline"><p class="p"><span class="c5">Nama</span></td>
                                    <td style="vertical-align: baseline">:</td>
                                    <td style="vertical-align: baseline"><span class="c2 c1">Nama Pegawai</span></p></td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: baseline"><p class="p"><span class="c5">TTL</span></td>
                                        <td style="vertical-align: baseline">:</td>
                                        <td class="text-capitalize" style="vertical-align: baseline"><span class="c2 c1">Tempat, Tanggal Lahir</span></p></td>
                                    </tr>
                                <tr>
                                    <td style="vertical-align: baseline"><p class="p"><span class="c5">Alamat</span></td>
                                    <td style="vertical-align: baseline">:</td>
                                    <td style="vertical-align: baseline"><span class="c2 c1">Alamat rumah pegawai</span></p></td>
                                </tr>
                            </tbody>
                        </table>

                        <p class="c4"><span class="c2 c5">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        </p>

                        <p class="c11"><span class="c5"><input required type="text" class="main-input" id="main.pernyataan_1" value="{{$main['pernyataan_1']}}"> Tanggal Masuk <input required type="text" class="main-input" id="main.pernyataan_2" value="{{$main['pernyataan_2']}}"> Tanggal Berakhir. <input required type="text" class="main-input" id="main.pernyataan_3" value="{{$main['pernyataan_3']}}"></span><span class="c1"> Jabatan</span>.
                        </p>

                        <p class="c11 c12"><span class="c2 c5"></span>
                        </p>

                        <p class="c11"><span class="c2 c5"><input required type="text" style="max-width: 31rem" class="main-input" id="main.isi_1" value="{{$main['isi_1']}}">Nama Pegawai <textarea required name="" class="main-input" id="main.isi_2" cols="71" rows="5">{{$main['isi_2']}}</textarea></span>
                        </p>

                        <p class="c11 c12"><span class="c2 c5"></span>
                        </p>

                        <p class="c11"><span class="c2 c5"><textarea required name="" class="main-input" id="main.harapan" cols="71" rows="4">{{$main['harapan']}}</textarea></span>
                        </p>

                        <p class="c11 c12"><span class="c2 c5"></span>
                        </p>

                        <p class="c11"><span class="c2 c5"><textarea required name="" class="main-input" id="main.penutup" cols="71" rows="3">{{$main['penutup']}}</textarea></span>
                        </p>

                        <p class="c0"><span class="c2 c5"></span>
                        </p>
                        <br>

                        <p class="c6"><span class="c2 c5"><input required type="text" class="main-input" style="max-width:38%" id="main.lokasi" value="{{$main['lokasi']}}">, {{\Carbon\Carbon::now()->isoFormat('D MMMM Y')}}</span>
                        </p>

                        <p class="c12 c15"><span class="c2 c5"></span>
                        </p>

                        <p class="c0"><span class="c2 c5"></span>
                        </p>

                        <p class="c0"><span class="c2 c5"></span>
                        </p>

                        <p class="c0"><span class="c2 c5"></span>
                        </p>

                        <p class="c0"><span class="c2 c5"></span>
                        </p>

                        <p class="c0"><span class="c2 c5"></span>
                        </p>

                        <p class="c6"><span class="c2 c5"><input required style="max-width: 85%" type="text" class="main-input" id="main.tdtgn_jabatan" value="{{$main['tdtgn_jabatan']}}"></span>
                        </p>

                        <p class="c6"><span class="c2 c5"><input required style="max-width: 85%"  type="text" class="main-input" id="main.tdtgn_nama" value="{{$main['tdtgn_nama']}}"></span>
                        </p>

                        <p class="c0"><span class="c2 c5"></span></p>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@push('scripts')
    <script>
        $( document ).ready(function() {
            $('.main-input').each(function(){
                // $(this).attr("size", $(this).val().length)
                $(this).css("width", $(this).val().length - 1 + "rem")
                let input_id = $(this).attr('id');
                $(this).on('input', function (e) {
                    console.log((e.target.value.length + 20) + '%')
                    // document.getElementById(e.target.id).setAttribute('size', e.target.value.length);
                    document.getElementById(e.target.id).style.width =  e.target.value.length - 1 + "rem";
                    @this.set(e.target.id, $(this).val())
                })
            });
        })
    </script>
@endpush
