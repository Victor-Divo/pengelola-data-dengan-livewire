@section('title', 'Dashboard')


@section('styles')
    <style>
        .content-wrapper {
            background-image: '';
            background-repeat: no-repeat;
            background-size: cover;
            -webkit-transition: background-image 0.2s ease-in-out;
            transition: background-image 0.2s ease-in-out;
        }

        .content {
            height: 709px;
            color: #333333 !important; 
        }
    </style>
@endsection

{{-- content --}}
<div class="container-fluid">
    <div class="card" style="background-color: #ffffffcc">
        <div class="card-header">
            <h3 class="card-title">This Month Birthday</h3>
            <div class="card-tools">
                <select style="background-color: #ffffff80;" id="selek" class="form-control form-control-sm w-auto">
                    <option value="">Semua</option>
                    <option value="Hari Ini">Hari Ini</option>
                    <option value="Sudah Lewat">Sudah Lewat</option>
                    <option value="Belum Lewat">Belum Lewat</option>
                </select>
            </div>
        </div>
        <div class="card-body">
            <h6>Tanggal Hari ini {{ $now }}</h6>
            <h6 class="">Ucapkan selamat ulang tahun kepada :</h6>
            <table class="table table-bordered table-hover" width="100%" id="example2">
                <thead>
                <tr>
                    <th scope="col">Nama</th>
                    <th scope="col">Umur</th>
                    <th scope="col">Tanggal</th>
                    <th scope="col">Status</th>
                </tr>
                </thead>
                <tbody>
                     @forelse ($birthThisMonth as $birth)
                        @php
                            $tanggal = Str::substr($birth->tanggal_lahir, 0, 2);
                            $bulan = Str::substr($birth->tanggal_lahir, 3, 2);
                            $tahun = Str::substr($birth->tanggal_lahir, 6);
                            $setDateNormal = ($tahun.'-'.$bulan.'-'.$tanggal);
                            $pegBirth = \Carbon\CarbonImmutable::parse($setDateNormal);
                            $currentDay = \Carbon\Carbon::now()->isoFormat('D');
                            $age = \Carbon\CarbonImmutable::parse($setDateNormal)->age;
                            $ageThisYear = \Carbon\CarbonImmutable::parse($setDateNormal)->age;
                            $whenBirth = $pegBirth->addYear($age);
                        @endphp
                        <tr>
                            <td>{{ $birth->nama }}</td>
                            @if ( $whenBirth->isoFormat('D') == $currentDay )  
                                <td>{{$ageThisYear}} tahun</td>
                                <td>{{$whenBirth = $pegBirth->addYear($age)->isoFormat('dddd, D MMMM Y')}}</td>
                                <td>Hari Ini</td>
                            @elseif ( $whenBirth->isoFormat('D') > $currentDay )
                                <td>{{$ageThisYear += 1}} tahun</td>
                                <td>{{$whenBirth = $pegBirth->addYear($age + 1)->isoFormat('dddd, D MMMM Y')}}</td>
                                <td>Belum Lewat</td>
                            @elseif ( $whenBirth->isoFormat('D') < $currentDay )
                                <td>{{$ageThisYear}} tahun</td>
                                <td>{{$whenBirth = $pegBirth->addYear($age)->isoFormat('dddd, D MMMM Y')}}</td>
                                <td>Sudah Lewat</td>
                            @endif
                        </tr>
                    @empty 
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
{{-- /.content --}}

@push('scripts')
<script type="text/javascript">
    $(document).ready( function () {
        let dataTable = $('#example2')
        .DataTable({
            initComplete: function () {
                this.api().columns([3]).every( function () {
                    let column = this;
                    let img = document.querySelectorAll('.imgbd');
                    let bg = document.querySelectorAll('.content-wrapper')[0];
                    let content = document.querySelectorAll('.content')[0];
                    let select = $('#selek')
                        .on('change', function (e) {
                            let val = e.target.value;
                            column
                                .search( val ? '^'+val+'$' : '', true, false )
                                .draw();
                            let empty = document.getElementsByClassName('dataTables_empty')[0];
                            if (empty) {
                                empty.innerHTML="Tidak ada pegawai yang berulang tahun";
                                img.forEach( e => e.style.display = 'none');
                                bg.style.backgroundImage = `url("{{asset('img/nobirth-min.png')}}")`;
                            } 
                            else {
                                img.forEach( e => e.style.display = '');
                                bg.style.backgroundImage = `url("{{asset('img/home2-min.png')}}")`;
                            }
                            $('.previous .page-link, .next .page-link').each(function() {
                                this.style.backgroundColor = ' #ffffff80';
                            }); 
                        } );
                } );
            },
            "paging": true,
            "pageLength": 7,
            "ordering": true,
            "searching": true,
            "lengthChange": false,
            "autoWidth": false,
            "responsive": false,
            "order": [[ 0, "asc" ]],
            "columnDefs":[{
                "targets" : [3],
                "orderable" : false,
                "class": "text-center d-none",
                }],
        });

        document.addEventListener("turbolinks:before-cache", function() {
            if (dataTable != null) {
                dataTable.destroy();
                dataTable = null;
            }
        });
        $('#selek').val('Hari Ini').trigger('change');
        // $('#selek').val('Belum Lewat').trigger('change');
        $('#example2_filter').attr('hidden', true);
    });
</script>
@endpush