<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Document</title>
</head>
<body>
    <style>
        tr > td {
            vertical-align: middle;
        }
    </style>
    
    <table>
        {{-- {{dd($pegawai[0]->statusKepegawaian)}} --}}
        <thead>
            <tr>
                <th valign="middle"><strong>Nip</strong> </th>
                <th valign="middle"><strong>Nama</strong></th>
                <th valign="middle"><strong>Tempat Lahir</strong></th>
                <th valign="middle"><strong>Tanggal Lahir</strong></th>
                <th valign="middle"><strong>Alamat</strong></th>
                <th valign="middle"><strong>Jenis Kelamin</strong></th>
                <th valign="middle"><strong>Agama</strong></th>
                <th valign="middle"><strong>Status Pekawinan</strong></th>
                <th valign="middle"><strong>Golongan Darah</strong></th>
                <th valign="middle"><strong>Pendidikan Terakhir</strong></th>
                <th valign="middle"><strong>Nomor KTP</strong></th>
                <th valign="middle"><strong>Nomor KK</strong></th>
                <th valign="middle"><strong>Nomor NPWP</strong></th>
                <th valign="middle"><strong>Nomor BPJS Kesehatan</strong></th>
                <th valign="middle"><strong>Nomor BPJS Ketenagakerjaan</strong></th>
                <th valign="middle"><strong>Status Kepegawaian</strong></th>
                <th valign="middle"><strong>Departemen</strong></th>
                <th valign="middle"><strong>Jabatan</strong></th>
                <th valign="middle"><strong>Nomor HP</strong></th>
                <th valign="middle"><strong>Email</strong></th>
            </tr>
        </thead>
        <tbody>
            @foreach($pegawai as $peg)
            <tr>
                <td valign="middle">{{ $peg->nip ?? '-'}}</td>
                <td valign="middle">{{ $peg->nama ?? '-'}}</td>
                <td valign="middle">{{ $peg->tempat_lahir ?? '-'}}</td>
                <td valign="middle">{{ $peg->tanggal_lahir ?? '-'}}</td>
                <td valign="middle">{{ $peg->alamat ?? '-'}}</td>
                <td valign="middle">{{ $peg->jenis_kelamin ?? '-'}}</td>
                <td valign="middle">{{ $peg->agama ?? '-'}}</td>
                <td valign="middle">{{ $peg->statusPerkawinan->nama ?? '-'}}</td>
                <td valign="middle">{{ $peg->golongan_darah ?? '-'}}</td>
                <td valign="middle">{{ $peg->pendidikan->nama ?? '-'}}</td>
                <td valign="middle">{{ $peg->nik ?? '-'}}</td>
                <td valign="middle">{{ $peg->no_kk ?? '-' }}</td>
                <td valign="middle">{{ $peg->no_npwp ?? '-' }}</td>
                <td valign="middle">{{ $peg->no_bpjs_kes ?? '-' }}</td>
                <td valign="middle">{{ $peg->no_bpjs_ket ?? '-' }}</td>
                
                <td valign="middle">
                    @foreach ($peg->statusKepegawaian as $stat_kep)
                    {{ $stat_kep->jenis  ?? '-' }} ({{ $stat_kep->pivot->tanggal_mulai ? \Carbon\Carbon::parse($stat_kep->pivot->tanggal_mulai)->isoFormat('D MMMM Y') : '-'}} s/d {{ $stat_kep->pivot->tanggal_berakhir ? \Carbon\Carbon::parse($stat_kep->pivot->tanggal_berakhir)->isoFormat('D MMMM Y') : ' - ' }}) {!! $loop->last ? '' : '<br>'!!}
                    @endforeach
                </td>
                
                <td valign="middle">{{ $peg->departemen->nama  ?? '-'}}</td>
                <td valign="middle">{{ $peg->jabatan->nama ?? '-' }}</td>
                <td valign="middle">{{ $peg->phone ?? '-' }}</td>
                <td valign="middle">{{ $peg->email ?? '-' }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>