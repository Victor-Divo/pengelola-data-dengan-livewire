<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Surat Keterangan Telah Bekerja</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=IBM+Plex+Sans&display=swap" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}

    <style>
        @page {
            margin: 0cm 0cm;
        }

        * {
            /* border: 1px solid #000000; */
        }

        body{
            position: fixed;
            top: 4cm ;
            right: 2.5cm ;
            bottom: 3cm ;
            left: 2.5cm ;
        }

        header {
            position: fixed;
            top: 1cm;
            left: 0cm;
            right: 0cm;

            /** Extra personal styles **/
            /* background-color: #03a9f4;
            color: white;
            text-align: center;
            line-height: 1.5cm; */
        }

        /** Define the footer rules **/
        .footer {
            position: fixed;
            bottom: 1cm;
            left: 0cm;
            right: 0cm;
            height: 2cm;

            /** Extra personal styles **/
            /* background-color: #03a9f4;
            color: white;
            text-align: center;
            line-height: 1.5cm; */
        }

        .c9 {
            -webkit-text-decoration-skip:none;
            color:#000000;
            font-weight:700;
            text-decoration:underline;
            vertical-align:baseline;
            text-decoration-skip-ink:none;
            font-size:18pt;
            font-family:"Times New Roman";
            font-style:normal
        }
        .c6 {
            margin-left:252pt;
            padding-top:0pt;
            text-indent:36pt;
            padding-bottom:0pt;
            line-height:1.15;
            orphans:2;
            widows:2;
            text-align:center
        }

        .c7 {
            margin-left:36pt;
            padding-top:0pt;
            padding-bottom:0pt;
            line-height:1.15;
            orphans:2;
            widows:2;
            text-align:right
        }

        .c0 {
            padding-top:0pt;
            padding-bottom:0pt;
            line-height:1.15;
            orphans:2;
            widows:2;
            text-align:left;
            height:11pt
        }

        .c4 {
            padding-top:0pt;
            padding-bottom:0pt;
            line-height:1.15;
            orphans:2;
            widows:2;
            text-align:left
        }

        .c11 {
            padding-top:0pt;
            padding-bottom:0pt;
            line-height:1.15;
            orphans:2;
            widows:2;
            text-align:justify
        }

        .c8 {
            padding-top:0pt;
            padding-bottom:0pt;
            line-height:1.15;
            orphans:2;
            widows:2;
            text-align:center
        }

        .c15 {
            padding-top:0pt;
            padding-bottom:0pt;
            line-height:1.15;
            orphans:2;
            widows:2;
            text-align:right
        }

        .c2 {
            color:#000000;
            text-decoration:none;
            vertical-align:baseline;
            font-style:normal
        }

        .c13 {
            font-weight:400;
            font-size:10pt;
            font-family:"IBM Plex Sans"
        }

        .c3 {
            font-weight:500;
            font-size:14pt;
            font-family:"IBM Plex Sans"
        }

        .c5 {
            font-size:12pt;
            font-family:"Times New Roman";
            font-weight:400
        }

        .c10 {
            font-weight:400;
            font-size:11pt;
            font-family:"Arial"
        }

        .c14 {
            margin: 0px auto;
        }

        .c1 {
            font-size:12pt;
            font-family:"Times New Roman";
            font-weight:700
        }

        .c12 {
            height:11pt
        }

        .title {
            padding-top:0pt;
            color:#000000;
            font-size:26pt;
            padding-bottom:3pt;
            font-family:"Arial";
            line-height:1.15;
            page-break-after:avoid;
            orphans:2;
            widows:2;
            text-align:left
        }

        .subtitle {
            padding-top:0pt;
            color:#666666;
            font-size:15pt;
            padding-bottom:16pt;
            font-family:"Arial";
            line-height:1.15;
            page-break-after:avoid;
            orphans:2;
            widows:2;
            text-align:left
        }

        li {
            color:#000000;
            font-size:11pt;
            font-family:"Arial"
        }

        p {
            margin:0;
            color:#000000;
            font-size:11pt;
            font-family:"Arial"
        }

        h1 {
            padding-top:20pt;
            color:#000000;
            font-size:20pt;
            padding-bottom:6pt;
            font-family:"Arial";
            line-height:1.15;
            page-break-after:avoid;
            orphans:2;
            widows:2;
            text-align:left
        }

        h2 {
            padding-top:18pt;
            color:#000000;
            font-size:16pt;
            padding-bottom:6pt;
            font-family:"Arial";
            line-height:1.15;
            page-break-after:avoid;
            orphans:2;
            widows:2;
            text-align:left
        }

        h3 {
            padding-top:16pt;
            color:#434343;
            font-size:14pt;
            padding-bottom:4pt;
            font-family:"Arial";
            line-height:1.15;
            page-break-after:avoid;
            orphans:2;
            widows:2;
            text-align:left
        }

        h4 {
            padding-top:14pt;
            color:#666666;
            font-size:12pt;
            padding-bottom:4pt;
            font-family:"Arial";
            line-height:1.15;
            page-break-after:avoid;
            orphans:2;
            widows:2;
            text-align:left
        }

        h5 {
            padding-top:12pt;
            color:#666666;
            font-size:11pt;
            padding-bottom:4pt;
            font-family:"Arial";
            line-height:1.15;
            page-break-after:avoid;
            orphans:2;
            widows:2;
            text-align:left
        }

        h6 {
            padding-top:12pt;
            color:#666666;
            font-size:11pt;
            padding-bottom:4pt;
            font-family:"Arial";
            line-height:1.15;
            page-break-after:avoid;
            font-style:italic;
            orphans:2;
            widows:2;
            text-align:left
        }
    </style>
  </head>
  <body>
    <header>
        {{-- <div style="background-color: #3b78b5">      --}}
            {{-- <img src="{{ asset('img/header.png') }}" style="width: 100%; height:90%" alt=""> --}}
        {{-- </div> --}}
        {!! $header !!}
    </header>

    <div class="footer">
        <div class="float-right" style="width: 25%;height:50%">
            {!! $footer !!}
        </div>
        {{-- <img class="float-right" src="{{ asset('img/footer.png') }}" style="width: 25%;height:50%" alt=""> --}}
    </div>

    <div class="main" >
        <div class="c14">
            <br>
            <p class="c8">
                <span class="c9">{{$main->judul}}</span>
            </p>

            <p class="c8">
                <span class="c1">Nomor : </span><span class="c1">{{$penomoran}}</span><span class="c1">/</span><span class="c1">{{$main->jenis_surat}}</span><span class="c1">/</span><span class="c1">{{$main->inisial}}</span><span class="c1">/</span><span class="c1">{{$month}}</span><span class="c1">/</span><span class="c1">{{date('Y')}}</span>
            </p>

            <p class="c8 c12"><span class="c9"></span></p>
            <br>

            <p class="c4" style="margin-bottom:5px">
                <span class="c2 c5">{{$main->pembuka}}</span>
            </p>

            <table style="width: 300px;">
                <thead>
                    <tr>
                        <th style="width: 10px"></th>
                        <th style="width: 1px"></th>
                        <th style="width: 30px"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="vertical-align: text-top"><p><span class="c5">Nama</span></td>
                        <td style="vertical-align: baseline">:</td>
                        <td style="vertical-align: baseline"><span class="c2 c1">{{$main->ttd_nama}}</span></p></td>
                    </tr>
                    <tr>
                        <td style="vertical-align: baseline"><p><span class="c5">Jabatan</span></td>
                        <td style="vertical-align: baseline">:</td>
                        <td style="vertical-align: baseline"><span class="c2 c1">{{$main->ttd_jabatan}}</span></p></td>
                    </tr>
                </tbody>
            </table>

            <p class="c0"><span class="c2 c5"></span></p>

            <p class="c4">
                <span class="c2 c5">{{$main->keterangan}}</span>
            </p>

            <table style="width: 300px;">
                <thead>
                    <tr>
                        <th style="width: 10px"></th>
                        <th style="width: 1px"></th>
                        <th style="width: 30px"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="vertical-align: baseline"><p><span class="c5">Nama</span></td>
                        <td style="vertical-align: baseline">:</td>
                        <td style="vertical-align: baseline"><span class="c2 c1">{{$pegawai->nama}}</span></p></td>
                    </tr>
                    <tr>
                        <td style="vertical-align: baseline"><p><span class="c5">TTL</span></td>
                            <td style="vertical-align: baseline">:</td>
                            <td class="text-capitalize" style="vertical-align: baseline"><span class="c2 c1">{{ $pegawai->tempat_lahir }}, {{ \Carbon\Carbon::createFromFormat('d/m/Y', $pegawai->tanggal_lahir)->isoFormat('D MMMM Y') }}</span></p></td>
                        </tr>
                    <tr>
                        <td style="vertical-align: baseline"><p><span class="c5">Alamat</span></td>
                        <td style="vertical-align: baseline">:</td>
                        <td style="vertical-align: baseline"><span class="c2 c1">{{$pegawai->alamat}}</span></p></td>
                    </tr>
                </tbody>
            </table>

            <p class="c4"><span class="c2 c5">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            </p>

            <p class="c11"><span class="c5">{{$main->pernyataan_1}} {{$stat_kep ? \Carbon\Carbon::parse($stat_kep->pivot->tanggal_mulai)->isoFormat('D MMMM Y') : \Carbon\Carbon::now()->isoFormat('D MMMM Y')}} {{$main->pernyataan_2}} {{$stat_kep ? \Carbon\Carbon::parse($stat_kep->pivot->tanggal_berakhir)->isoFormat('D MMMM Y') : \Carbon\Carbon::now()->isoFormat('D MMMM Y')}}. {{$main->pernyataan_3}} </span><span class="c1">{{$pegawai->jabatan->nama}}</span>.
            </p>

            <p class="c11 c12"><span class="c2 c5"></span>
            </p>

            <p class="c11"><span class="c2 c5">{{$main->isi_1}} {{$pegawai->nama}} {{$main->isi_2}}</span>
            </p>

            <p class="c11 c12"><span class="c2 c5"></span>
            </p>

            <p class="c11"><span class="c2 c5">{{$main->harapan}}</span>
            </p>

            <p class="c11 c12"><span class="c2 c5"></span>
            </p>

            <p class="c11"><span class="c2 c5">{{$main->penutup}}</span>
            </p>

            <p class="c0"><span class="c2 c5"></span>
            </p>
            <br>

            <p class="c6"><span class="c2 c5">{{$main->lokasi}}, {{\Carbon\Carbon::now()->isoFormat('D MMMM Y')}}</span>
            </p>

            <p class="c12 c15"><span class="c2 c5"></span>
            </p>

            <p class="c0"><span class="c2 c5"></span>
            </p>

            <p class="c0"><span class="c2 c5"></span>
            </p>

            <p class="c0"><span class="c2 c5"></span>
            </p>

            <p class="c0"><span class="c2 c5"></span>
            </p>

            <p class="c0"><span class="c2 c5"></span>
            </p>

            <p class="c6"><span class="c2 c5">{{$main->tdtgn_jabatan}}</span>
            </p>

            <p class="c6"><span class="c2 c5">{{$main->tdtgn_nama}}</span>
            </p>

            <p class="c0"><span class="c2 c5"></span></p>
        </div>
    </div>


</body>
</html>
