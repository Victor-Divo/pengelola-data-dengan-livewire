@extends('layouts.app')

@section('title', 'Login')

@section('styles')
<style>
    html::before{
        content: "";
        background-image: url( "{{asset('img/desktop17-min.png')}}" );
        background-repeat: no-repeat;
        background-size: 100%;
        position: absolute;
        top: 0px ;
        right: 0px ;
        bottom: 0px ;
        left: 0px ;
        opacity: 0.5;
    }

    body{
        background-color:transparent;
        opacity: 1.0;
    }

    .tombol {
        font-family: 'Poppins', sans-serif;
        font-style: normal;
        font-weight: 600;
        font-size: 15px;
        line-height: 30px;

        color: #ffffff;
        padding: 0 ;
        background-color: #76BADE;
    }
    
    .reg {
        font-family: 'Poppins', sans-serif;
        font-style: normal;
        font-weight: 600;
        font-size: 13.3px;
        line-height: 30px;

        color: #ffffff;
    }

    .tombol:hover {
        background-color: #0977AA;
    }

    #azura {
        font-family: 'Poppins', sans-serif;
        font-style: normal;
        font-weight: bold;
        font-size: 30px;
        line-height: 67px;
        margin: 0;

        /* primary */

        color: #0977AA;
    }
    #signin {
        font-family: 'Poppins', sans-serif;
        font-style: normal;
        font-weight: bold;
        font-size: 25px;
        line-height: 67px;
        margin-top: -7px;
        /* primary */

        color: #FFFFFF;
    }

    #username:focus, #password:focus{
    border:none !important;
    box-shadow:none;
    }

</style>
@endsection

@section('standalone')
<div class="container" style="margin:40px auto 0px;">
    <div class="row justify-content-center">
        <div class="col-md-8">      
            <div class="card border-0 text-center shadow-none" style="background-color: transparent;">
                <img class="card-img-top mx-auto" src="{{ asset('img/logo.png') }}" style="width: 10%;" alt="Card image cap">
                <h4 id="azura">Azura Labs</h4>
                <div class="card-body d-flex justify-content-center" style="padding: 0.8rem">
                    <div class="card" style="width: 380px; background-color : #2499BC;box-shadow: 10px 10px 10px 10px 0.7;">
                        <div class="card-body pt-1">
                            <h6 id="signin">Sign In</h6>
                            <form method="POST" action="{{ route('login') }}" autocomplete="off">
                                @csrf
                                <div class="form-group row d-flex justify-content-center">
                                    <div class="col-md-10" style="">
                                        <div class="input-group ">
                                            <input id="username" style="border:0px;" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required placeholder="Username" autocomplete="off">
                                            <div class="input-group-append" style="border-radius: 0px 4px 4px 0px;">
                                                <div class="input-group-text" style="background: #FFFFFF; border:solid #FFF;">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                            </div>
                                            @error('username')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
    
                                <div class="form-group row d-flex justify-content-center">
                                    <div class="col-md-10">
                                        <div class="input-group">
                                            <input id="password" style="border:0px;" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required placeholder="Password" autocomplete="off">
                                            <div class="input-group-append" style="border-radius: 0px 4px 4px 0px;">
                                                <div class="input-group-text btn" style="background: #FFFFFF; border:solid #FFF;">
                                                    <span class="far fa-eye" id="togglePassword"></span>
                                                </div>
                                            </div>
                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
    
                                <div class="form-group row d-flex justify-content-center">
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col d-flex justify-content-start">
                                                <div class="form-check mb-1" style="text-align: left; font-size:100%">
                                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                                    
                                                    <label class="form-check-label text-white" for="remember">
                                                        Remember Me
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col d-flex justify-content-end" style="text-align:right;">
                                                <div class="" style="display:inline-block;">
                                                    @if (Route::has('password.request'))
                                                        <a class="text-white" href="{{ route('password.request') }}">
                                                            Forgot Password
                                                        </a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row d-flex justify-content-center">
                                    <div class="col-md-10 text-left">
                                        <button type="submit" class="btn btn-block tombol">
                                            Login 
                                        </button>
                                        @if (Route::has('register'))
                                            <a id="register" class="reg" href="{{ route('register') }}">Doesn't Have Account?</a>
                                        @endif 
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    $(function(){
    const togglePassword = document.querySelector('#togglePassword');
    const password = document.querySelector('#password');
    togglePassword.addEventListener('click', function (e) {
    // toggle the type attribute
    const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
    password.setAttribute('type', type);
    // toggle the eye slash icon
    this.classList.toggle('fa-eye-slash');
});
});
</script>
@endpush
