@extends('layouts.app')

@section('title', 'Login')

@section('styles')
<style>
    html::before{
        content: "";
        background-image: url( "{{url(asset('img/desktop17.png'))}}" );
        background-repeat: no-repeat;
        background-size: 100%;
        position: absolute;
        top: 0px ;
        right: 0px ;
        bottom: 0px ;
        left: 0px ;
        opacity: 0.5;
    }

    body{
        background-color:transparent;
        opacity: 1.0;
    }

    .tombol {
        font-family: 'Poppins', sans-serif;
        font-style: normal;
        font-weight: 600;
        font-size: 15px;
        line-height: 30px;

        color: #ffffff;
        padding: 0 ;
        background-color: #76BADE;
    }
    
    .reg {
        font-family: 'Poppins', sans-serif;
        font-style: normal;
        font-weight: 600;
        font-size: 13.3px;
        line-height: 30px;

        color: #ffffff;
    }

    .tombol:hover {
        background-color: #0977AA;
    }

    #azura {
        font-family: 'Poppins', sans-serif;
        font-style: normal;
        font-weight: bold;
        font-size: 30px;
        line-height: 67px;
        margin: 0;

        /* primary */

        color: #0977AA;
    }
    #signin {
        font-family: 'Poppins', sans-serif;
        font-style: normal;
        font-weight: bold;
        font-size: 25px;
        line-height: 67px;
        margin-top: -7px;
        /* primary */

        color: #FFFFFF;
    }
    
    #username:focus, #password:focus{
    border:none !important;
    box-shadow:none;
    }
</style>
@endsection

@section('standalone')
<div class="container" style="margin:40px auto 0px;">
    <div class="row justify-content-center">
        <div class="col-md-8">      
            {{-- <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>
    
                <div class="card-body">
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf
    
                        <input type="hidden" name="token" value="{{ $token }}">
    
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
    
                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
    
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
    
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
    
                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
    
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
    
                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>
    
                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>
    
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div> --}}
            <div class="card border-0 text-center shadow-none" style="background-color: transparent;">
                <img class="card-img-top mx-auto" src="{{ asset('img/logo.png') }}" style="width: 10%;" alt="Card image cap">
                <h4 id="azura">Azura Labs</h4>
                <div class="card-body d-flex justify-content-center" style="padding: 0.8rem">
                    <div class="card" style="width: 380px; background-color : #2499BC;box-shadow: 10px 10px 10px 10px 0.7;">
                        <div class="card-body pt-1">
                            <h6 id="signin">Reset Password</h6>
                            <form method="POST" action="{{ route('password.update') }}" id="register-form">
                                @csrf
                                <input type="hidden" name="token" value="{{ $token }}">
                                <div class="form-group row d-flex justify-content-center">
                                    <div class="col-md-10" style="">
                                        <div class="input-group ">
                                            <input id="email" style="border:0px;" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required placeholder="Email Address" autocomplete="off">
                                            <div class="input-group-append" style="border-radius: 0px 4px 4px 0px;">
                                                <div class="input-group-text" style="background: #fff; border:solid #fff;">
                                                    <i class="fas fa-envelope"></i>
                                                </div>
                                            </div>
                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
    
                                <div class="form-group row d-flex justify-content-center">
                                    <div class="col-md-10" style="">
                                        <div class="input-group">
                                            <input id="password" style="border:0px;" type="password" class="form-control pwbos @error('password') is-invalid @enderror" name="password" data-parsley-required="true" placeholder="Password" data-parsley-validation-threshold="0" autocomplete="off" data-parsley-minlength="8" data-parsley-trigger="keyup" data-parsley-errors-container="#pwerr" data-parsley-minlength-message="min 8 character">
                                            <div class="input-group-append" style="border-radius: 0px 4px 4px 0px;">
                                                <div class="input-group-text btn" style="background: #FFFFFF; border:solid #FFF;  border-radius: inherit;">
                                                    <span class="far fa-eye" id="togglePassword"></span>
                                                </div>
                                            </div>
                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div id="pwerr" class="text-red text-left"></div>
                                    </div>
                                </div>
    
                                <div class="form-group row d-flex justify-content-center">
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col d-flex justify-content-start">
                                                <input id="password-confirm" type="password" class="form-control pwbos" name="password_confirmation" required data-parsley-equalto="#password" autocomplete="new-password" placeholder="Confirm Password" data-parsley-validation-threshold="0" data-parsley-minlength="8" data-parsley-errors-container="#pwcerr" data-parsley-trigger="keyup">
                                            </div>
                                        </div>
                                        <div id="pwcerr" class="text-red text-left"></div>
                                    </div>
                                </div>

                                <div class="form-group row d-flex justify-content-center">
                                    <div class="col-md-10 text-left">
                                        <button type="submit" class="btn btn-block tombol">
                                            Reset Password 
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="application/javascript">
    $(function(){
        jQuery.noConflict();
        const togglePassword = document.querySelector('#togglePassword');
        const password = document.querySelectorAll('.pwbos');
        togglePassword.addEventListener('click', function (e) {
            password.forEach(function(e) {
                // toggle the type attribute
                const type = e.getAttribute('type') === 'password' ? 'text' : 'password';
                e.setAttribute('type', type);
            });
            // toggle the eye slash icon
            this.classList.toggle('fa-eye-slash');
        });

        $('#register-form').parsley().on('field:error', function () {
            let err = document.querySelectorAll('.parsley-errors-list');
            if (err) {
                err.forEach( (e) => {
                    e.style.listStyleType = "none";
                    e.style.padding = "0px";
                    e.style.margin = "0px";
                    e.style.fontSize = "small";
                    e.classList.add('font-weight-bolder');
                } )
            }
        });
    });
</script>
@endpush
