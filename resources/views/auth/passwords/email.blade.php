@extends('layouts.app')

@section('title', 'Login')

@section('styles')
<style>
    html::before{
        content: "";
        background-image: url( "{{asset('img/desktop17.png')}}" );
        background-repeat: no-repeat;
        background-size: 100%;
        position: absolute;
        top: 0px ;
        right: 0px ;
        bottom: 0px ;
        left: 0px ;
        opacity: 0.5;
    }

    body{
        background-color:transparent;
        opacity: 1.0;
    }

    .tombol {
        font-family: 'Poppins', sans-serif;
        font-style: normal;
        font-weight: 600;
        font-size: 15px;
        line-height: 30px;

        color: #ffffff;
        padding: 0 ;
        background-color: #76BADE;
    }
    
    .reg {
        font-family: 'Poppins', sans-serif;
        font-style: normal;
        font-weight: 600;
        font-size: 13.3px;
        line-height: 30px;

        color: #ffffff;
    }

    .tombol:hover {
        background-color: #0977AA;
    }

    #azura {
        font-family: 'Poppins', sans-serif;
        font-style: normal;
        font-weight: bold;
        font-size: 30px;
        line-height: 67px;
        margin: 0;

        /* primary */

        color: #0977AA;
    }
    #signin {
        font-family: 'Poppins', sans-serif;
        font-style: normal;
        font-weight: bold;
        font-size: 25px;
        line-height: 67px;
        margin-top: -7px;
        /* primary */

        color: #FFFFFF;
    }

    #email:focus {
    border:none !important;
    box-shadow:none;
    }

</style>
@endsection

@section('standalone')
<div class="container" style="margin:40px auto 0px;">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card border-0 text-center shadow-none" style="background-color: transparent;">
                <img class="card-img-top mx-auto" src="{{ asset('img/logo.png') }}" style="width: 10%;" alt="Card image cap">
                <h4 id="azura">Azura Labs</h4>
                <div class="card-body d-flex justify-content-center" style="padding: 0.8rem">
                    <div class="card" style="width: 380px; background-color : #2499BC;box-shadow: 10px 10px 10px 10px 0.7;">
                        <div class="card-body pt-1">
                            <h6 id="signin">Forgot Password</h6>
                            @if (session('status'))
                                <div class="form-group row d-flex justify-content-center mb-0">
                                    <div class="col-md-10" style="">
                                        <div class="input-group">
                                            <div class="alert alert-success" role="alert">
                                                {{ session('status') }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
    
                            <form method="POST" action="{{ route('password.email') }}">
                            @csrf
                                <div class="form-group row d-flex justify-content-center">
                                    <div class="col-md-10" style="">
                                        <div class="input-group ">
                                            <input id="email" style="border:0px;" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required placeholder="Email Address" autocomplete="off">
                                            <div class="input-group-append" style="border-radius: 0px 4px 4px 0px;">
                                                <div class="input-group-text" style="background: #FFFFFF; border:solid #FFF;">
                                                    <i class="fas fa-envelope"></i>
                                                </div>
                                            </div>
                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row d-flex justify-content-center">
                                    <div class="col-md-10 text-left">
                                        <button type="submit" class="btn btn-block tombol mb-1">
                                            Send Password Reset Link 
                                        </button>
                                        @if (Route::has('login'))
                                            <a id="login" class="reg" href="{{ route('login') }}">Already Have Account?</a>
                                        @endif 
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
